﻿using classesDB;
using System;
using System.Collections.Generic;
using System.Linq;
namespace classes
{
    public class MenuRequestHandler : RequestHandler
    {
        public void HandleProgramClosed(LoggedUser user)
        {
            if (user.GetUsername() != null)
            {
                PrivateStopWaiting(user);
            }

            Logout(user);
        }

        public static Response GetRooms(GetRoomsRequest request)
        {
            if(request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            GetRoomsResponse rooms = new GetRoomsResponse
            {
                Status = states.success,
                Rooms = RoomManager.Instance.GetRooms()
            };

            return rooms;
        }

        public static Response Signout(LogoutRequest request)
        {
            if (request == null) return new Response();

            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            Logout(request.UserWhoRequests);

            LogoutResponse logoutResponse = new LogoutResponse();
            logoutResponse.Status = states.success; 
            return logoutResponse;
        }

        private static void Logout(LoggedUser user)
        {
            LoginManager.Instance.Logout(user);
        }

        public static Response GetTopWinners(GetTopWinnersRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            GetTopWinnersResponse highScore = new GetTopWinnersResponse();
            highScore.Winners = SqliteDatabase.GetTopWinnersWithGamesPlayed();

            return highScore;
        }

        public static Response GetTopCategories(GetTopCategoriesRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            GetTopCategoriesResponse topCategories = new GetTopCategoriesResponse();
            topCategories.topCategories = SqliteDatabase.GetTopSelectedCategories();

            return topCategories;
        }

        public static Response JoinRoom(JoinRoomRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            JoinRoomResponse joinRoomResponse = new JoinRoomResponse();

            Room room = request.Room;

            if (room.GetMetaData().IsPublic == "Private")
            {
                request.UserWhoRequests.StartWaiting(room);

                if (room.HasUserBeenApproved(request.UserWhoRequests) != (int)states.fail)
                {
                    room.AddToWaitingList(request.UserWhoRequests);
                }

                joinRoomResponse.Status = 0;
            }
            else
            {
                if (room.HasUserBeenApproved(request.UserWhoRequests) == states.fail)
                {
                    joinRoomResponse.Status = states.fail;
                }
                else
                {
                    room.AddUserToRoom(request.UserWhoRequests);
                    request.UserWhoRequests.JoinRoom(room);
                    joinRoomResponse.Status = states.success;
                    request.UserWhoRequests.SetRoomMemberAsHandler();
                }
            }

            return joinRoomResponse;
        }

        public static Response CreateRoom(CreateRoomRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            RoomData roomData = new RoomData
            {
                Name = request.RoomName,
                MaxPlayers = request.MaxUsers,
                TimePerQuestion = request.AnswerTimeout,
                NumOfQuestionsInGame = request.QuestionCount,
                IsPublic = request.Privacy,
                Category = request.Category,
                RoomOwner = request.UserWhoRequests.GetUsername(),
                Id = RoomManager.Instance.GetFreeId(),
                CurrentAmountOfPlayers = 1,
                IsActive = 0
            };

            RoomManager.Instance.CreateRoom(request.UserWhoRequests, roomData);
            request.UserWhoRequests.SetRoomAdminAsHandler(RoomManager.Instance.GetRoom(roomData.Id));
            CreateRoomResponse response = new CreateRoomResponse();
            response.Status = states.success;
            return response;
        }

        public static Response GetPersonalStats(GetPersonalStatsRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            GetPersonalStatsResponse response = new GetPersonalStatsResponse
            {
                Statistics = SqliteDatabase.GetPersonalStat(request.UserWhoRequests.GetUsername()),
            };

            return response;
        }

        public static Response HasApproved(HasApprovedRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            HasApprovedResponse response = new HasApprovedResponse();
            Room room = request.UserWhoRequests.GetWaitingRoom();

            if (room == null)
            {
                response.ApprovedCode = states.fail;
            }

            if (request.UserWhoRequests.GetNotApprovedRooms().Contains(request.UserWhoRequests.GetWaitingRoom()))
            {
                response.ApprovedCode = states.fail;
            }

            response.ApprovedCode = (RoomManager.Instance.GetRoom(request.UserWhoRequests.GetWaitingRoom().GetMetaData().Id)).HasUserBeenApproved(request.UserWhoRequests);

            if (response.ApprovedCode == states.success)
            {
                request.UserWhoRequests.SetRoomMemberAsHandler();
            }
            else
            {
                if (response.ApprovedCode == states.fail)
                {
                    request.UserWhoRequests.StartWaiting(request.UserWhoRequests.GetWaitingRoom());
                }
            }

            return response;
        }

        public static Response StopWaiting(StopWaitingToApproveRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.menuHandler)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString();
                return error;
            }

            PrivateStopWaiting(request.UserWhoRequests);
            StopWaitingToApproveResponse response = new StopWaitingToApproveResponse();
            response.Status = states.success;
            return response;
        }

        private static void PrivateStopWaiting(LoggedUser user)
        {
            Room waitingRoom = user.GetWaitingRoom();
            if (waitingRoom != null)
            {
                waitingRoom.RemoveUser(user);
                user.StopWaiting();
            }
        }
    }
}
