﻿using classesDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class Game
    {
        [DataMember]
        private DateTime start;
        [DataMember]
        private List<Question> questions;
        [DataMember]
        private Dictionary<LoggedUser, GameData> players;
        [DataMember] 
        private readonly Room room;
        [DataMember]
        private readonly int gameId;

        public Game(Room room)
        {
            this.room = room;
            players = new Dictionary<LoggedUser, GameData>();

            foreach (var player in players.ToList())
            {
                players[player.Key] = new GameData { HasAnsweredLastQuestion = false };
            }

            start = DateTime.Now;
            gameId = room.GetMetaData().Id;
            InsertAllQuestions();
            InitializationUsers();
        }

        public LoggedUser GetWinner()
        {
            LoggedUser winner = null;
            KeyValuePair<LoggedUser, GameData> currentMax = new KeyValuePair<LoggedUser, GameData>();
            bool isCurrentTie = false;

            foreach (KeyValuePair<LoggedUser, GameData> player in players)
            {
                if(currentMax.Value == null)
                {
                    currentMax = player;
                }
                else
                {
                    if (player.Value.CorrectAnswerCount == currentMax.Value.CorrectAnswerCount)
                        isCurrentTie = true;

                    if (player.Value.CorrectAnswerCount > currentMax.Value.CorrectAnswerCount)
                    {
                        isCurrentTie = false;
                        currentMax = player;
                    }
                }
            }
            if (!isCurrentTie)
            {
                winner = currentMax.Key;
            }
            return winner;
        }

        public Question GetQuestionForUser(LoggedUser user)
        {
            var userKeys = players.Keys.ToList(); // Create a copy of the keys

            if (!userKeys.Contains(user))
            {
                return null;
            }


            players[user].QuestionIndex++;

            var userData = players[user];
            var currentQuestion = userData.CurrentQuestion;


            if (!userData.HasAnsweredLastQuestion && userData.QuestionIndex != 1)
            {
                userData.WrongAnswerCount++;
            }

            if (currentQuestion == null)
            {
                return null;
            }

            if (userData.QuestionIndex == room.GetMetaData().NumOfQuestionsInGame)
            {
                var end = DateTime.Now;
                var duration = end - start;
                int durationInt = (int)duration.TotalSeconds;
                if (players.TryGetValue(user, out var userGameData))
                {
                    userGameData.AverageAnswerTime = durationInt / room.GetMetaData().NumOfQuestionsInGame;
                    players[user] = userGameData; // Update the dictionary with the modified value
                }
                userData.CurrentQuestion = null;
            }
            else
            {
                userData.CurrentQuestion = questions[userData.QuestionIndex];
            }

            userData.HasAnsweredLastQuestion = false;
            return currentQuestion;
        }

        public Room GetRoom()
        {
            return room;
        }

        public Dictionary<LoggedUser, GameData> GetPlayers()
        {
            return players;
        }

        public int SubmitAnswer(LoggedUser user, int answerId)
        {
            if (!players.ContainsKey(user))
            {
                return 0;
            }

            SqliteDatabase.IncrementNumberOfQuestionAnswered(user.GetUsername());
            var userData = players[user];

            int correctAnswerId = questions[userData.QuestionIndex - 1].GetCorrectAnswerId();

            if (answerId == correctAnswerId)
            {
                userData.CorrectAnswerCount++;
            }
            else
            {
                userData.WrongAnswerCount++;
            }

            userData.HasAnsweredLastQuestion = true;

            if(HasEveryoneFinished())
            {
                SqliteDatabase.UpdateGameStatistics(this);
            }

            return correctAnswerId;
        }

        public void RemovePlayer(LoggedUser user)
        {
            if (players.ContainsKey(user))
            {
                players.Remove(user);
            }
        }

        public int GetId()
        {
            return gameId;
        }

        private void InitializationUsers()
        {
            foreach (var player in room.GetAllUsers())
            {
                GameData userData = new GameData
                {
                    CurrentQuestion = questions[0],
                    CorrectAnswerCount = 0,
                    WrongAnswerCount = 0,
                    AverageAnswerTime = 0,
                    QuestionIndex = 0
                };
                players[player.Key] = userData;
            }
        }

        public List<PlayerResults> GetGameResults()
        {
            List<PlayerResults> results = new List<PlayerResults>();

            if (!HasEveryoneFinished())
            {
                return results;
            }

            foreach (var player in players)
            {
                var user = player.Key;
                var gameData = player.Value;

                PlayerResults playerResults = new PlayerResults
                {
                    Username = user.GetUsername(),
                    CorrectAnswerCount = gameData.CorrectAnswerCount,
                    WrongAnswerCount = gameData.WrongAnswerCount,
                    AverageAnswerTime = (int)gameData.AverageAnswerTime
                };

                results.Add(playerResults);
            }
            return results;
        }

        public int GetTimePerQuestion()
        {
            return room.GetMetaData().TimePerQuestion;
        }

        public bool HasEveryoneFinished()
        {
            foreach (var player in players)
            {
                if (player.Value.CorrectAnswerCount + player.Value.WrongAnswerCount !=
                    room.GetMetaData().NumOfQuestionsInGame)
                {
                    return false;
                }
            }

            return true;
        }
        private void InsertAllQuestions()
        {
            questions = SqliteDatabase.GetQuestionsByCategoryAndNumber(
                room.GetMetaData().Category, room.GetMetaData().NumOfQuestionsInGame);
        }
    }
}
