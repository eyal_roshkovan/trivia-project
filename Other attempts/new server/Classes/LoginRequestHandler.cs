﻿using System;
using System.Collections.Generic;
using System.Linq;
using classes;

namespace classes
{
    public class LoginRequestHandler : RequestHandler
    {

        public static LoginResponse TryLogin(LoginRequest request)
        {
            LoginResponse response = new LoginResponse();
            response.User = LoginManager.Instance.Login(request.Username, request.Password); 
        
            if (response.User == null)
            {
                response.Status = states.fail;    
            }
            else
            {
                response.Status = states.success;
            }
            return response;
        }

        public static SignupResponse TrySignup(SignupRequest request)
        {
            if(request == null) return new SignupResponse { Status = states.fail, User = null};
            SignupResponse response = new SignupResponse();
            response.User = LoginManager.Instance.Signup(request.Username, request.Password, request.Email);

            if (response.User == null)
            {
                response.Status = states.fail;
            }
            else
            {
                response.Status = states.success;
            }
            return response;
        }

        public void HandleProgramClosed(LoggedUser member)
        {
            // can be empty
        }
    }
}
