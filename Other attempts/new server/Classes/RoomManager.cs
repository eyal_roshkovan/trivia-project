﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class RoomManager
    {
        [DataMember] private static RoomManager instance;
        [DataMember] private Dictionary<int, Room> rooms;

        private RoomManager()
        {
            rooms = new Dictionary<int, Room>();
        }

        public static RoomManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RoomManager();
                }
                return instance;
            }
        }

        public void CreateRoom(LoggedUser user, RoomData data)
        {
            Room newRoom = new Room(data, user);
            rooms[data.Id] = newRoom;
        }

        public void DeleteRoom(int id)
        {
            if (rooms.ContainsKey(id))
            {
                rooms.Remove(id);
            }
        }

        public int GetRoomState(int id)
        {
            if (rooms.ContainsKey(id))
            {
                return rooms[id].GetMetaData().IsActive;
            }

            return 0;
        }

        public List<RoomData> GetRooms()
        {
            return rooms.Values.Where(room => room.GetMetaData().IsActive == 0).Select(room => room.GetMetaData()).ToList();
        }

        public Room GetRoom(int id)
        {
            return rooms.ContainsKey(id) ? rooms[id] : null;
        }

        public int GetFreeId()
        {
            int closestFreeId = 1;

            foreach (var pair in rooms)
            {
                if (pair.Key > closestFreeId)
                {
                    break;
                }
                closestFreeId++;
            }

            return closestFreeId;
        }
    }
}