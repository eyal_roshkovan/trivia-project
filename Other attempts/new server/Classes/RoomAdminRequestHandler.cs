﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class RoomAdminRequestHandler : RequestHandler
    {
        public void HandleProgramClosed(LoggedUser member)
        {
            CloseRoom(member.GetRoom());
        }

        public static Response StartGame(StartGameRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            RoomManager.Instance.GetRoom(request.UserWhoRequests.GetRoom().GetMetaData().Id).SetActive(1);
            StartGameResponse response = new StartGameResponse
            {
                Status = states.success
            };
            return response;
        }
        private static void CloseRoom(Room room)
        {
            RoomManager.Instance.DeleteRoom(room.GetMetaData().Id);
        }

        public static Response CloseRoom(CloseRoomRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            CloseRoom(request.UserWhoRequests.GetRoom());
            CloseRoomResponse response = new CloseRoomResponse
            {
                Status = states.success
            };
            return response;
        }

        public static Response GetRoomState(GetRoomStateRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            Room room = request.UserWhoRequests.GetRoom();
            GetRoomStateResponse roomState = new GetRoomStateResponse
            {
                AnswerTimeout = room.GetMetaData().TimePerQuestion,
                HasGameBegun = room.GetMetaData().IsActive != 0,
                Players = room.GetAllUsers(),
                QuestionCount = room.GetMetaData().NumOfQuestionsInGame
            };

            return roomState;
        }

        public static Response GetPlayers(GetPlayersInRoomRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            GetPlayersInRoomResponse response = new GetPlayersInRoomResponse();
            response.Users = request.UserWhoRequests.GetRoom().GetAllUsers();
            return response;
        }

        public static Response GetWaitingUsers(GetWaitingToApproveListRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            GetWaitingToApproveListResponse response = new GetWaitingToApproveListResponse();
            response.Users = request.UserWhoRequests.GetRoom().GetWaitingUsers();
            return response;
        }

        public static Response ApproveUser(ApproveUserRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            if (request.Approved && !request.UserWhoRequests.GetRoom().IsFull())
            {
                request.UserWhoRequests.GetRoom().AddUserToRoom(request.memberToApprove);
            }
            else
            {
                DisapproveUser(request.UserWhoRequests, request.memberToApprove);
            }

            request.UserWhoRequests.GetRoom().RemoveFromWaitingList(request.memberToApprove);
            ApprovedUserResponse response = new ApprovedUserResponse
            {
                Status = states.success
            };
            return response;
        }

        public static Response KickPlayer(KickPlayerRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomAdminHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            DisapproveUser(request.UserWhoRequests, request.memberToKick);
            KickPlayerResponse response = new KickPlayerResponse
            {
                Status = states.success
            };
            return response;
        }

        private static void DisapproveUser(LoggedUser member, LoggedUser memberToDisapprove)
        {
            member.GetRoom().RemoveUser(memberToDisapprove);
        }
    }
}

