﻿using System.Collections.Generic;
namespace classes
{

    public class GameRequestHandler : RequestHandler
    {

        public void HandleProgramClosed(LoggedUser member)
        {
            GameManager.Instance.RemovePlayer(member, member.GetRoom().GetGame()); // To do
        }

        public static HasEveryoneFinishedResponse HasEveryoneFinished(HasEveryoneFinishedRequest request)
        {
            return new HasEveryoneFinishedResponse
            {
                HasEveryoneFinished = GameManager.Instance.HasEveryoneFinished(request.UserWhoRequests.GetRoom().GetGame())
            };
        }

        public static GetQuestionResponse GetQuestion(GetQuestionRequest request)
        {
            Question question = GameManager.Instance.GetQuestionForUserAndGame(request.UserWhoRequests, request.UserWhoRequests.GetRoom().GetGame());
            GetQuestionResponse response = new GetQuestionResponse();

            if (question != null)
            {
                response.Question = question.GetQuestion();

                // Convert vector to dictionary
                Dictionary<int, string> mapPossibleAnswers = new Dictionary<int, string>();
                int index = 1;
                List<string> vectorPossibleAnswers = question.GetPossibleAnswers();

                foreach (string str in vectorPossibleAnswers)
                {
                    mapPossibleAnswers[index++] = str;
                }

                response.Answers = mapPossibleAnswers;
                response.Status = states.success;
            }
            else
            {
                response.Status = 0;
                response.Question = "";
                response.Answers = new Dictionary<int, string>();
            }
            return response;
        }

        public static SubmitAnswerResponse SubmitAnswer(SubmitAnswerRequest request)
        {
            GameManager.Instance.SubmitAnswer(request.UserWhoRequests, request.UserWhoRequests.GetRoom().GetGame(), request.AnswerId);
            return new SubmitAnswerResponse
            {
                Status = states.success
            };
        }

        public static GetGameResultsResponse GetGameResults(GetGameResultsRequest request)
        {
            request.UserWhoRequests.SetMenuAsCurrentHandler();
            return new GetGameResultsResponse()
            {
                Status = states.success,
                Results = GameManager.Instance.GetGameResultsOfGame(request.UserWhoRequests.GetRoom().GetGame())
            };
        }

        public static LeaveGameResponse LeaveGame(LeaveGameRequest request)
        {
            GameManager.Instance.RemovePlayer(request.UserWhoRequests, request.UserWhoRequests.GetRoom().GetGame());
            request.UserWhoRequests.SetMenuAsCurrentHandler();
            return new LeaveGameResponse()
            {
                Status = states.success
            };
        }
    }
}
