﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class LoggedUser
    {
        [DataMember]
        private string username;
        [DataMember]
        private bool isReady;
        [DataMember]
        private int amountOfGamesPlayed;
        [DataMember]
        private Room roomWaitingToApprove;
        [DataMember]
        private statesOfHandlers currentHandler;
        [DataMember]
        private readonly List<Room> notApprovedRooms;
        [DataMember]
        private Room currentRoom;

        public LoggedUser(string userName, int amountOfGamesPlayed)
        {
            SetMenuAsCurrentHandler();
            notApprovedRooms = new List<Room>();
            username = userName;
            isReady = false;
            currentRoom = null;
            roomWaitingToApprove = null;
            this.amountOfGamesPlayed = amountOfGamesPlayed;
        }
        public List<Room> GetNotApprovedRooms()
        {
            return notApprovedRooms;
        }

        public void JoinRoom(Room room)
        {
            currentRoom = room;
        }

        public void LeaveRoom()
        {
            currentRoom = null;
        }

        public Room GetRoom()
        {
            return currentRoom;
        }
        public statesOfHandlers GetCurrentHandler()
        {
            return currentHandler;
        }
        public void SetMenuAsCurrentHandler()
        {
            currentHandler = statesOfHandlers.menuHandler;
        }
        public void SetRoomAdminAsHandler(Room room)
        {
            currentHandler = statesOfHandlers.roomAdminHandler;
        }
        public void SetRoomMemberAsHandler()
        {
            currentHandler = statesOfHandlers.roomMemberHandler;
        }
        public void SetGameAsHandler()
        {
            currentHandler = statesOfHandlers.gameAdminHandler;
        }
        public string GetUsername()
        {
            return username;
        }

        public int GetAmountOfGamesPlayed()
        {
            return amountOfGamesPlayed;
        }

        public void PlayGame()
        {
            amountOfGamesPlayed++;
        }
    
        public void ReadyUnready()
        {
            isReady = !isReady;
        }

        public bool GetIsReady()
        {
            return isReady;
        }

        public void ClearRoomList(StopWaitingToApproveRequest stopWaitingToApproveRequest)
        {

        }

        public void StartWaiting(Room room)
        {
            roomWaitingToApprove = room;
        }

        public void StopWaiting()
        {
            roomWaitingToApprove = null;
        }

        public Room GetWaitingRoom()
        {
            return roomWaitingToApprove;
        }   

        public bool Equals(LoggedUser other)
        {
            return username == other.username;
        }
    }

    public static class LoggedUserExtensions
    {
        public static bool Equals(this LoggedUser lhs, LoggedUser rhs)
        {
            return lhs.GetUsername() == rhs.GetUsername();
        }

        public static bool LessThan(this LoggedUser lhs, LoggedUser rhs)
        {
            return string.Compare(lhs.GetUsername(), rhs.GetUsername(), StringComparison.Ordinal) < 0;
        }
    }
}
