﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class GameManager
    {
        [DataMember]
        private static GameManager instance;
        [DataMember]
        private readonly List<Game> games;

        private GameManager()
        {
            games = new List<Game>();
        }
        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameManager();
                }
                return instance;
            }
        }

        public Game CreateGame(Room room)
        {
            // If the game already exists, return the game; if not, create a new game
            Game existingGame = games.FirstOrDefault(game => game.GetId() == room.GetMetaData().Id);
            if (existingGame != null)
            {
                return existingGame;
            }

            Game newGame = new Game(room);
            games.Add(newGame);

            return newGame;
        }

        public void DeleteGame(int gameId)
        {
            games.RemoveAll(game => game.GetId() == gameId);
        }

        public Question GetQuestionForUserAndGame(LoggedUser user, Game game)
        {
            Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
            return gameToSearch?.GetQuestionForUser(user);
        }

        public List<PlayerResults> GetGameResultsOfGame(Game game)
        {
            Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
            if (gameToSearch != null)
            {
                return gameToSearch.GetGameResults();
            }
            return new List<PlayerResults>();
        }

        public bool RemovePlayer(LoggedUser userToRemove, Game game)
        {
            try
            {
                Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
                gameToSearch?.RemovePlayer(userToRemove);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int SubmitAnswer(LoggedUser user, Game game, int answerId)
        {
            Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
            return gameToSearch?.SubmitAnswer(user, answerId) ?? 0;
        }

        public void IncGamesInTable(LoggedUser user)
        {
            // SqliteDatabase.IncrementCountOfGames(user.GetUsername());
        }

        public bool HasEveryoneFinished(Game game)
        {
            Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
            return gameToSearch?.HasEveryoneFinished() ?? false;
        }
    }
}
