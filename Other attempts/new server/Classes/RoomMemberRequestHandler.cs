﻿using System;
using System.Collections.Generic;
namespace classes
{

    public class RoomMemberRequestHandler : RequestHandler
    {
        public void HandleProgramClosed(LoggedUser member)
        {
            LeaveRoom(member);
        }

        public static Response LeaveRoom(LeaveRoomRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomMemberHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            LeaveRoom(request.UserWhoRequests);

            LeaveRoomResponse response = new LeaveRoomResponse();
            response.Status = states.success;
            return response;
        }
        private static Response LeaveRoom(LoggedUser user)
        {
            Room room = user.GetRoom();
            room.RemoveUser(user);

            if (user.GetIsReady())
                user.ReadyUnready();

            user.LeaveRoom();

            LeaveRoomResponse response = new LeaveRoomResponse();
            response.Status = states.success;

            return response;
        }

        public static Response ReadyUnready(ReadyUnreadyRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomMemberHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            ReadyUnreadyResponse response = new ReadyUnreadyResponse();
            response.Status = states.success;
            return response;
        }

        public static Response Refresh(RefreshRequest request)
        {
            if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomMemberHandler)
            {
                return new ErrorResponse
                {
                    Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                };
            }

            LoggedUser user = request.UserWhoRequests;
            Room room = user.GetRoom();

            if (RoomManager.Instance.GetRoom(room.GetMetaData().Id) == null)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Room has been closed by the owner";
                // Move to Menu Handler
                return error;
            }
            else if (RoomManager.Instance.GetRoom(room.GetMetaData().Id).GetMetaData().IsActive == (int)states.success)
            {
                user.ReadyUnready();
                RefreshResponse response = new RefreshResponse();
                response.Status = 2;
                // Move to Game Handler
                user.SetGameAsHandler();
                return response;
            }
            else
            {
                RefreshResponse response = new RefreshResponse();
                // Check if the user is kicked out by the room admin
                if (room.HasUserBeenApproved(user) == 0)
                {
                    LeaveRoom(user);
                    
                    response.Status = 9; // kicked out status
                    // Move to Menu Handler
                    user.SetMenuAsCurrentHandler();
                }
                else
                {
                    // No update 
                    response.Status = (int)states.success;
                }
                return response;
            }
        }

        public Response GetPlayers(GetPlayersInRoomRequest request)
        {
            try
            {
                if (request.UserWhoRequests.GetCurrentHandler() != statesOfHandlers.roomMemberHandler)
                {
                    return new ErrorResponse
                    {
                        Message = "Not your handler ATM, your current handler is " + request.UserWhoRequests.GetCurrentHandler().ToString()
                    };
                }

                GetPlayersInRoomResponse response = new GetPlayersInRoomResponse();
                response.Users = request.Room.GetAllUsers();
                return response;
            }
            catch (Exception)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Room has been closed by the owner";
                return error;
            }
        }
    }
}
