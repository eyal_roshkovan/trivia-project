﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class Response
    {

    }

    [DataContract]
    [KnownType(typeof(LogoutRequest))]
    public class Request
    {
        [DataMember]
        public LoggedUser UserWhoRequests;
    }

    [DataContract]
    public enum states
    {
        [EnumMember]
        success = 1,
        [EnumMember]
        fail = 0,
        [EnumMember]
        onHold = -1
    };

    [DataContract]
    public enum statesOfHandlers
    {
        [EnumMember]
        menuHandler = 1,
        [EnumMember]
        roomAdminHandler = 2,
        [EnumMember]
        roomMemberHandler = 3,
        [EnumMember]
        gameAdminHandler = 4
    };

    [DataContract]
    public class LeaveRoomRequest : Request
    { }

    [DataContract]
    public class HasEveryoneFinishedRequest : Request
    { }

    [DataContract]
    public class GetQuestionRequest : Request
    { }
    
    [DataContract]
    public class GetGameResultsRequest: Request
    { }

    [DataContract]
    public class LeaveGameRequest : Request
    { }

    [DataContract]
    public class RefreshRequest : Request
    { }

    public class UserGamesWonResponse : Response
    {
        [DataMember]
        public int GamesWon;
    }

    [DataContract]
    public class LoginResponse
    {
        [DataMember]
        public states Status { get; set; }

        [DataMember]
        public LoggedUser User { get; set; }
    }

    [DataContract]
    public class LeaveRoomResponse :  Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class LeaveGameResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class SubmitAnswerResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class CreateRoomResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class SignupResponse
    {
        [DataMember]
        public states Status;
        [DataMember]
        public LoggedUser User;
    }

    [DataContract]
    public class ReadyUnreadyResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class ErrorResponse : Response
    {
        [DataMember]
        public string Message;
    }

    [DataContract]
    public class GetTopWinnersResponse : Response
    {
        [DataMember]
        public List<Tuple<string, int, int>> Winners;
    };

    [DataContract]
    public class GetTopCategoriesResponse : Response
    {
        [DataMember]
        public List<Tuple<string, int>> topCategories;
    }

    [DataContract]
    public class LoginRequest
    {
        [DataMember]
        public string Username;
        [DataMember]
        public string Password;
    }

    [DataContract]
    public class SignupRequest
    {
        [DataMember]
        public string Username;
        [DataMember]
        public string Password;
        [DataMember]
        public string Email;
    }

    [DataContract]
    public class GetRoomsResponse : Response
    {
        [DataMember]
        public states Status;
        [DataMember]
        public List<RoomData> Rooms;
    }

    [DataContract]
    public class GetPersonalStatsResponse : Response
    {
        [DataMember]
        public List<string> Statistics;
    }

    [DataContract]
    public class JoinRoomResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class CreateRoomRequest : Request
    {
        [DataMember]
        public string Category;
        [DataMember]
        public string RoomName;
        [DataMember]
        public int MaxUsers;
        [DataMember]
        public int QuestionCount;
        [DataMember]
        public int AnswerTimeout;
        [DataMember]
        public string Privacy;
    }

    [DataContract]
    public class JoinRoomRequest : Request
    {
        [DataMember]
        public Room Room;
    }

    [DataContract]
    public class ReadyUnreadyRequest : Request
    {
        [DataMember]
        public bool ReadyOrNot;
    }

    [DataContract]
    public class GetRoomStateResponse : Response
    {
        [DataMember]
        public bool HasGameBegun;
        [DataMember]
        public Dictionary<LoggedUser, string> Players;
        [DataMember]
        public int QuestionCount;
        [DataMember]
        public int AnswerTimeout;
    }

    [DataContract]
    public class RoomData
    {
        [DataMember]
        public int Id;
        [DataMember]
        public string Name;
        [DataMember]
        public int MaxPlayers;
        [DataMember]
        public int NumOfQuestionsInGame;
        [DataMember]
        public int CurrentAmountOfPlayers;
        [DataMember]
        public int TimePerQuestion;
        [DataMember]
        public int IsActive;
        [DataMember]
        public string IsPublic;
        [DataMember]
        public string RoomOwner;
        [DataMember]
        public string Category;
    }

    [DataContract]
    public class RefreshResponse : Response
    {
        [DataMember]
        public int Status;
    }

    [DataContract]
    public class GetPlayersInRoomRequest : Request
    {
        [DataMember]
        public Room Room;
    }

    [DataContract]
    public class GetPlayersInRoomResponse : Response
    {
        [DataMember]
        public Dictionary<LoggedUser, string> Users;
    }

    [DataContract]
    public class HasApprovedResponse : Response
    {
        [DataMember]
        public states ApprovedCode;
    }

    [DataContract]

    public class GetWaitingToApproveListRequest : Request
    {}

    [DataContract]
    public class GetWaitingToApproveListResponse : Response
    {
        [DataMember]
        public List<LoggedUser> Users;
    }
    [DataContract]
    public class StopWaitingToApproveRequest : Request
    {}

    [DataContract]
    public class CloseRoomRequest : Request
    { }

    [DataContract]
    public class HasApprovedRequest : Request
    { }

    [DataContract]
    public class GetRoomStateRequest : Request
    { }


    [DataContract]
    public class ApproveUserRequest : Request
    {
        [DataMember]
        public bool Approved;
        [DataMember]
        public LoggedUser memberToApprove;
    }

    [DataContract]
    public class ApprovedUserResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class CloseRoomResponse : Response
    {
        [DataMember]
        public states Status;
    }

    [DataContract]
    public class StopWaitingToApproveResponse : Response
    {
        [DataMember]
        public states Status;
    }
    

    [DataContract]
    public class KickPlayerRequest : Request
    {
        [DataMember]
        public LoggedUser memberToKick;
    }

    [DataContract]
    public class GetRoomsRequest : Request
    {}

    [DataContract]
    public class GetTopCategoriesRequest : Request
    { }

    [DataContract]
    public class GetTopWinnersRequest : Request
    { }

    [DataContract]
    public class GetPersonalStatsRequest : Request
    { }

    [DataContract]
    public class LogoutRequest : Request
    { }

    [DataContract]
    public class LogoutResponse : Response
    {
        public states Status;
    }

    [DataContract]
    public class KickPlayerResponse : Response
    {
        public states Status;
    }


    [DataContract]
    public class GetQuestionResponse : Response
    {
        [DataMember]
        public states Status;
        [DataMember]
        public string Question;
        [DataMember]
        public Dictionary<int, string> Answers;
    }

    [DataContract]
    public class PlayerResults
    {
        [DataMember]
        public string Username;
        [DataMember]
        public int CorrectAnswerCount;
        [DataMember]
        public int WrongAnswerCount;
        [DataMember]
        public int AverageAnswerTime;
    }

    [DataContract]
    public class GetGameResultsResponse : Response
    {
        [DataMember]
        public states Status;
        [DataMember]
        public List<PlayerResults> Results;
    }

    [DataContract]
    public class SubmitAnswerRequest : Request
    {
        [DataMember]
        public int AnswerId;
    }

    [DataContract]
    public class StartGameRequest : Request
    {}

    [DataContract]
    public class GameData
    {
        [DataMember]
        public Question CurrentQuestion;
        [DataMember]
        public int CorrectAnswerCount;
        [DataMember]
        public int WrongAnswerCount;
        [DataMember]
        public float AverageAnswerTime;
        [DataMember]
        public int QuestionIndex;
        [DataMember]
        public bool HasAnsweredLastQuestion;
    }

    [DataContract]
    public class GetTimePerQuestionResponse : Response
    {
        public int TimePerQuestion;
    }

    [DataContract]
    public class StartGameResponse : Response
    {
        public states Status;
    }

    [DataContract]
    public class HasEveryoneFinishedResponse : Response
    {
        public bool HasEveryoneFinished;
    }
}
