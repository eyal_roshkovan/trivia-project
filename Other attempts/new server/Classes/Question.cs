﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

[DataContract]
public class Question
{
    [DataMember] 
    private string question;
    [DataMember]
    private List<string> possibleAnswers;
    [DataMember]
    private int correctAnswerId;
 
    public Question(string question, List<string> possibleAns, int correctId)
    {
        this.question = question;
        possibleAnswers = possibleAns;
        correctAnswerId = correctId;
    }

    public string GetQuestion()
    {
        return question;
    }

    public List<string> GetPossibleAnswers()
    {
        return possibleAnswers;
    }

    public int GetCorrectAnswerId()
    {
        return correctAnswerId;
    }

    public bool Equals(Question other)
    {
        // Implement your own logic for equality comparison
        // This is a basic example, adjust as needed
        return question == other.question &&
               possibleAnswers.SequenceEqual(other.possibleAnswers) &&
               correctAnswerId == other.correctAnswerId;
    }

}
