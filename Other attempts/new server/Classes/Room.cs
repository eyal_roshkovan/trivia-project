﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace classes
{
    [DataContract]
    public class Room
    {
        [DataMember]
        private RoomData metadata;
        [DataMember]
        private Dictionary<LoggedUser, string> users;
        [DataMember] 
        private LoggedUser roomOwner;
        [DataMember]
        private List<LoggedUser> waitingUsers;
        [DataMember]
        private Game game;

        public Room(RoomData roomData, LoggedUser roomOwner)
        {
            metadata = roomData;
            users = new Dictionary<LoggedUser, string>();
            waitingUsers = new List<LoggedUser>();
            users[roomOwner] = "Owner";
            this.roomOwner = roomOwner;
            game = null;
        }

        public void StartGame(Game game)
        {
            this.game = game;
        }

        public Game GetGame()
        {
            return game;
        }

        public states HasUserBeenApproved(LoggedUser user)
        {
            if (waitingUsers.Contains(user))
                return states.onHold;

            if (users.ContainsKey(user))
                return states.success;

            return states.fail;
        }

        public List<LoggedUser> GetWaitingUsers()
        {
            return waitingUsers;
        }

        public void AddToWaitingList(LoggedUser user)
        {
            waitingUsers.Add(user);
        }

        public void RemoveFromList(LoggedUser user)
        {
            waitingUsers.Remove(user);
        }

        public void AddUserToRoom(LoggedUser newUser)
        {
            users[newUser] = "Member";
            metadata.CurrentAmountOfPlayers++;
        }

        public void RemoveUser(LoggedUser userToRemove)
        {
            foreach (var user in users)
            {
                if (user.Key == userToRemove)
                {
                    users.Remove(user.Key);
                    metadata.CurrentAmountOfPlayers--;
                }
            }
        }

        public void RemoveFromWaitingList(LoggedUser userToRemove)
        {
            foreach (var user in waitingUsers)
            {
                if (user == userToRemove)
                {
                    users.Remove(user);
                }
            }
        }

        public Dictionary<LoggedUser, string> GetAllUsers()
        {
            return new Dictionary<LoggedUser, string>(users);
        }
        public LoggedUser GetOwner() 
        { 
            return roomOwner; 
        }
        public RoomData GetMetaData()
        {
            if (this == null)
            {
                throw new InvalidOperationException("Room has been closed");
            }

            return metadata;
        }

        public void SetActive(int isActive)
        {
            metadata.IsActive = isActive;
        }

        public bool Equals(Room other)
        {
            return GetMetaData().Id == other.GetMetaData().Id;
        }

        public void ReadyUnreadyUser(LoggedUser user)
        {
            foreach (var entry in users)
            {
                if (entry.Key.Equals(user))
                {
                    entry.Key.ReadyUnready();
                }
            }
        }

        public bool IsFull()
        {
            return metadata.MaxPlayers == metadata.CurrentAmountOfPlayers;
        }
    }

    public static class RoomExtensions
    {
        public static bool Equals(this Room lhs, Room rhs)
        {
            return lhs.GetMetaData().Name == rhs.GetMetaData().Name;
        }
    }
}
