﻿using classesDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
namespace classes
{
    [DataContract]
    public class LoginManager
    {
        [DataMember]
        private List<LoggedUser> loggedUsers = new List<LoggedUser>();
        [DataMember] 
        private object _loggedUserLock = new object();
        [DataMember] 
        private static LoginManager instance;

        private LoginManager()
        {
            SqliteDatabase.Open();
        }
        public static LoginManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LoginManager();
                }
                return instance;
            }
        }

        public LoggedUser Signup(string userName, string password, string email)
        {
            int res = SqliteDatabase.AddNewUser(userName, password, email);
            if (res == 0)
                return null;

            LoggedUser newUser = new LoggedUser(userName, SqliteDatabase.GetNumOfPlayerGames(userName));

            lock (_loggedUserLock)
            {
                loggedUsers.Add(newUser);
            }

            return newUser;
        }

        public LoggedUser Login(string username, string password)
        {
            int res = SqliteDatabase.DoesPasswordMatch(username, password);
            if (res == 0)
                return null;

            lock (_loggedUserLock)
            {
                if (loggedUsers.Any(user => user.GetUsername() == username))
                    return null;

                LoggedUser newUser = new LoggedUser(username, SqliteDatabase.GetNumOfPlayerGames(username));
                loggedUsers.Add(newUser);
                return newUser;
            }
        }

        public void Logout(LoggedUser user)
        {
            lock (_loggedUserLock)
            {
                if (user != null)
                {
                    loggedUsers.Remove(user);
                    Console.WriteLine($"User {user.GetUsername()} logged out.");
                }
            }
        }
    }
}
