﻿namespace classes
{
    public interface RequestHandler
    {
        void HandleProgramClosed(LoggedUser member);
    }
}
