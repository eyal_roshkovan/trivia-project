﻿using classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
namespace My_Server
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        Response GetResponse(Request request);

        [OperationContract]
        LoginResponse GetLoginResponse(LoginRequest request);        
        
        [OperationContract]
        SignupResponse GetSignupResponse(SignupRequest request);

        [OperationContract]
        Response GetLogoutResponse(LogoutRequest request);
        [OperationContract]
        Response GetRoomsResponse(GetRoomsRequest request);

        [OperationContract]
        Response GetTopCategoriesResponse(GetTopCategoriesRequest request);

        [OperationContract]
        Response GetTopWinnersResponse(GetTopWinnersRequest request);

        [OperationContract]
        Response GetPersonalStatsResponse(GetPersonalStatsRequest request);

        [OperationContract]
        Response SubmitAnswerResponse(SubmitAnswerRequest request);

        [OperationContract]
        Response StartGameResponse(StartGameRequest request);

        [OperationContract]
        Response CreateRoomResponse(CreateRoomRequest request);

        [OperationContract]
        Response JoinRoomResponse(JoinRoomRequest request);

        [OperationContract]
        Response ReadyUnreadyResponse(ReadyUnreadyRequest request);

        [OperationContract]
        Response GetPlayersInRoomResponse(GetPlayersInRoomRequest request);

        [OperationContract]
        Response GetWaitingToApproveListResponse(GetWaitingToApproveListRequest request);

        [OperationContract]
        Response StopWaitingToApproveResponse(StopWaitingToApproveRequest request);

        [OperationContract]
        Response CloseRoomResponse(CloseRoomRequest request);

        [OperationContract]
        Response HasApprovedResponse(HasApprovedRequest request);

        [OperationContract]
        Response GetRoomStateResponse(GetRoomStateRequest request);

        [OperationContract]
        Response ApproveUserResponse(ApproveUserRequest request);

        [OperationContract]
        Response KickPlayerResponse(KickPlayerRequest request);

        [OperationContract]
        Response LogoutResponse(LogoutRequest request);

        [OperationContract]
        Response HasEveryoneFinishedResponse(HasEveryoneFinishedRequest request);

        [OperationContract]
        Response GetQuestionResponse(GetQuestionRequest request);

        [OperationContract]
        Response GetGameResultsResponse(GetGameResultsRequest request);

        [OperationContract]
        Response LeaveGameResponse(LeaveGameRequest request);
    }
}
