﻿using classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace My_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private Dictionary<Type, Func<Request, Response>> requestProcessorMap;

        public Service1()
        {
            requestProcessorMap = new Dictionary<Type, Func<Request, Response>>
            {
                { typeof(GetRoomsRequest), request => MenuRequestHandler.GetRooms((GetRoomsRequest)request)},
                { typeof(GetTopCategoriesRequest), request => MenuRequestHandler.GetTopCategories((GetTopCategoriesRequest)request)},
                { typeof(GetTopWinnersRequest), request => MenuRequestHandler.GetTopWinners((GetTopWinnersRequest)request)},
                { typeof(GetPersonalStatsRequest), request => MenuRequestHandler.GetPersonalStats((GetPersonalStatsRequest)request)},
                { typeof(SubmitAnswerRequest), request => GameRequestHandler.SubmitAnswer((SubmitAnswerRequest)request)},
                { typeof(StartGameRequest), request => RoomAdminRequestHandler.StartGame((StartGameRequest)request)},
                { typeof(CreateRoomRequest), request => MenuRequestHandler.CreateRoom((CreateRoomRequest)request)},
                { typeof(JoinRoomRequest), request => MenuRequestHandler.JoinRoom((JoinRoomRequest)request)},
                { typeof(ReadyUnreadyRequest), request => RoomMemberRequestHandler.ReadyUnready((ReadyUnreadyRequest)request)},
                { typeof(GetPlayersInRoomRequest), request => RoomAdminRequestHandler.GetPlayers((GetPlayersInRoomRequest)request)},
                { typeof(GetWaitingToApproveListRequest), request => RoomAdminRequestHandler.GetWaitingUsers((GetWaitingToApproveListRequest)request)},
                { typeof(StopWaitingToApproveRequest), request => MenuRequestHandler.StopWaiting((StopWaitingToApproveRequest)request)},
                { typeof(CloseRoomRequest), request => RoomAdminRequestHandler.CloseRoom((CloseRoomRequest)request)},
                { typeof(HasApprovedRequest), request => MenuRequestHandler.HasApproved((HasApprovedRequest)request)},
                { typeof(GetRoomStateRequest), request => RoomAdminRequestHandler.GetRoomState((GetRoomStateRequest)request)},
                { typeof(ApproveUserRequest), request => RoomAdminRequestHandler.ApproveUser((ApproveUserRequest)request)},
                { typeof(KickPlayerRequest), request => RoomAdminRequestHandler.KickPlayer((KickPlayerRequest)request)},
                { typeof(LogoutRequest), request => MenuRequestHandler.Signout((LogoutRequest)request)},
                { typeof(HasEveryoneFinishedRequest), request => GameRequestHandler.HasEveryoneFinished((HasEveryoneFinishedRequest)request)},
                { typeof(GetQuestionRequest), request => GameRequestHandler.GetQuestion((GetQuestionRequest)request)},
                { typeof(GetGameResultsRequest), request => GameRequestHandler.GetGameResults((GetGameResultsRequest)request)},
                { typeof(LeaveGameRequest), request => GameRequestHandler.LeaveGame((LeaveGameRequest)request)}
            };

        }

        private Response ProcessRequest<TRequest>(TRequest request) where TRequest : Request
        {
            if (requestProcessorMap.TryGetValue(typeof(TRequest), out var processor))
            {
                return processor(request);
            }
            else
            {
                return new ErrorResponse
                {
                    Message = "Unsupported request type"
                };
            }
        }

        public Response GetResponse(Request request)
        {
            return ProcessRequest(request);
        }

        public LoginResponse GetLoginResponse(LoginRequest request)
        {
            return LoginRequestHandler.TryLogin(request);
        }
        
        public SignupResponse GetSignupResponse(SignupRequest request)
        {
            return LoginRequestHandler.TrySignup(request);
        }

        public Response GetLogoutResponse(LogoutRequest request)
        {
            return ProcessRequest(request);
        }

        public Response GetRoomsResponse(GetRoomsRequest request)
        {
            return ProcessRequest(request);
        }

        public Response GetTopCategoriesResponse(GetTopCategoriesRequest request)
        {
            return ProcessRequest(request);
        }

        public Response GetTopWinnersResponse(GetTopWinnersRequest request)
        {
            return ProcessRequest<GetTopWinnersRequest>(request);
        }

        public Response GetPersonalStatsResponse(GetPersonalStatsRequest request)
        {
            return ProcessRequest<GetPersonalStatsRequest>(request);
        }

        public Response SubmitAnswerResponse(SubmitAnswerRequest request)
        {
            return ProcessRequest<SubmitAnswerRequest>(request);
        }

        public Response StartGameResponse(StartGameRequest request)
        {
            return ProcessRequest<StartGameRequest>(request);
        }

        public Response CreateRoomResponse(CreateRoomRequest request)
        {
            return ProcessRequest<CreateRoomRequest>(request);
        }

        public Response JoinRoomResponse(JoinRoomRequest request)
        {
            return ProcessRequest<JoinRoomRequest>(request);
        }

        public Response ReadyUnreadyResponse(ReadyUnreadyRequest request)
        {
            return ProcessRequest<ReadyUnreadyRequest>(request);
        }

        public Response GetPlayersInRoomResponse(GetPlayersInRoomRequest request)
        {
            return ProcessRequest<GetPlayersInRoomRequest>(request);
        }

        public Response GetWaitingToApproveListResponse(GetWaitingToApproveListRequest request)
        {
            return ProcessRequest<GetWaitingToApproveListRequest>(request);
        }

        public Response StopWaitingToApproveResponse(StopWaitingToApproveRequest request)
        {
            return ProcessRequest<StopWaitingToApproveRequest>(request);
        }

        public Response CloseRoomResponse(CloseRoomRequest request)
        {
            return ProcessRequest<CloseRoomRequest>(request);
        }

        public Response HasApprovedResponse(HasApprovedRequest request)
        {
            return ProcessRequest<HasApprovedRequest>(request);
        }

        public Response GetRoomStateResponse(GetRoomStateRequest request)
        {
            return ProcessRequest<GetRoomStateRequest>(request);
        }

        public Response ApproveUserResponse(ApproveUserRequest request)
        {
            return ProcessRequest<ApproveUserRequest>(request);
        }

        public Response KickPlayerResponse(KickPlayerRequest request)
        {
            return ProcessRequest<KickPlayerRequest>(request);
        }

        public Response LogoutResponse(LogoutRequest request)
        {
            return ProcessRequest<LogoutRequest>(request);
        }

        public Response HasEveryoneFinishedResponse(HasEveryoneFinishedRequest request)
        {
            return ProcessRequest<HasEveryoneFinishedRequest>(request);
        }

        public Response GetQuestionResponse(GetQuestionRequest request)
        {
            return ProcessRequest<GetQuestionRequest>(request);
        }

        public Response GetGameResultsResponse(GetGameResultsRequest request)
        {
            return ProcessRequest<GetGameResultsRequest>(request);
        }

        public Response LeaveGameResponse(LeaveGameRequest request)
        {
            return ProcessRequest<LeaveGameRequest>(request);
        }
    }
}
