import asyncio
import websockets
import socket

def binary_to_char(binary_string):
    # Convert the binary string to a decimal value and then to a character.
    decimal_value = int(binary_string, 2)
    character = chr(decimal_value)
    return character


def get_json_from_binary(binary):
    current = ""  # Initialize a variable to store the current 8-bit binary chunk.
    json_str = ""  # Initialize the resulting JSON string.

    # Loop through the binary string in 8-bit chunks.
    for i in range(0, len(binary), 8):
        for j in range(8):
            current += binary[i + j]  # Construct the 8-bit chunk.

        if len(json_str) == 0:
            # If the JSON string is empty and the current chunk represents an opening brace '{', add it to the JSON.
            if binary_to_char(current) == '{':
                json_str += binary_to_char(current)
        else:
            # For subsequent chunks, directly add them to the JSON.
            json_str += binary_to_char(current)

        current = ""  # Reset the current chunk.

    return json_str.replace(r'\\n', '\n')  # Replace '\\n' with newline characters.



# Define a function to handle WebSocket connections
socket_client_dict = {}


async def handle_websocket(websocket, path):
    try:
        print("new client connected")
        # Create a new socket for each WebSocket client to the main server
        async for message in websocket:
            serial_number = message[0:10]
            message = message[10:]

            if serial_number in socket_client_dict:
                # Use the existing socket
                client_socket = socket_client_dict[serial_number]
            else:
                # Create a new socket and store it in the dictionary
                client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client_socket.connect(('127.0.0.1', 8826))
                socket_client_dict[serial_number] = client_socket

            # Forward the message to the main server
            client_socket.sendall(message.encode('utf-8'))

            # Receive the response from the main server
            response = client_socket.recv(2048)
            response_str = ''.join(['0' if byte == 0 else '1' for byte in response])
            # Send the response back to the client
            await websocket.send(response_str)
            print("Sent " + get_json_from_binary(response_str))
    except websockets.exceptions.ConnectionClosedOK:
        # Handle a clean closure of the WebSocket client connection
        print(f"Connection from client {websocket.remote_address} closed cleanly")
    except websockets.exceptions.ConnectionClosedError as e:
        # Handle a closure with an error
        print(f"Connection from client {websocket.remote_address} closed with an error: {e.reason}")
    except Exception as e:
        # Handle other WebSocket errors
        print(f"WebSocket Error for client {websocket.remote_address}: {str(e)}")
    finally:
        pass

# in web server, every time handle_websocket is called, it creates a new socket and connection

# Create a WebSocket server
start_server = websockets.serve(handle_websocket, '127.0.0.1', 8827)

# Start the WebSocket server
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()