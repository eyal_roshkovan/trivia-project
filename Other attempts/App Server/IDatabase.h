#pragma once
#include <iostream>
#include <string>
#include "sqlite3.h"
#include <io.h>
#include <vector>
#include "Question.h"

using std::string;
using std::cout;
using std::endl;

class IDatabase
{
public:
	virtual bool open() = 0;
	virtual bool close() = 0;
	virtual int doesUserExist(string userName) = 0;
	virtual int doesPasswordMatch(string userName, string password) = 0;
	virtual int addNewUser(string userName, string password, string email) = 0;
	virtual int addNewUserStatistics(string userName) = 0;
	virtual std::vector<Question> getQuestionsByCategoryAndNumber(string category, int limit) = 0;

	//Statistics table
	virtual float getPlayerAverageAnswerTime(string userName) = 0;
	virtual int getNumOfCorrectAnswers(string userName) = 0;
	virtual int getNumOfTotalAnswers(string userName) = 0;
	virtual int getNumOfPlayerGames(string userName) = 0;
	virtual void incrementNumberOfQuestionAnswered(const std::string& userName) = 0;
	virtual void incrementCorrectQuestionsAnswered(const std::string& userName) = 0;
	virtual void incrementCountOfGames(const std::string& userName) = 0;


	//User score
	virtual int getPlayerScore(string userName) = 0;
	virtual std::vector<string> getHighScores() = 0;
	virtual std::vector<string> getPersonalStat(string userName) = 0;
};

