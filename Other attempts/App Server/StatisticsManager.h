#pragma once
#include "IDatabase.h"
#include "json.hpp"


class StatisticsManager
{
public:
	StatisticsManager(IDatabase* db);
	~StatisticsManager();

	std::vector<string> getHighScore();
	std::vector<string> getUserStatistics(string userName);
	string gerUserPoints(string userName);
private:
	IDatabase* m_database;
};

