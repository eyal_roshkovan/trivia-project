#pragma once
#include "IDatabase.h"
#include <vector>
#include "Question.h"
#include <algorithm> // for std::shuffle
#include <random>

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	//Users table
	virtual int doesUserExist(string userName) override;
	virtual int doesPasswordMatch(string userName, string password) override;
	virtual int addNewUser(string userName, string password, string email) override;
	virtual int addNewUserStatistics(string userName) override;

	//Statistics table
	virtual float getPlayerAverageAnswerTime(string userName) override;
	virtual int getNumOfCorrectAnswers(string userName) override;
	virtual int getNumOfTotalAnswers(string userName) override;
	virtual int getNumOfPlayerGames(string userName) override;
	virtual std::vector<string> getPersonalStat(string userName) override;
	virtual void incrementNumberOfQuestionAnswered(const std::string& userName) override;
	virtual void incrementCorrectQuestionsAnswered(const std::string& userName) override;
	virtual void incrementCountOfGames(const std::string& userName) override;


	//User score
	virtual int getPlayerScore(string userName) override;
	virtual std::vector<string> getHighScores() override;

	virtual bool open() override;
	virtual bool close() override;
	virtual std::vector<Question> getQuestionsByCategoryAndNumber(string category, int limit) override;

private:
	sqlite3* _db;
	const string _dbFileName = "Trivia.sqlite";

};