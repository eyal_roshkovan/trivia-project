#include "JsonRequestPacketDeserializer.h"
#include <iostream>
#include <string>
using namespace std;
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
    LoginRequest loginRequest;

    string password = "";
    string userName = "";

    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "username")
                    userName = it.value();

                if (it.key() == "password")
                    password = it.value();
            }
        }

    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    loginRequest.password = password;
    loginRequest.username = userName;
    return loginRequest;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
    SignupRequest request;

    string password = "";
    string userName = "";
    string email = "";

    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "username")
                    userName = it.value();
                if (it.key() == "password")
                    password = it.value();
                if (it.key() == "email")
                    email = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.password = password;
    request.username = userName;
    request.email = email;

    return request;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::vector<unsigned char> buffer)
{
    GetPlayersInRoomRequest request;

    int roomID = 0;

    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "roomID")
                    roomID = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.roomID = roomID;
    return request;

}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
    JoinRoomRequest request;

    int roomID = 0;

    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "roomID")
                    roomID = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.roomID = roomID;
    return request;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
    CreateRoomRequest request;

    int maxUsers = 0;
    int questionCount = 0;
    int answerTimeout = 0;
    string roomName;
    string category;
    string privacy;
    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "maxUsers")
                    maxUsers = it.value();
                else if (it.key() == "questionCount")
                    questionCount = it.value();
                else if (it.key() == "answerTimeout")
                    answerTimeout = it.value();
                else if (it.key() == "roomName")
                    roomName = it.value();
                else if (it.key() == "category")
                    category = it.value();
                else if (it.key() == "privacy")
                    privacy = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }

    request.answerTimeout = answerTimeout;
    request.maxUsers = maxUsers;
    request.questionCount = questionCount;
    request.roomName = roomName;
    request.category = category;
    request.privacy = privacy;
    return request;
}

ReadyUnreadyRequest JsonRequestPacketDeserializer::deserializeReadyUnreadyRequest(std::vector<unsigned char> buffer)
{
    ReadyUnreadyRequest request;

    bool ready = 0;

    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "ready")
                    ready = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.readyOrNot = ready;
    return request;
}

ApprovedUserRequest JsonRequestPacketDeserializer::deserializeApprovedUserRequest(std::vector<unsigned char> buffer)
{
    ApprovedUserRequest request;

    string username;
    bool approved;

    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "username")
                    username = it.value();
                if (it.key() == "approved")
                    approved = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.username = username;
    request.approved = approved;
    return request;
}

KickPlayerRequest JsonRequestPacketDeserializer::deserializeKickPlayerRequest(std::vector<unsigned char> buffer)
{

    KickPlayerRequest request;


    // Decode bytes from binary to regular string
    std::string jsonString = get_string(buffer);
    string username;
    try
    {
        json j = json::parse(jsonString); // convert json string
        if (j.is_object())
        {
            // extract values from json

            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "username")
                    username = it.value();
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.username = username;
    return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(std::vector<unsigned char> buffer)
{
    SubmitAnswerRequest request;

    //Get data
    string dedoced_string = get_string(buffer);

    int answerId;

    // Decode bytes from binary to regular string
    try
    {
        json j = json::parse(dedoced_string); // convert json string
        if (j.is_object())
        {
            // extract values from json
            for (json::iterator it = j.begin(); it != j.end(); ++it)
            {
                if (it.key() == "answerId")
                {
                    answerId = it.value();
                }
            }
        }
    }
    catch (const nlohmann::json::exception& e)
    {
        // handle parse error
    }
    request.answerId = answerId;
    return request;
}

string JsonRequestPacketDeserializer::get_string(std::vector<unsigned char> buffer)
{
    string strLen = "";
    for (int i = CODE_SIZE; i < DATA_LEN_SIZE + CODE_SIZE; i++)
    {
        strLen += buffer[i];
    }
    std::bitset<DATA_LEN_SIZE> bits(strLen);
    int dataLen = bits.to_ulong();

    string binaryStr = "";
    buffer.erase(buffer.begin(), buffer.begin() + DATA_LEN_SIZE + CODE_SIZE);
    for (char ch : buffer)
        binaryStr += ch;

    string bytes;
    for (std::size_t i = 0; i < dataLen; i += 8)
    {
        std::bitset<8> bits(binaryStr.substr(i, 8));
        bytes.push_back(static_cast<unsigned char>(bits.to_ulong()));
    }

    return bytes;
}
