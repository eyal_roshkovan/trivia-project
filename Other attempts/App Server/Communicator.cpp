#include "Communicator.h"
#include <future>

Communicator::Communicator(RequestHandlerFactory& handleFactory) : m_handlerFactory(handleFactory)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(LISTEN_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << LISTEN_PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		try
		{
			client_accept();	
		}
		catch (const std::exception& e)
		{
			// Do nothing about it
		}
	}
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	IRequestHandler* handler = m_handlerFactory.createLoginRequestHandler();
	try
	{
		while (handler != nullptr)
		{
			RequestResult requestResult;

			RequestInfo requestInfo = handleNewMessage(clientSocket);

			RequestResult result = handler->handleRequest(requestInfo);
			Helper::sendData(clientSocket, std::string(result.buffer.data(), result.buffer.data() + result.buffer.size()));
			handler = result.newHandler;
		}
	}
	catch (const std::exception& e)
	{
		handler->handleProgramClosed();
	}
}

RequestInfo Communicator::handleNewMessage(SOCKET clientSocket)
{
	std::vector<unsigned char> buffer;
	int requestId = 0;
	string message;
	RequestInfo requestInfo;

	// Using std::async to handle the receiving with a timeout
	auto future = std::async(std::launch::async, [&] {
		return Helper::getStringPartFromSocket(clientSocket, 2048);
		});

	// Wait for the result with a timeout of 60 seconds
	if (future.wait_for(std::chrono::seconds(60)) == std::future_status::timeout)
	{
		// Throw an exception if no data received within 50 seconds
		throw std::runtime_error("No data received within 50 seconds.");
	}

	// Retrieve the message from the future
	message = future.get();

	buffer = std::vector<unsigned char>(message.data(), message.data() + message.size());

	try
	{
		requestId = Helper::getMessageCode(buffer);
	}
	catch (...)
	{
		// Handle exception if any
	}

	requestInfo.codeID = requestId;
	requestInfo.buffer = buffer;

	auto now = std::chrono::system_clock::now();
	requestInfo.recevialTime = std::chrono::system_clock::to_time_t(now);

	return requestInfo;
}


void Communicator::client_accept()
{
	// this accepts the client and create a specific socket from server to this client
	// the process will not continue until a client connects to the server
	SOCKET client_socket = accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	// the function that handle the conversation with the client

	std::unique_lock<std::mutex> lock(_mapSocketLock);

	LoginRequestHandler* loginHandler;
	_mapSockets.insert({ client_socket, loginHandler });

	std::thread handler(&Communicator::handleNewClient, this, client_socket);
	handler.detach();
}
