#pragma once

#include <string>
#include <vector>
#include <map>
using std::string;

struct RoomData {
    unsigned int id;
    std::string name;
    unsigned int maxPlayers;
    unsigned int numOfQuestionsInGame;
    unsigned int currentAmountOfPlayers;
    unsigned int timePerQuestion;
    unsigned int isActive;
    std::string isPublic;
    std::string roomOwner;
    std::string category;
};

class LoggedUser;

class Room
{
public:
    Room();
    Room(const RoomData& roomData, LoggedUser* roomOwner);
    ~Room();
    void addUser(LoggedUser* newUser);
    void removeUser(LoggedUser* userToRemove);
    std::map<LoggedUser*, string> getAllUsers();
    RoomData getMetaData() const;
    void setActive(unsigned int isActive);
    bool operator== (const Room& other) const;
    void readyUnreadyUser(LoggedUser* user);
    bool isFull() const;
private:
    RoomData m_metadata;
    std::map<LoggedUser*, string> m_users;
    LoggedUser* m_roomOwner;
};
