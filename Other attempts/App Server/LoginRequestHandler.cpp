#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& factory): m_handlerFactory(factory)
{

}

bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	std::vector<unsigned char> buffer = requestInfo.buffer;
    string strCode = "";

    for (int i = 0; i < CODE_SIZE; i++)
    {
        strCode += buffer[0];
        buffer.erase(buffer.begin() + 0);
    }

    std::bitset<CODE_SIZE> bits(strCode);
    int code = bits.to_ulong();
    
    return code == 1 || code == 2;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult requestResult;
    bool loginValid;
    try {
        if (requestInfo.codeID == LOGIN_CODE)
        {
            LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
            LoginResponse response;
            // Access to DB
            loginValid = m_handlerFactory.getLoginManager().login(loginRequest.username, loginRequest.password);
            if (!loginValid)
            {
                response.status = fail;
                requestResult.buffer = JsonResponsePacketSerializer::serializeLoginResponse(response);
                requestResult.newHandler = new LoginRequestHandler(m_handlerFactory);
            }
            else {
                response.status = success;
                requestResult.buffer = JsonResponsePacketSerializer::serializeLoginResponse(response);
                //Create new handler MenuRequestHandler
                LoggedUser* user = new LoggedUser(loginRequest.username, std::stoi(m_handlerFactory.getStatisticsManager().gerUserPoints(loginRequest.username)));
                
                requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, user);
                
            }
        }
        else if (requestInfo.codeID == SIGNUP_CODE)
        {
            SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
            SignupResponse response;
            // Access to DB
            loginValid = m_handlerFactory.getLoginManager().signup(signupRequest.username, signupRequest.password, signupRequest.email);
            if (!loginValid)
            {
                response.status = fail;
                requestResult.buffer = JsonResponsePacketSerializer::serializeSignUpResponse(response);
                requestResult.newHandler = new LoginRequestHandler(m_handlerFactory);
            }
            else
            {
                response.status = success;
                requestResult.buffer = JsonResponsePacketSerializer::serializeSignUpResponse(response);

                //Create new handler MenuRequestHandler
                LoggedUser* user = new LoggedUser(signupRequest.username, 0);

                requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, user);
            }
        }
        else {
            ErrorResponse response;
            response.message = "Login Error";

            requestResult.buffer = JsonResponsePacketSerializer::serializeErrorResponse(response);
            requestResult.newHandler = nullptr;
        }
    }
    catch (...)
    {
        ErrorResponse response;
        response.message = "Syntax Error";

        requestResult.buffer = JsonResponsePacketSerializer::serializeErrorResponse(response);
        requestResult.newHandler = nullptr;
    }
    return requestResult;
}

void LoginRequestHandler::handleProgramClosed()
{
    // can be empty
}
