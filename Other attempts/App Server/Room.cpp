#include "Room.h"
#include "LoggedUser.h"

Room::Room(): m_metadata(RoomData()), m_roomOwner(nullptr)
{}


Room::Room(const RoomData& roomData, LoggedUser* roomOwner)
    : m_metadata(roomData), m_roomOwner(roomOwner)
{
    m_users[roomOwner] = "Owner";
}

Room::~Room()
{
    delete &m_users;

}

void Room::addUser(LoggedUser* newUser)
{
    m_users[newUser] = "Member";
    m_metadata.currentAmountOfPlayers++;
}

void Room::removeUser(LoggedUser* userToRemove)
{
    for (auto it = m_users.begin(); it != m_users.end(); )
    {
        if (*it->first == userToRemove)
        {
            it = m_users.erase(it);
            m_metadata.currentAmountOfPlayers--;
        }
        else
        {
            ++it;
        }
    }
}

std::map<LoggedUser*, string> Room::getAllUsers()
{
    return m_users;
}

RoomData Room::getMetaData() const
{
    if(this == NULL)
        throw std::exception("Room has been close");

        
    return m_metadata;
}

void Room::setActive(unsigned int isActive)
{
    m_metadata.isActive = isActive;
}

bool Room::operator==(const Room& other) const
{
    return getMetaData().id == other.getMetaData().id;
}

void Room::readyUnreadyUser(LoggedUser* user)
{
    for (std::map<LoggedUser*, string>::iterator it = m_users.begin(); it != m_users.end(); ++it)
    {
        if (*it->first == *user)
            it->first->readyUnready();
    }
}

bool Room::isFull() const
{
    return m_metadata.maxPlayers == m_metadata.currentAmountOfPlayers;
}

bool operator==(const Room& lhs, const Room& rhs)
{
    return lhs.getMetaData().name == rhs.getMetaData().name;
}