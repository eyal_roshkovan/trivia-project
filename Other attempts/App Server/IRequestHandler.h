#pragma once
//#include "Helper.h"
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <thread>
#include <algorithm>
#include <iostream>
#include <exception>
#include <vector>
#include <fstream>
#include <mutex>
#include "StructsHeader.h"

struct RequestResult;
struct RequestInfo;

using std::cout;
using std::endl;
using std::string;


class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo requestInfo) = 0;
	virtual void handleProgramClosed() = 0;
};



