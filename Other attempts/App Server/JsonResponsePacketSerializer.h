#pragma once
#include "StructsHeader.h"
#include "Room.h"

enum states {
	success = 1,
	fail = 0
};

class JsonResponsePacketSerializer
{
public:
	static std::vector<unsigned char> serializeLoginResponse(LoginResponse loginResponse);
	static std::vector<unsigned char> serializeSignUpResponse(SignupResponse signUpResponse);
	static std::vector<unsigned char> serializeErrorResponse(ErrorResponse errorResponse);

	static std::vector<unsigned char> serializeLogoutResponse(LogoutResponse logoutResponse);
	static std::vector<unsigned char> serializeGetRoomResponse(GetRoomsResponse getRoomsResponse);
	static std::vector<unsigned char> serializeGetPlayersInRoomResponse(GetPlayersInRoomResponse playerInRoomResponse);
	static std::vector<unsigned char> serializeJoinRoomResponse(JoinRoomResponse joinRoomResponse);
	static std::vector<unsigned char> serializeCreateRoomResponse(CreateRoomResponse createRoomResponse);
	static std::vector<unsigned char> serializeHighScoreResponse(getHighScoreResponse highScoreResponse);
	static std::vector<unsigned char> serializePersonalStats(getPersonalStatsResponse personalStatsResponse);

	static std::vector<unsigned char> serializeCloseRoomResponse(CloseRoomResponse closeRoomResponse);
	static std::vector<unsigned char> serializeStartGameResponse(StartGameResponse startGameResponse);
	static std::vector<unsigned char> serializeLeaveRoomResponse(LeaveRoomResponse leaveRoomResponse);
	static std::vector<unsigned char> serializeGetRoomStateRespone(GetRoomStateResponse getRoomStateResponse);
	static std::vector<unsigned char> serializeReadyUnreadyResponse(ReadyUnreadyResponse readyUnreadyResponse);
	static std::vector<unsigned char> serializeRefreshResponse(RefreshResponse refreshResponse);
	static std::vector<unsigned char> serializeUserPointsResponse(UserPointsResponse userPointsResponse);
	static std::vector<unsigned char> serializeGetOwnerRoomResponse(GetOwnerRoomResponse getOwnerRoomResponse);
	static std::vector<unsigned char> serializeHasApprovedResponse(HasApprovedResponse hasApprovedResponse);
	static std::vector<unsigned char> serializeStopWaitingResponse(StopWaitingResponse stopWaitingResponse);
	static std::vector<unsigned char> serializeGetWaitingUsersResponse(GetWaitingToApproveListResponse getWaitingToApproveListResponse);
	static std::vector<unsigned char> serializeApproveUserResponse(ApprovedUserResponse approvedUserResponse);
	static std::vector<unsigned char> serializeKickPlayerResponse(KickPlayerResponse kickPlayerResponse);
	
	static std::vector<unsigned char> serializeGetGameResultsResponse(GetGameResultsResponse getGameResultsResponse);
	static std::vector<unsigned char> serializeSubmitAnswerResponse(SubmitAnswerResponse submitAnswerResponse);
	static std::vector<unsigned char> serializeGetQuestionResponse(GetQuestionResponse getQuestionResponse);
	static std::vector<unsigned char> serializeLeaveGameResponse(LeaveGameResponse leaveGameResponse);
	static std::vector<unsigned char> serializeHasEveryoneFinishedResponse(HasEveryoneFinishedResponse hasEveryoneFinishedResponse);
private:
	static std::vector<unsigned char> convert_into_shape(json data, int code);

};