#include "LoggedUser.h"
#include "Room.h"
#include "RoomAdminRequestHandler.h"

LoggedUser::LoggedUser(const std::string& userName, int points) : m_username(userName), m_ownedRoom(nullptr), m_totalPoints(points), m_isReady(false)
{}

std::string LoggedUser::getUserName() const
{
    return m_username;
}

void LoggedUser::readyUnready()
{
    m_isReady = !m_isReady;
}

bool LoggedUser::getIsReady() const
{
    return m_isReady;
}

void LoggedUser::addRoom(Room* room)
{
    m_ownedRoom = room;
}

void LoggedUser::deleteOwnedRoom()
{
    m_ownedRoom = nullptr;
}

void LoggedUser::clearRoomList()
{
    m_roomWaitingToApprove->removeUser(m_username);
    m_roomWaitingToApprove = nullptr;
}

void LoggedUser::addRoom(RoomAdminRequestHandler* room)
{
    m_roomWaitingToApprove = room;
}

int LoggedUser::getTotalPoints() const
{
    return m_totalPoints;
}

bool LoggedUser::isApproved()
{
    return m_roomWaitingToApprove->hasUserBeenApproved(this);
}

Room* LoggedUser::getWaitingRoom()
{
    if (m_roomWaitingToApprove == nullptr)
        return nullptr;

    return m_roomWaitingToApprove->getRoom();
}

bool LoggedUser::operator==(const LoggedUser* other) const
{
    return m_username == other->m_username && m_totalPoints == other->m_totalPoints;
}

bool operator==(const LoggedUser& lhs, const LoggedUser& rhs)
{
    return lhs.getUserName() == rhs.getUserName();
}


bool operator<(const LoggedUser& lhs, const LoggedUser& rhs)
{
    return lhs.getUserName() < rhs.getUserName();
}
