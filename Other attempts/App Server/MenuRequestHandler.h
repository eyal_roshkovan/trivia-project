#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "StructsHeader.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RoomMemberRequestHandler.h"

class RequestHandlerFactory;
class RoomMemberRequestHandler;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory& factory, LoggedUser* user);
	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;
	virtual void handleProgramClosed() override;
private:
	RequestResult getRooms();
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult signout();
	RequestResult getHighScore();
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);
	RequestResult getPersonalStats();
	RequestResult HasApproved();
	RequestResult StopWaiting();
	std::vector<Room*> m_notApprovedRooms;
	RequestHandlerFactory& m_handlerFactory;
	LoggedUser* m_user;
	RoomManager& m_roomManager;
	StatisticsManager& m_statisticsManager;

};