#pragma once
#include <iostream>
#include <vector>

using std::string;

class Question
{
public:
	Question(string question, std::vector<string> possibleAns, int correctId);
	string getQuestion() const;
	std::vector<string> getPossibleAnswers();
	int getCorrectAnswerId();
	bool operator==(const Question& other);

private:
	string m_question;
	std::vector<string> m_possibleAnswers;
	int m_correctAnswerId;
};

