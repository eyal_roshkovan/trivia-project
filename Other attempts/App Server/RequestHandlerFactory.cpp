#include "RequestHandlerFactory.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db): m_database(db), m_loginManager(db), m_StatisticsManager(db), m_gameManager(db)
{}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    LoginRequestHandler* loginRequestHandler = new LoginRequestHandler(*this);
    return loginRequestHandler;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
    return m_loginManager;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
    MenuRequestHandler* menuRequestHandler = new MenuRequestHandler(*this, &user);
    return menuRequestHandler;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return m_StatisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
    return m_roomManager;
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room* room)
{
    return new RoomAdminRequestHandler(m_roomManager, *this, user, room);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room* room)
{
    return new RoomMemberRequestHandler(m_roomManager, *this, user, room);
}

GameManager& RequestHandlerFactory::getGameManager()
{
    return m_gameManager;
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser* user, Room& room)
{
    GameRequestHandler* handler = new GameRequestHandler(*this, user, &room);
    return handler;
}