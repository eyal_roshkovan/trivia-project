#pragma once
#include "StructsHeader.h"
#include "RoomManager.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"

class MenuRequestHandler;
class RequestHandlerFactory;

class RoomMemberRequestHandler: public IRequestHandler
{
public:
	RoomMemberRequestHandler(RoomManager& roomManager, RequestHandlerFactory& handlerFactory, LoggedUser user, Room* room);
	bool isRequestRelevant(RequestInfo information) override;
	RequestResult handleRequest(RequestInfo information) override;
	virtual void handleProgramClosed() override;

private:
	RequestResult leaveRoom();
	RequestResult getRoomState();
	RequestResult readyUnready();
	RequestResult refresh();
	RequestResult getUserPoints();
	RequestResult GetPlayers();

	Room* m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
};