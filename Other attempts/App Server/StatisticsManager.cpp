#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDatabase* db) : m_database(db)
{
    m_database->open();
}

StatisticsManager::~StatisticsManager()
{
    m_database->close();
}

std::vector<string> StatisticsManager::getHighScore()
{
    return m_database->getHighScores();
}

std::vector<string> StatisticsManager::getUserStatistics(string userName)
{
    std::vector<string> userStat;

    userStat.push_back(std::to_string(m_database->getPlayerAverageAnswerTime(userName)));
    userStat.push_back(std::to_string(m_database->getNumOfCorrectAnswers(userName)));
    userStat.push_back(std::to_string(m_database->getNumOfTotalAnswers(userName)));
    userStat.push_back(std::to_string(m_database->getNumOfPlayerGames(userName)));

    return userStat;
}

string StatisticsManager::gerUserPoints(string userName)
{
    int points = m_database->getPlayerScore(userName);
    return std::to_string(points);
}
