#pragma once
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include "IDatabase.h"
#include "SqliteDatabase.h"
class Server
{
public:
	Server();
	void run();

private:
	Communicator m_communicator;
	IDatabase* m_database;
	RequestHandlerFactory m_handleFactory;
};

