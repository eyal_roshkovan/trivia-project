#include "SqliteDatabase.h"

bool SqliteDatabase::open()
{
    int file_exist = _access(_dbFileName.c_str(), 0);
    int res = sqlite3_open(_dbFileName.c_str(), &_db);
    if (res != SQLITE_OK)
    {
        _db = nullptr;
        cout << "Error with open data base" << endl;
        return false;
    }
    const char* sqlStatement;

    char* errMessage = nullptr;
    if (file_exist != 0)
    {
        //Create table Users
        sqlStatement = "CREATE TABLE USERS(UserName TEXT NOT NULL,Password TEXT NOT NULL,Email TEXT NOT NULL,PRIMARY KEY(UserName));";
        errMessage = nullptr;
        res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
        if (res != SQLITE_OK)
            return false;

        //Create table statistics
        sqlStatement = "CREATE TABLE STATISTICS (userName TEXT NOT NULL,numberOfQuestionAnswered INTEGER NOT NULL, sumOfTime REAL NOT NULL, correctQuestionsAnswered	INTEGER NOT NULL,countOfGames INTEGER NOT NULL,PRIMARY KEY(userName),FOREIGN KEY (UserName) REFERENCES USERS(UserName));";
        errMessage = nullptr;
        res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
        if (res != SQLITE_OK)
            return false;
    }
    return true;
}

bool SqliteDatabase::close()
{
    try {
        sqlite3_close(_db);
        _db = nullptr;
    }
    catch (...)
    {
        return false;
    }
    return true;
}

SqliteDatabase::SqliteDatabase()
{}

int SqliteDatabase::doesUserExist(string userName)
{
    string statementStr = "SELECT * FROM USERS WHERE UserName = '" + userName + "'";
    const char* sqlStatement = statementStr.c_str();
    int exist = 0;
    char* errMessage;

    auto userExist = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* pExist = static_cast<int*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pExist = 1;
        }
        return 0;
    };

    int res = sqlite3_exec(_db, sqlStatement, userExist, &exist, &errMessage);
    if (res != SQLITE_OK)
        return 0;
    return exist;
}

int SqliteDatabase::doesPasswordMatch(string userName, string password)
{
    string statementStr = "SELECT * FROM USERS WHERE UserName = '" + userName + "' AND Password = '" + password + "'";
    const char* sqlStatement = statementStr.c_str();
    int exist = 0;
    char* errMessage;

    auto userExist = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* pExist = static_cast<int*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pExist = 1;
        }
        return 0;
    };

    int res = sqlite3_exec(_db, sqlStatement, userExist, &exist, &errMessage);
    if (res != SQLITE_OK)
        return 0;
    return exist;
}

int SqliteDatabase::addNewUser(string userName, string password, string email)
{
    string statementStr = "INSERT INTO USERS (UserName, Password, Email) VALUES ('"+userName+"', '" + password + "', '" + email + "');";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK)
        return 0;
    return 1;
}

int SqliteDatabase::addNewUserStatistics(string userName)
{
    string statementStr = "INSERT INTO STATISTICS (userName, numberOfQuestionAnswered, sumOfTime, correctQuestionsAnswered, countOfGames) VALUES ('"+userName+"', 0, 0.0, 0, 0);";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK)
        return 0;
    return 1;
}

float SqliteDatabase::getPlayerAverageAnswerTime(string userName)
{
    //Case: user dont exist
    if (!doesUserExist(userName))
        return 0;
    float avg = 0;
    float sumOfTime = 0;
    int numOfQuestion = 0;

    //Get time
    auto getTime = [](void* data, int argc, char** argv, char** azColName) -> int {
        float* pTime = static_cast<float*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pTime = std::stof(argv[0]); // convert string to float
        }
        return 0;
    };

    string statementStr = "SELECT sumOfTime FROM STATISTICS WHERE userName = '"+userName+"'";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement, getTime, &sumOfTime, &errMessage);
    if (res != SQLITE_OK)
        return 0;

    if (sumOfTime == 0) // cant divied with 0
        return 0;

    //Get num of questions
    auto getNumOfQuestions = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* pNum = static_cast<int*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pNum = std::stoi(argv[0]); // convert string to int
        }
        return 0;
    };

    statementStr = "SELECT numberOfQuestionAnswered FROM STATISTICS WHERE userName = '" + userName + "'";
    sqlStatement = statementStr.c_str();
    errMessage = nullptr;
    res = sqlite3_exec(_db, sqlStatement, getNumOfQuestions, &numOfQuestion, &errMessage);
    if (res != SQLITE_OK)
        return 0;
    
    return numOfQuestion/sumOfTime;
}

int SqliteDatabase::getNumOfCorrectAnswers(string userName)
{
    int numOfCorrectAnswer = 0;

    auto getCorrectAnswers = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* pTime = static_cast<int*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pTime = std::stoi(argv[0]); // convert string to float
        }
        return 0;
    };

    string statementStr = "SELECT correctQuestionsAnswered FROM STATISTICS WHERE userName = '" + userName + "'";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement, getCorrectAnswers, &numOfCorrectAnswer, &errMessage);
    if (res != SQLITE_OK)
        return 0;

    return numOfCorrectAnswer;
}

int SqliteDatabase::getNumOfTotalAnswers(string userName)
{
    int numOfQuestion = 0;
    auto getNumOfQuestions = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* pNum = static_cast<int*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pNum = std::stoi(argv[0]); // convert string to int
        }
        return 0;
    };

    string statementStr = "SELECT numberOfQuestionAnswered FROM STATISTICS WHERE userName = '" + userName + "'";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement, getNumOfQuestions, &numOfQuestion, &errMessage);
    if (res != SQLITE_OK)
        return 0;

    return numOfQuestion;
}

int SqliteDatabase::getNumOfPlayerGames(string userName)
{
    int numOfGames = 0;
    auto getNumOfGames = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* pNum = static_cast<int*>(data);
        if (argc > 0 && argv[0] != nullptr) {
            *pNum = std::stoi(argv[0]); // convert string to int
        }
        return 0;
    };

    string statementStr = "SELECT countOfGames FROM STATISTICS WHERE userName = '" + userName + "'";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement, getNumOfGames, &numOfGames, &errMessage);
    if (res != SQLITE_OK)
        return 0;

    return numOfGames;
}

std::vector<string> SqliteDatabase::getPersonalStat(string userName)
{
    std::vector<string> userStat;

    userStat.push_back(std::to_string(getPlayerScore(userName)));//Points
    userStat.push_back(",");
    userStat.push_back(std::to_string(getPlayerAverageAnswerTime(userName)));//Avg time
    userStat.push_back(",");
    userStat.push_back(std::to_string(getNumOfCorrectAnswers(userName)));//Num of correct answers
    userStat.push_back(",");
    userStat.push_back(std::to_string(getNumOfTotalAnswers(userName)));//Num of Total Answers
    userStat.push_back(",");
    userStat.push_back(std::to_string(getNumOfPlayerGames(userName)));//Num of Games that played

    return userStat;
}

int SqliteDatabase::getPlayerScore(string userName)
{
    ////                  a                  b                       c
    ////Formula: (correctAnswer * 8) - (avgTime) + numberOfAnswerWhereAnswered
    //int a = getNumOfCorrectAnswers(userName) * 8;
    //int b = getPlayerAverageAnswerTime(userName);
    //if (a - b < 0)
    //    b = 0;
    //int c = getNumOfTotalAnswers(userName);
    //return (a - b ) + c;
    int scores = 0;

    auto getScoresCallback = [](void* data, int argc, char** argv, char** azColName) -> int {
        int* scores = static_cast<int*>(data);
        if (argc >= 0 && argv[0] != nullptr && argv[0] != nullptr) {
            *scores = std::stoi(argv[1]);
        }
        return 0;
    };

    std::string query = "SELECT userName, ((correctQuestionsAnswered * 8) - (sumOfTime) + numberOfQuestionAnswered) AS score FROM STATISTICS WHERE userName = '"+userName+"';";

    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, query.c_str(), getScoresCallback, &scores, &errMessage);
    if (res != SQLITE_OK)
    {
        return scores;
    }
    return scores;
}

std::vector<string> SqliteDatabase::getHighScores()
{
    std::vector<string> highScores;

    struct ScoreData
    {
        string userName;
        int score;
    };

    auto getScoresCallback = [](void* data, int argc, char** argv, char** azColName) -> int {
        std::vector<ScoreData>* scores = static_cast<std::vector<ScoreData>*>(data);
        if (argc >= 2 && argv[0] != nullptr && argv[1] != nullptr) {
            ScoreData scoreData;
            scoreData.userName = argv[0];
            scoreData.score = std::stoi(argv[1]);
            scores->push_back(scoreData);
        }
        return 0;
    };

    std::string query = "SELECT userName, ((correctQuestionsAnswered * 8) - (sumOfTime) + numberOfQuestionAnswered) AS score FROM STATISTICS ORDER BY score DESC LIMIT 3";

    char* errMessage = nullptr;
    std::vector<ScoreData> scores;
    int res = sqlite3_exec(_db, query.c_str(), getScoresCallback, &scores, &errMessage);
    if (res != SQLITE_OK)
    {
        return highScores;
    }

    for (const auto& scoreData : scores)
    {
        highScores.push_back(scoreData.userName + "-" + std::to_string(scoreData.score) + ",");
    }

    return highScores;
}

std::vector<Question> SqliteDatabase::getQuestionsByCategoryAndNumber(string category, int limit)
{
    std::vector<Question> questions;

    string statementStr = "SELECT question, firstWrongAnswer, secondWrongAnswer, thirdWrongAnswer, rightAnswer FROM QuestionsAnswers WHERE category = '" + category + "' ORDER BY RANDOM() LIMIT " + std::to_string(limit) + ";";
    const char* sqlStatement = statementStr.c_str();
    char* errMessage;

    auto retrieveQuestion = [](void* data, int argc, char** argv, char** azColName) -> int {
        std::vector<Question>* questionList = static_cast<std::vector<Question>*>(data);

        if (argc >= 5) {
            string question = argv[0] ? argv[0] : "";
            string firstWrongAnswer = argv[1] ? argv[1] : "";
            string secondWrongAnswer = argv[2] ? argv[2] : "";
            string thirdWrongAnswer = argv[3] ? argv[3] : "";
            string rightAnswer = argv[4] ? argv[4] : "";

            std::vector<string> possibleAnswers = { firstWrongAnswer, secondWrongAnswer, thirdWrongAnswer, rightAnswer };
            std::random_device rd;
            std::default_random_engine rng(rd());

            // Shuffle the vector using the random number generator
            std::shuffle(possibleAnswers.begin(), possibleAnswers.end(), rng);
            int correctAnswerId = -1; // Placeholder for the correct answer index

            // Find the correct answer index within the possibleAnswers vector
            for (int i = 0; i < possibleAnswers.size(); i++) {
                if (possibleAnswers[i] == rightAnswer) {
                    correctAnswerId = i + 1;
                    break;
                }
            }

            Question questionObj(question, possibleAnswers, correctAnswerId);
            questionList->push_back(questionObj);
        }

        return 0;
    };

    int res = sqlite3_exec(_db, sqlStatement, retrieveQuestion, &questions, &errMessage);
    if (res != SQLITE_OK) {
        // Handle error, such as logging or error reporting
    }

    return questions;
}

void SqliteDatabase::incrementNumberOfQuestionAnswered(const std::string& userName)
{
    const std::string sqlStatement = "UPDATE STATISTICS SET numberOfQuestionAnswered = numberOfQuestionAnswered + 1 WHERE userName = '" + userName + "';";
    char* errMessage;
    int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK) {
        // Handle error, such as logging or error reporting
    }
}

void SqliteDatabase::incrementCorrectQuestionsAnswered(const std::string& userName)
{
    const std::string sqlStatement = "UPDATE STATISTICS SET correctQuestionsAnswered = correctQuestionsAnswered + 1 WHERE userName = '" + userName + "';";
    char* errMessage;
    int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK) {
        // Handle error, such as logging or error reporting
    }
}

void SqliteDatabase::incrementCountOfGames(const std::string& userName)
{
    const std::string sqlStatement = "UPDATE STATISTICS SET countOfGames = countOfGames + 1 WHERE userName = '" + userName + "';";
    char* errMessage;
    int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
    if (res != SQLITE_OK) {
        // Handle error, such as logging or error reporting
    }
}
