#pragma once
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "IDatabase.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "MenuRequestHandler.h"
#include "GameManager.h"
#include "GameRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;


class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* db);
	
	//For login
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();

	//For Menu
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();

	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser user, Room* room);

	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, Room* room);
	GameManager& getGameManager();
	GameRequestHandler* createGameRequestHandler(LoggedUser* user, Room& room);
private:
	IDatabase* m_database;
	
	//Login
	LoginManager m_loginManager;

	//Menu
	RoomManager m_roomManager;
	StatisticsManager m_StatisticsManager;
	GameManager m_gameManager;

};