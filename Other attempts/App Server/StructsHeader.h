#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include "IRequestHandler.h"
#include "json.hpp"
#include <bitset>
#include "Room.h"
#include "LoggedUser.h"
#include "Question.h"


class IRequestHandler;

using std::string;
using json = nlohmann::json;

#define CODE_SIZE  8
#define DATA_LEN_SIZE 32 


enum TypesOfRequestsCodes {
	ERROR_CODE = 0,
	LOGIN_CODE = 1,
	SIGNUP_CODE = 2,
	LOGOUT_CODE = 3,
	JOINROOM_CODE = 4,
	CREATEROOM_CODE = 5,
	GETROOMS_CODE = 6,
	PLAYERSINROOM_CODE = 7,
	HIGHSCORE_CODE = 8,
	PERSONALSTATISTICS_CODE = 9,
	CLOSEROOM_CODE = 10,
	READYNOTREADY_CODE = 11,
	GETROOMSTATE_CODE = 12,
	LEAVEROOM_CODE = 13,
	STARTGAME_CODE = 14,
	REFRESH_CODE = 15,
	USERPOINTS_CODE = 16,
	GETOWNERROOM_CODE = 17,
	HASAPPROVED_CODE = 18,
	STOPWAITING_CODE = 19,
	GETWAITINGTOAPPROVELIST_CODE = 20,
	APPROVEUSER_CODE = 21,
	LEAVEGAME_CODE = 22,
	GETQUESTION_CODE = 23,
	SUBMITANSWER_CODE = 24,
	GETGAMERESULT_CODE = 25,
	KICKPLAYER_CODE = 26,
	HASEVERYONEFINISHED_CODE = 27
};

struct UserPointsResponse
{
	string points;
};

struct LoginResponse 
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	string message;
};

struct LoginRequest 
{
	string username;
	string password;
};

struct SignupRequest 
{
	string username;
	string password;
	string email;
};

struct RequestInfo
{
	int codeID;
	std::time_t recevialTime;
	std::vector<unsigned char> buffer;
};

struct RequestResult
{
	std::vector<unsigned char> buffer;
	IRequestHandler* newHandler;
};


struct LogoutResponse
{
	unsigned int status;
};

struct GetRoomsResponse 
{
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct getHighScoreResponse 
{
	unsigned int status;
	std::vector<string> statistics;
};

struct getPersonalStatsResponse 
{
	unsigned int status;
	std::vector<string> statistics;
};

struct JoinRoomResponse {
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
};

struct CreateRoomRequest 
{
	string category;
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
	string privacy;
};

struct JoinRoomRequest 
{
	unsigned int roomID;
};

struct CloseRoomResponse 
{
	unsigned int status;
};

struct StartGameResponse 
{
	unsigned int status;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct ReadyUnreadyResponse
{
	unsigned int status;
};

struct ReadyUnreadyRequest
{
	bool readyOrNot;
};

struct GetRoomStateResponse 
{
	unsigned int status;
	bool hasGameBegun;
	std::map<LoggedUser*, string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct GetOwnerRoomRequest
{
	string username;
};

struct GetOwnerRoomResponse
{
	Room* room;
};

struct RefreshResponse 
{
	unsigned int status;
};

struct GetPlayersInRoomRequest
{
	int roomID;
};

struct GetPlayersInRoomResponse
{
	std::map<LoggedUser*, string> users;
};

struct HasApprovedResponse
{
	int approvedCode;
};

struct StopWaitingResponse
{
	bool succeded;
};

struct GetWaitingToApproveListResponse
{
	std::vector<LoggedUser*> users;
};

struct ApprovedUserRequest
{
	string username;
	bool approved;
};

struct ApprovedUserResponse
{
	int status;
};

struct KickPlayerRequest
{
	string username;
};

struct KickPlayerResponse
{
	bool succcess;
};

struct GetQuestionResponse
{
	int status;
	string question;
	std::map<unsigned int, string> answers;
};

struct SubmitAnswerResponse
{
	int status;
	unsigned int correctAnswerId;
};

struct PlayerResults
{
	string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct GetGameResultsResponse
{
	int status;
	std::vector<PlayerResults> results;
};

struct SubmitAnswerRequest 
{
	int answerId;
};

struct GameData 
{
	Question* currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averangeAnswerTime;
	int questionIndex;
	bool hasAnsweredLastQuestion;
};

struct GetTimePerQuestionResponse 
{
	int timePerQuestion;
};

struct LeaveGameResponse
{
	int status;
};

struct HasEveryoneFinishedResponse
{
	bool hasEveryoneFinished;
};