#pragma once
#include "StructsHeader.h"
#include "RoomManager.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(RoomManager& roomManager, RequestHandlerFactory& handlerFactory, LoggedUser user, Room* room);
	bool isRequestRelevant(RequestInfo information) override;
	RequestResult handleRequest(RequestInfo information) override;
	Room* getRoom();
	void addToList(LoggedUser* user);
	int hasUserBeenApproved(LoggedUser* user) const;
	void removeUser(string username);
	virtual void handleProgramClosed() override;


private:
	RequestResult startGame();
	RequestResult closeRoom();
	RequestResult getRoomState();
	RequestResult getOwnerRoom();
	RequestResult GetPlayers();
	RequestResult GetWaitingUsers();
	RequestResult ApproveUser(RequestInfo information);
	RequestResult KickPlayer(RequestInfo information);

	void disapproveUser(string username);
	void addUser(string username);

	Room* m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	std::vector<LoggedUser*> m_usersWaiting;
	std::vector<LoggedUser*> m_notApprovedUsers;
};

