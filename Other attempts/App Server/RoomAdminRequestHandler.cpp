#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RoomManager& roomManager, RequestHandlerFactory& handlerFactory, LoggedUser user, Room* room) : m_roomManager(roomManager), m_handlerFactory(handlerFactory), m_user(user), m_room(room)
{}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo information)
{
    return information.codeID == CLOSEROOM_CODE || information.codeID == GETROOMSTATE_CODE || information.codeID == STARTGAME_CODE;
}
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo information)
{
    RequestResult requestResult;

    int code = information.codeID;
    switch (code)
    {
    case ERROR_CODE:
        handleProgramClosed();
        requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
        break;

    case CLOSEROOM_CODE:
        requestResult = closeRoom();
        break;

    case GETROOMSTATE_CODE:
        requestResult = getRoomState();
        break;

    case STARTGAME_CODE:
        requestResult = startGame();
        break;

    case GETOWNERROOM_CODE:
        requestResult = getOwnerRoom();
        break;

    case PLAYERSINROOM_CODE:
        requestResult = GetPlayers();
        break;

    case GETWAITINGTOAPPROVELIST_CODE:
        requestResult = GetWaitingUsers();
        break;

    case APPROVEUSER_CODE:
        requestResult = ApproveUser(information);
        break;

    case KICKPLAYER_CODE:
        requestResult = KickPlayer(information);
        break;
    }

    return requestResult;
}

Room* RoomAdminRequestHandler::getRoom()
{
    return m_room;
}

void RoomAdminRequestHandler::addToList(LoggedUser* user)
{
    m_usersWaiting.push_back(user);
}

int RoomAdminRequestHandler::hasUserBeenApproved(LoggedUser* user) const
{
    for (int i = 0; i < m_usersWaiting.size(); i++)
    {
        if (m_usersWaiting[i] == user)
            return -1;
    }
    for (int i = 0; i < m_notApprovedUsers.size(); i++)
    {
        if (*m_notApprovedUsers[i] == user)
            return fail;
    }
    return success;
}

void RoomAdminRequestHandler::removeUser(string username)
{
    for (auto it = m_usersWaiting.begin(); it != m_usersWaiting.end(); ++it)
    {
        if ((*it)->getUserName() == username)
        {
            m_usersWaiting.erase(it);
            break; // Exit the loop after erasing the user
        }
    }
}

void RoomAdminRequestHandler::handleProgramClosed()
{
    closeRoom();
}

RequestResult RoomAdminRequestHandler::startGame()
{
    RequestResult requestResult;

    m_roomManager.getRoom(m_room->getMetaData().id)->setActive(1);
    Room* room = m_roomManager.getRoom(m_room->getMetaData().id);

    //New handler
    requestResult.newHandler = new GameRequestHandler(m_handlerFactory, &m_user, room);
    StartGameResponse response;
    response.status = 1;
    requestResult.buffer = JsonResponsePacketSerializer::serializeStartGameResponse(response);

    return requestResult;
}

RequestResult RoomAdminRequestHandler::closeRoom()
{
    RequestResult requestResult;
    CloseRoomResponse response;

    m_roomManager.deleteRoom(m_room->getMetaData().id);
    m_roomManager.deleteRoom(this);
    response.status = success;
    requestResult.buffer = JsonResponsePacketSerializer::serializeCloseRoomResponse(response);
    requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
    return requestResult;
}

RequestResult RoomAdminRequestHandler::getRoomState()
{
    RequestResult requestResult;
    GetRoomStateResponse roomState;

    roomState.answerTimeout = m_room->getMetaData().timePerQuestion;
    roomState.hasGameBegun = m_room->getMetaData().isActive;
    roomState.players = m_room->getAllUsers();
    roomState.questionCount = m_room->getMetaData().numOfQuestionsInGame;

    roomState.status = success;
    requestResult.buffer = JsonResponsePacketSerializer::serializeGetRoomStateRespone(roomState);

    requestResult.newHandler = this;

    return requestResult;
}

RequestResult RoomAdminRequestHandler::getOwnerRoom()
{
    GetOwnerRoomResponse response;
    RequestResult result;
    response.room = m_room;
    result.buffer = JsonResponsePacketSerializer::serializeGetOwnerRoomResponse(response);
    result.newHandler = this;
    return result;
}

RequestResult RoomAdminRequestHandler::GetPlayers()
{
    GetPlayersInRoomResponse response;
    RequestResult result;
    response.users = m_room->getAllUsers();
    result.buffer = JsonResponsePacketSerializer::serializeGetPlayersInRoomResponse(response);
    result.newHandler = this;
    return result;
}

RequestResult RoomAdminRequestHandler::GetWaitingUsers()
{
    RequestResult result;
    GetWaitingToApproveListResponse response;
    response.users = m_usersWaiting;
    result.newHandler = this;
    result.buffer = JsonResponsePacketSerializer::serializeGetWaitingUsersResponse(response);
    return result;
}

RequestResult RoomAdminRequestHandler::ApproveUser(RequestInfo information)
{
    RequestResult result;
    ApprovedUserRequest request = JsonRequestPacketDeserializer::deserializeApprovedUserRequest(information.buffer);
    if (request.approved && !m_room->isFull())
    {
        addUser(request.username);
    }
    else
    {
        disapproveUser(request.username);
    }
    ApprovedUserResponse response;
    response.status = 1;
    result.buffer = JsonResponsePacketSerializer::serializeApproveUserResponse(response);
    removeUser(request.username);
    result.newHandler = this;
    return result;
}

RequestResult RoomAdminRequestHandler::KickPlayer(RequestInfo information)
{
    RequestResult requestResult;
    KickPlayerResponse response;
    KickPlayerRequest kickRequest = JsonRequestPacketDeserializer::deserializeKickPlayerRequest(information.buffer);
    try
    {
        disapproveUser(kickRequest.username);
        response.succcess = true;
    }
    catch(...)
    {
        response.succcess = false;
    }
    requestResult.buffer = JsonResponsePacketSerializer::serializeKickPlayerResponse(response);
    requestResult.newHandler = this;
    return requestResult;
}

void RoomAdminRequestHandler::disapproveUser(string username)
{
    for (LoggedUser* user : m_usersWaiting)
    {
        if (user->getUserName() == username)
        {
            m_notApprovedUsers.push_back(user);
            return;
        }
    }
    for (auto it : m_room->getAllUsers())
    {
        if (it.first->getUserName() == username)
        {
            m_notApprovedUsers.push_back(it.first);
            m_room->removeUser(it.first);
            return;
        }
    }

}

void RoomAdminRequestHandler::addUser(string username)
{
    for (LoggedUser* user : m_usersWaiting)
    {
        if (user->getUserName() == username)
            m_room->addUser(user);
    }
}
