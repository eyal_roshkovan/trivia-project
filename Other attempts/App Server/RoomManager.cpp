#include "RoomManager.h"
#include "LoggedUser.h"
#include "Room.h"
#include "RoomAdminRequestHandler.h"


void RoomManager::createRoom(LoggedUser* user, RoomData data)
{
    Room* newRoom = new Room(data, user);
    m_rooms[data.id] = newRoom;
}

void RoomManager::deleteRoom(int id)
{
    // Find the room with the specified id
    auto it = m_rooms.find(id);

    if (it != m_rooms.end()) {
        // Remove the room from the map
        m_rooms.erase(it);
    }
}

unsigned int RoomManager::getRoomState(int id)
{
    if (m_rooms.count(id) == 1)
        return m_rooms[id]->getMetaData().isActive;

    return 0;
}

std::vector<RoomData> RoomManager::getRooms()
{
    std::vector<RoomData> roomsData;
    for (const auto& roomPair : m_rooms)
    {
        try
        {
            if (roomPair.second->getMetaData().isActive == 0)
                roomsData.push_back(roomPair.second->getMetaData());
        }
        catch (...) {}
    }

    return roomsData;
}

Room* RoomManager::getRoom(int id)
{
    Room* temp = m_rooms[id];
    return temp;
}

unsigned int RoomManager::getFreeId()
{
    unsigned int closestFreeId = 1;

    for (const auto& pair : m_rooms) {
        if (pair.first > closestFreeId) {
            break;
        }
        closestFreeId++;
    }

    return closestFreeId;
}

void RoomManager::addRoomAdmin(RoomAdminRequestHandler* admin)
{
    m_room_admins.push_back(admin);
}

RoomAdminRequestHandler& RoomManager::getRoom(Room* room)
{
    for (int i = 0; i < m_room_admins.size(); i++)
    {
        if (m_room_admins[i]->getRoom() == room)
            return *m_room_admins[i];
    }
    throw std::exception("couldn't find");
}

void RoomManager::deleteRoom(RoomAdminRequestHandler* admin)
{
    // Find the iterator pointing to the admin in the vector
    auto it = std::find(m_room_admins.begin(), m_room_admins.end(), admin);

    if (it != m_room_admins.end()) {
        // Remove the admin from the vector
        m_room_admins.erase(it);
    }

    m_rooms.erase(admin->getRoom()->getMetaData().id);
}
