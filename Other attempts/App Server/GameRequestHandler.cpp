#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory& factory, LoggedUser* user, Room* room) :
    m_handlerFactory(factory),
    m_gameManager(factory.getGameManager()),
    m_user(user)
{
    m_game = m_gameManager.createGame(room);
    m_gameManager.incGamesInTable(m_user);

}

bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.codeID == GETGAMERESULT_CODE ||
        requestInfo.codeID == LEAVEGAME_CODE ||
        requestInfo.codeID == SUBMITANSWER_CODE ||
        requestInfo.codeID == GETQUESTION_CODE;
}

RequestResult GameRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult requestResult;

    switch (requestInfo.codeID)
    {
    case ERROR_CODE:
        handleProgramClosed();
        requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, m_user);
        break;
    case (GETGAMERESULT_CODE):
        requestResult = getGameResults(requestInfo);
        break;
    case (LEAVEGAME_CODE):
        requestResult = leaveGame(requestInfo);
        break;
    case (SUBMITANSWER_CODE):
        requestResult = submitAnswer(requestInfo);
        break;
    case (GETQUESTION_CODE):
        requestResult = getQuestion(requestInfo);
        break;
    case (HASEVERYONEFINISHED_CODE):
        requestResult = hasEveryoneFinished(requestInfo);
        break;

    }

    return requestResult;
}

void GameRequestHandler::handleProgramClosed()
{
    m_gameManager.removePlayer(m_user, m_game);
}

RequestResult GameRequestHandler::hasEveryoneFinished(RequestInfo requestInfo)
{
    RequestResult requestResult;
    HasEveryoneFinishedResponse response;

    response.hasEveryoneFinished = m_gameManager.hasEveryoneFinished(m_game);
    requestResult.buffer = JsonResponsePacketSerializer::serializeHasEveryoneFinishedResponse(response);
    requestResult.newHandler = this;
    return requestResult;
}

RequestResult GameRequestHandler::getQuestion(RequestInfo requestInfo)
{
    RequestResult requestResult; 

    Question* question = m_gameManager.getQuestionForUserAndGame(m_user, m_game);
    GetQuestionResponse response;
    if (question != nullptr) 
    {
        response.question = question->getQuestion();

        //Convert vector to map
        std::map<unsigned int, std::string> mapPossibleAnswers;
        unsigned int index = 1;
        std::vector<string> vectorPossibleAnswers = question->getPossibleAnswers();
        for (const auto& str : vectorPossibleAnswers) {
            mapPossibleAnswers[index] = str;
            index++;
        }
        response.answers = mapPossibleAnswers;
        response.status = 1;
    }
    else 
    {
        response.status = 0;
        response.question = "";
        response.answers = std::map<unsigned int, string>();
    }

    requestResult.buffer = JsonResponsePacketSerializer::serializeGetQuestionResponse(response);
    requestResult.newHandler = this;

    return requestResult;
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo requestInfo)
{
    RequestResult requestResult;

    SubmitAnswerResponse response;
    SubmitAnswerRequest request = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(requestInfo.buffer);

    response.correctAnswerId = m_gameManager.submitAnswer(m_user, m_game, request.answerId);

    requestResult.buffer = JsonResponsePacketSerializer::serializeSubmitAnswerResponse(response);
    requestResult.newHandler = this;

    return requestResult;
}

RequestResult GameRequestHandler::getGameResults(RequestInfo requestInfo)
{
    RequestResult requestResult;

    GetGameResultsResponse response;
    response.status = success;
    response.results = m_gameManager.getGameResultsOfGame(m_game, response.status);

    requestResult.buffer = JsonResponsePacketSerializer::serializeGetGameResultsResponse(response);
    requestResult.newHandler = this;

    return requestResult;
}

RequestResult GameRequestHandler::leaveGame(RequestInfo requestInfo)
{
    RequestResult requestResult;

    LeaveGameResponse response;
    m_gameManager.removePlayer(m_user, m_game);
    response.status = 1;

    requestResult.buffer = JsonResponsePacketSerializer::serializeLeaveGameResponse(response);
    requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, m_user);

    return requestResult;
}