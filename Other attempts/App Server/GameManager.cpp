#include "GameManager.h"

GameManager::GameManager(IDatabase* db) : m_database(db)
{
}

Game* GameManager::createGame(Room* room)
{
    //If game already exist return the game if not create a new game
    for (Game* game : m_games)
    {
        if (game->getId() == room->getMetaData().id)
            return game;
    }
    Game* newGame = new Game(*room, m_database);
    m_games.push_back(newGame);

    

    return newGame;
}

void GameManager::deleteGame(int gameId)
{
    m_games.erase(std::remove_if(m_games.begin(), m_games.end(), [gameId](const Game* game) {
        return game->getId() == gameId;
        }), m_games.end());

}

Question* GameManager::getQuestionForUserAndGame(LoggedUser* user, Game* game)
{
    for (Game* gameToSearch : m_games)
    {
        if (gameToSearch->getId() == game->getId())
        {
            return gameToSearch->getQuestionForUser(*user);
        }
   }
}

std::vector<PlayerResults> GameManager::getGameResultsOfGame(Game* game, int& status)
{
    for (Game* gameToSearch : m_games)
    {
        if (gameToSearch->getId() == game->getId())
        {
            return gameToSearch->getGameResults(status);
        }

    }
}

bool GameManager::removePlayer(LoggedUser* usertoRemove, Game* game)
{
    try {
        for (Game* gameToSearch : m_games)
        {
            if (gameToSearch->getId() == game->getId())
            {
                gameToSearch->removePlayer(*usertoRemove);
            }
        }

        return true;
    }
    catch (...)
    {
        return false;
    }
}

int GameManager::submitAnswer(LoggedUser* user, Game* game, int answerId)
{
    for (Game* gameToSearch : m_games)
    {
        if (gameToSearch->getId() == game->getId())
        {
            return gameToSearch->submitAnswer(*user, answerId);
        }
    }
}

void GameManager::incGamesInTable(LoggedUser* user)
{
    m_database->incrementCountOfGames(user->getUserName());
}

bool GameManager::hasEveryoneFinished(Game* game)
{
    for (Game* gameToSearch : m_games)
    {
        if (gameToSearch->getId() == game->getId())
        {
            return gameToSearch->hasEveryoneFinished();
        }
    }
}
