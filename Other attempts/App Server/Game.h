#pragma once
#include "StructsHeader.h"
#include "IDatabase.h"
#include <algorithm>

class Game
{
public:
	Game(Room& room, IDatabase* db);
	Question* getQuestionForUser(LoggedUser user);
	int submitAnswer(LoggedUser user, int answerId);
	void removePlayer(LoggedUser user);
	int getId() const;
	void InitializationUsers();
	std::vector<PlayerResults> getGameResults(int& status);
	int getTimePerQuestion();
	bool hasEveryoneFinished() const;
	Game& operator=(const Game& other);

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
	Room& m_room;
	int m_gameId;
	IDatabase* m_database;
	void insertAllQuestions();
};

