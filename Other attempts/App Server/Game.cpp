#include "Game.h"

Game::Game(Room& room, IDatabase* db) : m_room(room), m_database(db)
{
    for (auto it : m_players)
    {
        it.second.hasAnsweredLastQuestion = false;
    }

    start = std::chrono::high_resolution_clock::now();
    m_gameId = room.getMetaData().id;
	insertAllQuestions();
    InitializationUsers();
}

Question* Game::getQuestionForUser(LoggedUser user)
{
    // Check if the user is in the game
    if (m_players.find(user) == m_players.end()) 
    {
        return nullptr;
    }

    // Get the current question for the user
    GameData& userData = m_players[user];
    Question* currentQuestion = userData.currentQuestion;
    
    userData.questionIndex++;

    if (!userData.hasAnsweredLastQuestion && userData.questionIndex != 1)
    {
        userData.wrongAnswerCount++;
    }

    if (currentQuestion == nullptr)
    {
        return nullptr;
    }
    

    if (userData.questionIndex == m_questions.size())
    {
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(end - start);
        int durationInt = static_cast<int>(duration.count());
        m_players[user].averangeAnswerTime = durationInt / m_room.getMetaData().numOfQuestionsInGame;
        userData.currentQuestion = nullptr;
    }
    else
    {
        userData.currentQuestion = &m_questions[userData.questionIndex];
    }
    userData.hasAnsweredLastQuestion = false;
    return currentQuestion;
}

int Game::submitAnswer(LoggedUser user, int answerId)
{
    // Check if the user exist
    if (m_players.find(user) == m_players.end()) 
        return 0;

    m_database->incrementNumberOfQuestionAnswered(user.getUserName());

    GameData& userData = m_players[user];
    
    //Check if user has right
    int correctAnswerId = m_questions[userData.questionIndex - 1].getCorrectAnswerId();
    
    if (answerId == correctAnswerId)
    {
        userData.correctAnswerCount++;
        m_database->incrementCorrectQuestionsAnswered(user.getUserName());
    }
    else 
    {
        userData.wrongAnswerCount++;
    }

    userData.hasAnsweredLastQuestion = true;

    return correctAnswerId;
}

void Game::removePlayer(LoggedUser user)
{
    // Check if the user exist
    user.deleteOwnedRoom();
    if (m_players.find(user) == m_players.end()) 
    {
        // User not found in the game
        return;
    }
    
    m_players.erase(user);

    //Update is statistics table 
}
 
int Game::getId() const
{
    return m_gameId;
}

void Game::InitializationUsers()
{
    for (const auto& player : m_room.getAllUsers()) 
    {
        GameData userData;
        userData.currentQuestion = &m_questions[0];
        userData.correctAnswerCount = 0;
        userData.wrongAnswerCount = 0;
        userData.averangeAnswerTime = 0;
        userData.questionIndex = 0;
        m_players[*player.first] = userData;
    }
}

std::vector<PlayerResults> Game::getGameResults(int& status)
{
    std::vector<PlayerResults> results;

    if (!hasEveryoneFinished())
    {
        status = 1;
        return results;
    }

    for (const auto& player : m_players) 
    {
        const LoggedUser& user = player.first;
        const GameData& gameData = player.second;

        // Create a PlayerResults object and populate its fields
        PlayerResults playerResults;
        playerResults.username = user.getUserName();
        playerResults.correctAnswerCount = gameData.correctAnswerCount;
        playerResults.wrongAnswerCount = gameData.wrongAnswerCount;
        playerResults.averageAnswerTime = static_cast<unsigned int>(gameData.averangeAnswerTime);

        // Add the PlayerResults object to the results vector
        results.push_back(playerResults);
    }

    return results;
}

int Game::getTimePerQuestion()
{
    return m_room.getMetaData().timePerQuestion;
}

bool Game::hasEveryoneFinished() const
{
    for (auto it : m_players)
    {
        if (it.second.correctAnswerCount + it.second.wrongAnswerCount != m_room.getMetaData().numOfQuestionsInGame)
            return false;
    }
    return true;
}

Game& Game::operator=(const Game& other)
{
    if (this != &other) {
        m_questions = other.m_questions;
        m_players = other.m_players;
        m_room = other.m_room;
        m_gameId = other.m_gameId;
        m_database = other.m_database;
    }
    return *this;
}

void Game::insertAllQuestions()
{
	std::vector<Question> questions = m_database->getQuestionsByCategoryAndNumber(m_room.getMetaData().category, m_room.getMetaData().numOfQuestionsInGame);

	for (const Question& question : questions)
	{
		m_questions.push_back(question);
	}

}
