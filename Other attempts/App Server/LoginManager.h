#pragma once
#include "IDatabase.h"
#include <vector>
#include "LoggedUser.h"
#include <mutex>
#include <iterator>
#include <algorithm>

class LoginManager
{
public:
	LoginManager(IDatabase* database);
	~LoginManager();

	bool signup(string userName, string password, string email);
	bool login(string userName, string password);
	void logout(string userName);
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
	std::mutex _loggedUserLock;
};

