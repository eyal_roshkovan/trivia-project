#pragma once

#include "Room.h"
#include <map>
#include <vector>

class LoggedUser; // Forward declaration
struct RoomData; // Forward declaration
class RoomAdminRequestHandler;

class RoomManager
{
public:
    void createRoom(LoggedUser* user, RoomData data);
    void deleteRoom(int id);
    unsigned int getRoomState(int id);
    std::vector<RoomData> getRooms();
    Room* getRoom(int id);
    unsigned int getFreeId();
    void addRoomAdmin(RoomAdminRequestHandler* admin);
    RoomAdminRequestHandler& getRoom(Room* room);
    void deleteRoom(RoomAdminRequestHandler* admin);

private:
    std::map<unsigned int, Room*> m_rooms;
    std::vector<RoomAdminRequestHandler*> m_room_admins;
};
