#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Helper.h"

#include <chrono>
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <string>
#include <exception>
#include <vector>
#include <fstream>
#include <mutex>
#include <map>

class Communicator
{
public:
	Communicator(RequestHandlerFactory& handleFactory);
	~Communicator();
	void bindAndListen();
		
private:
	void handleNewClient(SOCKET clientSocket);
	RequestInfo handleNewMessage(SOCKET clientSocket);
	void client_accept();

	RequestHandlerFactory& m_handlerFactory;

	std::map<SOCKET, IRequestHandler*> _mapSockets;
	SOCKET _serverSocket;
	std::mutex _mapSocketLock;
};

