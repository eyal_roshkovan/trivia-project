#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoggedUser.h"

class IRequestHandler;
class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory& factory, LoggedUser* user, Room* room);
	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;
	virtual void handleProgramClosed() override;

private:
	LoggedUser* m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFactory;
	Game* m_game;

	RequestResult hasEveryoneFinished(RequestInfo requestInfo);
	RequestResult getQuestion(RequestInfo requestInfo);
	RequestResult submitAnswer(RequestInfo requestInfo);
	RequestResult getGameResults(RequestInfo requestInfo);
	RequestResult leaveGame(RequestInfo requestInfo);
};

