#include "Server.h"

Server::Server()
	:m_database(new SqliteDatabase()),
	m_handleFactory(m_database),
	m_communicator(m_handleFactory)
{
	m_database->open();
}

void Server::run()
{
	m_communicator.bindAndListen();
}