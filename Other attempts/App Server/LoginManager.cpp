#include "LoginManager.h"


LoginManager::LoginManager(IDatabase* database): m_database(database)
{
	m_database->open();
}

LoginManager::~LoginManager()
{
	m_database->close();
}

bool LoginManager::signup(string userName, string password, string email)
{
	int res = m_database->addNewUser(userName, password, email);
	if (res == 0)
		return false;
	int ans = m_database->addNewUserStatistics(userName);
	LoggedUser newUser(userName, 0);
	std::unique_lock<std::mutex> lock(_loggedUserLock);
	m_loggedUsers.push_back(newUser);
	return true;
}

bool LoginManager::login(string userName, string password)
{
	int res = m_database->doesPasswordMatch(userName, password);
	if (res == 0)
		return false;
	
	for (LoggedUser& user : m_loggedUsers)
	{
		if (user.getUserName() == userName)
			return false;
	}
	
	LoggedUser newUser(userName, m_database->getPlayerScore(userName));
	std::unique_lock<std::mutex> lock(_loggedUserLock);
	m_loggedUsers.push_back(newUser);
	return true;
}

void LoginManager::logout(string userName)
{
	auto it = m_loggedUsers.begin();
	while (it != m_loggedUsers.end())
	{
		if (userName == it->getUserName())
		{
			std::unique_lock<std::mutex> lock(_loggedUserLock);
			it = m_loggedUsers.erase(it);
			cout << "User " << userName << " logged out." << endl;
			return;
		}
		else
			++it;
	}
}
