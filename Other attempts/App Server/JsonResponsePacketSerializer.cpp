#include "JsonResponsePacketSerializer.h"

std::vector<unsigned char> JsonResponsePacketSerializer::serializeLoginResponse(LoginResponse loginResponse)
{
    json data;

    data["status"] = loginResponse.status;

    return convert_into_shape(data, LOGIN_CODE);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeSignUpResponse(SignupResponse signUpResponse)
{
    json data;

    data["status"] = signUpResponse.status;

    return convert_into_shape(data, SIGNUP_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeErrorResponse(ErrorResponse errorResponse)
{
    json data;

    data["message"] = errorResponse.message;

    return convert_into_shape(data, ERROR_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeLogoutResponse(LogoutResponse logoutResponse)
{
    json data;
 
    data["status"] = logoutResponse.status;

    return convert_into_shape(data, LOGOUT_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeJoinRoomResponse(JoinRoomResponse joinRoomResponse)
{
    json data;

    data["status"] = joinRoomResponse.status;

    return convert_into_shape(data, JOINROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeCreateRoomResponse(CreateRoomResponse createRoomResponse)
{
    json data;

    data["status"] = createRoomResponse.status;

    return convert_into_shape(data, CREATEROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetRoomResponse(GetRoomsResponse getRoomsResponse)
{
    json data;

    string rooms = "";
    for (RoomData& roomData : getRoomsResponse.rooms)
    {
        rooms += roomData.name;
        rooms += ", ";
        rooms += roomData.roomOwner;
        rooms += ", ";
        rooms += roomData.category;
        rooms += ", ";
        rooms += roomData.isPublic;
        rooms += ", ";
        rooms += std::to_string(roomData.maxPlayers);
        rooms += ", ";
        rooms += std::to_string(roomData.currentAmountOfPlayers);
        rooms += ", ";
        rooms += std::to_string(roomData.id);
        rooms += ", ";
        rooms += std::to_string(roomData.numOfQuestionsInGame);
        rooms += ", ";
        rooms += std::to_string(roomData.timePerQuestion);
        rooms += "\n";
    }
    if (!rooms.empty())
        rooms.erase(rooms.size() - 1);

    data["status"] = getRoomsResponse.status;
    data["Rooms"] = rooms;

    return convert_into_shape(data, GETROOMS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetPlayersInRoomResponse(GetPlayersInRoomResponse playerInRoomResponse)
{
    json data;

    string players = "";
    std::map<LoggedUser, string>::iterator it;
    for (auto& it : playerInRoomResponse.users)
    {
        players += it.first->getUserName();
        players += ", ";
        players += it.second;
        players += ", ";
        players += std::to_string(it.first->getTotalPoints());
        players += ", ";
        players += std::to_string(it.first->getIsReady());
        players += "\n";
    }
    if (!players.empty())
        players.erase(players.size() - 1);

    data["PlayersInRoom"] = players;

    return convert_into_shape(data, PLAYERSINROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeHighScoreResponse(getHighScoreResponse highScoreResponse)
{
    json data;

    string highscore = "";
    for (string& roomData : highScoreResponse.statistics)
    {
        highscore += roomData;
        highscore += ",";
    }
    if (!highscore.empty())
        highscore.erase(highscore.size() - 1);

    data["status"] = highScoreResponse.status;
    data["HighScores"] = highscore;

    return convert_into_shape(data, HIGHSCORE_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializePersonalStats(getPersonalStatsResponse personalStatsResponse)
{
    json data;
 
    string s = "";
    for (string& stats : personalStatsResponse.statistics)
    {
        s += stats;
        s += ",";
    }
    if (!s.empty())
        s.erase(s.size() - 1);

    data["status"] = personalStatsResponse.status;
    data["statistics"] = s;

    return convert_into_shape(data, PERSONALSTATISTICS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeCloseRoomResponse(CloseRoomResponse closeRoomResponse)
{
    json data;

    data["status"] = closeRoomResponse.status;

    return convert_into_shape(data, CLOSEROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeStartGameResponse(StartGameResponse startGameResponse)
{
    json data;
     
    data["status"] = startGameResponse.status;

    return convert_into_shape(data, STARTGAME_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeLeaveRoomResponse(LeaveRoomResponse leaveRoomResponse)
{ 
    json data;
 
    data["status"] = leaveRoomResponse.status;

    return convert_into_shape(data, CREATEROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetRoomStateRespone(GetRoomStateResponse getRoomStateResponse)
{
    std::map<LoggedUser*, string>::iterator it;
 
    json data;
 
    string playersData;
    for (it = getRoomStateResponse.players.begin(); it != getRoomStateResponse.players.end(); ++it)
    {
        playersData += it->first->getUserName() + ", ";
        playersData += it->second + ", ";
        playersData += it->first->getTotalPoints() + ", ";
        playersData += "\n";
    }
    data["status"] = getRoomStateResponse.status;
    data["hasGameBegun"] = getRoomStateResponse.hasGameBegun;
    data["players"] = playersData;
    data["answerCount"] = getRoomStateResponse.questionCount;
    data["answerTimeOut"] = getRoomStateResponse.answerTimeout;


    return convert_into_shape(data, PERSONALSTATISTICS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeReadyUnreadyResponse(ReadyUnreadyResponse readyUnreadyResponse)
{
    json data;
 
    data["status"] = readyUnreadyResponse.status;

    return convert_into_shape(data, READYNOTREADY_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRefreshResponse(RefreshResponse refreshResponse)
{   
    json data;

    data["status"] = refreshResponse.status;

    return convert_into_shape(data, REFRESH_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeUserPointsResponse(UserPointsResponse userPointsResponse)
{
    json data;

    data["points"] = userPointsResponse.points;

    return convert_into_shape(data, USERPOINTS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetOwnerRoomResponse(GetOwnerRoomResponse getOwnerRoomResponse)
{
    json data;
 
    RoomData roomData = getOwnerRoomResponse.room->getMetaData();
    string rooms = "";
    rooms += roomData.name;
    rooms += ", ";
    rooms += roomData.roomOwner;
    rooms += ", ";
    rooms += roomData.category;
    rooms += ", ";
    rooms += roomData.isPublic;
    rooms += ", ";
    rooms += std::to_string(roomData.maxPlayers);
    rooms += ", ";
    rooms += std::to_string(roomData.currentAmountOfPlayers);
    rooms += ", ";
    rooms += std::to_string(roomData.id);
    rooms += ", ";
    rooms += std::to_string(roomData.numOfQuestionsInGame);
    rooms += ", ";
    rooms += std::to_string(roomData.timePerQuestion);
    rooms += "  ";

    if (!rooms.empty())
        rooms.erase(rooms.size() - 1);

    data["Room"] = rooms;

    return convert_into_shape(data, GETOWNERROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeHasApprovedResponse(HasApprovedResponse hasApprovedResponse)
{
    json data;

    data["approved"] = hasApprovedResponse.approvedCode;

    return convert_into_shape(data, HASAPPROVED_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeStopWaitingResponse(StopWaitingResponse stopWaitingResponse)
{
    json data;
 
    data["succeded"] = stopWaitingResponse.succeded;

    return convert_into_shape(data, STOPWAITING_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetWaitingUsersResponse(GetWaitingToApproveListResponse getWaitingToApproveListResponse)
{
    json data;
 
    string players = "";
    std::map<LoggedUser, string>::iterator it;
    for (auto& it : getWaitingToApproveListResponse.users)
    {
        players += it->getUserName();
        players += ", ";
        players += "Member";
        players += ", ";
        players += std::to_string(it->getTotalPoints());
        players += "\n";
    }
    if (!players.empty())
        players.erase(players.size() - 1);

    data["WaitingPlayers"] = players;

    return convert_into_shape(data, PLAYERSINROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeApproveUserResponse(ApprovedUserResponse approvedUserResponse)
{
    json data;

    data["status"] = approvedUserResponse.status;

    return convert_into_shape(data, APPROVEUSER_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeKickPlayerResponse(KickPlayerResponse kickPlayerResponse)
{
    json data;
 
    data["succeded"] = kickPlayerResponse.succcess;

    return convert_into_shape(data, KICKPLAYER_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetGameResultsResponse(GetGameResultsResponse getGameResultsResponse)
{
    json data;
 
    data["status"] = getGameResultsResponse.status;

    // Convert results vector to JSON list
    string playersResults = "";
    for (const PlayerResults& playerResults : getGameResultsResponse.results)
    {
        playersResults += playerResults.username;
        playersResults += ", ";
        playersResults += std::to_string(playerResults.correctAnswerCount);
        playersResults += ", ";
        playersResults += std::to_string(playerResults.wrongAnswerCount);
        playersResults += ", ";
        playersResults += std::to_string(playerResults.averageAnswerTime);
        playersResults += "\n";
    }
    data["results"] = playersResults;
    return convert_into_shape(data, GETGAMERESULT_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeSubmitAnswerResponse(SubmitAnswerResponse submitAnswerResponse)
{
    json data;
 
    data["status"] = submitAnswerResponse.status;

    return convert_into_shape(data, SUBMITANSWER_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeGetQuestionResponse(GetQuestionResponse getQuestionResponse)
{
    json data;
    
    data["status"] = getQuestionResponse.status;
    data["question"] = getQuestionResponse.question;
    data["answers"] = getQuestionResponse.answers;

    return convert_into_shape(data, GETQUESTION_CODE);

}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeLeaveGameResponse(LeaveGameResponse leaveGameResponse)
{
    json data;
    
    data["status"] = leaveGameResponse.status;

    return convert_into_shape(data, LEAVEGAME_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeHasEveryoneFinishedResponse(HasEveryoneFinishedResponse hasEveryoneFinishedResponse)
{
    json data;

    data["hasEveryoneFinished"] = hasEveryoneFinishedResponse.hasEveryoneFinished;

    return convert_into_shape(data, HASEVERYONEFINISHED_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::convert_into_shape(json data, int code)
{
    std::vector<unsigned char> buffer;
    string strBuffer;
    string strJson;

    strJson = data.dump();

    //Insert code 
    strBuffer = std::bitset<CODE_SIZE>(code).to_string(); //to binary

    //Insert len of data
    size_t jLen = 0;
    jLen = strJson.length();
    strBuffer += std::bitset<DATA_LEN_SIZE>(jLen).to_string();

    //Insert data
    for (char ch : strJson)
        strBuffer += std::bitset<8>(ch).to_string();

    for (char ch : strBuffer)
        buffer.push_back(ch);

    return buffer;
}