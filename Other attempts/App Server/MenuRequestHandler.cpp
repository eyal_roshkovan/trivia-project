#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory& factory, LoggedUser* user) : m_handlerFactory(factory),
                    m_statisticsManager(factory.getStatisticsManager()), m_roomManager(factory.getRoomManager()),
                    m_user(user)
{}

bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    std::vector<unsigned char> buffer = requestInfo.buffer;
    string strCode = "";

    for (int i = 0; i < CODE_SIZE; i++)
    {
        strCode += buffer[0];
        buffer.erase(buffer.begin() + 0);
    }

    std::bitset<CODE_SIZE> bits(strCode);
    int code = bits.to_ulong();

    return code >= JOINROOM_CODE && code <= HIGHSCORE_CODE;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult requestResult;

    std::vector<unsigned char> buffer = requestInfo.buffer;
    string strCode = "";

    for (int i = 0; i < CODE_SIZE; i++)
    {
        strCode += buffer[0];
        buffer.erase(buffer.begin() + 0);
    }

    std::bitset<CODE_SIZE> bits(strCode);
    int code = bits.to_ulong();
    requestResult.newHandler = this;
    switch (code)
    {
    case ERROR_CODE:
        handleProgramClosed();
        break;  

    case LOGOUT_CODE:
        requestResult = signout();
        break;
    
    case JOINROOM_CODE:
        requestResult = joinRoom(requestInfo);
        break;
    
    case CREATEROOM_CODE:
        requestResult = createRoom(requestInfo);
        break;
    
    case GETROOMS_CODE:
        requestResult = getRooms();
        break;
    
    case HIGHSCORE_CODE:
        requestResult = getHighScore();
        break;
    
    case PERSONALSTATISTICS_CODE:
        requestResult = getPersonalStats();
        break;
    
    case HASAPPROVED_CODE:
        requestResult = HasApproved();
        break;
    
    case STOPWAITING_CODE:
        requestResult = StopWaiting();
        break;

    default:
        break;
    }
    return requestResult;
}

void MenuRequestHandler::handleProgramClosed()
{
    if (m_user->getWaitingRoom() != nullptr) // if there's a waiting room
    {
        StopWaiting();
    }
    signout();
}

RequestResult MenuRequestHandler::getRooms()
{
    RequestResult requestResult;

    GetRoomsResponse rooms;
    rooms.status = 1;
    rooms.rooms = m_roomManager.getRooms();
    requestResult.buffer = JsonResponsePacketSerializer::serializeGetRoomResponse(rooms);
    requestResult.newHandler = this;


    return requestResult;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
    RequestResult requestResult;

    GetPlayersInRoomRequest request = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(requestInfo.buffer);
    
    GetPlayersInRoomResponse playersResponse;
    playersResponse.users = m_roomManager.getRoom(request.roomID)->getAllUsers();

    requestResult.buffer = JsonResponsePacketSerializer::serializeGetPlayersInRoomResponse(playersResponse);
    requestResult.newHandler = this;


    return requestResult;
}

RequestResult MenuRequestHandler::signout()
{
    RequestResult requestResult;
    LogoutResponse logout;
    
    m_handlerFactory.getLoginManager().logout(m_user->getUserName());
    
    logout.status = 1;
    requestResult.buffer = JsonResponsePacketSerializer::serializeLogoutResponse(logout);
    requestResult.newHandler = new LoginRequestHandler(m_handlerFactory);

    return requestResult;
}

RequestResult MenuRequestHandler::getHighScore()
{
    RequestResult requestResult;

    getHighScoreResponse highScore;
    highScore.statistics = m_statisticsManager.getHighScore();
    highScore.status = 1;
    requestResult.buffer = JsonResponsePacketSerializer::serializeHighScoreResponse(highScore);
    requestResult.newHandler = this;


    return requestResult;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
    RequestResult requestResult;
    JoinRoomResponse joinRoomResponse;

    JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);
    Room* room = m_roomManager.getRoom(joinRoomRequest.roomID);
    if (room->getMetaData().isPublic == "Private")
    {
        // ask to join room
        m_user->addRoom(&m_roomManager.getRoom(room));
        if (m_roomManager.getRoom(room).hasUserBeenApproved(m_user) != fail)
        {
            m_roomManager.getRoom(room).addToList(m_user); // That's the "catch", I need to figure out how to add the user without disturbing the owner.
        }
        joinRoomResponse.status = 0;    
        requestResult.newHandler = this;

    }
    else
    {
        if (m_roomManager.getRoom(room).hasUserBeenApproved(m_user) == fail)
        {
            joinRoomResponse.status = 0;
        }
        else
        {
            room->addUser(m_user);
            joinRoomResponse.status = 1;
        }
        //New Handler
        requestResult.newHandler = new RoomMemberRequestHandler(m_roomManager, m_handlerFactory, *m_user, room);
    }
    requestResult.buffer = JsonResponsePacketSerializer::serializeJoinRoomResponse(joinRoomResponse);
    return requestResult;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
    RequestResult requestResult;

    CreateRoomRequest newRoom = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);
    RoomData roomData;

    roomData.name = newRoom.roomName;
    roomData.maxPlayers = newRoom.maxUsers;
    roomData.timePerQuestion = newRoom.answerTimeout;
    roomData.numOfQuestionsInGame = newRoom.questionCount;
    roomData.isPublic = newRoom.privacy;
    roomData.category = newRoom.category;
    roomData.roomOwner = m_user->getUserName();
    roomData.id = m_roomManager.getFreeId();
    roomData.currentAmountOfPlayers = 1;
    roomData.isActive = 0;

    m_roomManager.createRoom(m_user, roomData);
    CreateRoomResponse roomResponse;
    roomResponse.status = 1;
    requestResult.buffer = JsonResponsePacketSerializer::serializeCreateRoomResponse(roomResponse);
    
    // New Handler
    RoomAdminRequestHandler* admin = new RoomAdminRequestHandler(m_roomManager, m_handlerFactory, *m_user, m_roomManager.getRoom(roomData.id));
    m_roomManager.addRoomAdmin(admin);
    requestResult.newHandler = admin;


    return requestResult;
}

RequestResult MenuRequestHandler::getPersonalStats()
{
    RequestResult requestResult;

    getPersonalStatsResponse response;
    response.statistics = m_statisticsManager.getUserStatistics(m_user->getUserName());
    response.status = 1;
    requestResult.buffer = JsonResponsePacketSerializer::serializePersonalStats(response);
    requestResult.newHandler = this;

    return requestResult;
}

RequestResult MenuRequestHandler::HasApproved()
{
    RequestResult requestResult;
    HasApprovedResponse response;
    if (m_roomManager.getRoom(m_user->getWaitingRoom()->getMetaData().id) == nullptr)
    {
        m_user->clearRoomList();
        requestResult.newHandler = this;
        response.approvedCode = 9;
        requestResult.buffer = JsonResponsePacketSerializer::serializeHasApprovedResponse(response);
        return requestResult;
    }
    bool approved = m_roomManager.getRoom(m_user->getWaitingRoom()).hasUserBeenApproved(m_user);
    if (std::find(m_notApprovedRooms.begin(), m_notApprovedRooms.end(), m_user->getWaitingRoom()) != m_notApprovedRooms.end())
    {
        requestResult.newHandler = this;
        response.approvedCode = fail;
        requestResult.buffer = JsonResponsePacketSerializer::serializeHasApprovedResponse(response);
        return requestResult;
    }
    response.approvedCode = m_roomManager.getRoom(m_user->getWaitingRoom()).hasUserBeenApproved(m_user);
    requestResult.buffer = JsonResponsePacketSerializer::serializeHasApprovedResponse(response);
    if (response.approvedCode == success)
    {
        requestResult.newHandler = new RoomMemberRequestHandler(m_roomManager, m_handlerFactory, *m_user, m_user->getWaitingRoom());
        m_user->clearRoomList();
    }
    else
    {
        if (response.approvedCode == fail)
        {
            m_notApprovedRooms.push_back(m_user->getWaitingRoom());
            m_user->clearRoomList();
        }
        requestResult.newHandler = this;
    }
    return requestResult;
}

// There's a bug with this feature.
RequestResult MenuRequestHandler::StopWaiting()
{
    RequestResult requestResult;
    StopWaitingResponse response;
    m_user->clearRoomList();
    response.succeded = success;
    requestResult.buffer = JsonResponsePacketSerializer::serializeStopWaitingResponse(response);
    requestResult.newHandler = this;
    return requestResult;
}
