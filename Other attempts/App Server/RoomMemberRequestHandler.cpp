#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(RoomManager& roomManager, RequestHandlerFactory& handlerFactory, LoggedUser user, Room* room) : m_handlerFactory(handlerFactory), m_roomManager(roomManager), m_room(room), m_user(user)
{}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo information)
{
    return information.codeID == PLAYERSINROOM_CODE || information.codeID == LEAVEROOM_CODE || information.codeID == GETROOMSTATE_CODE || information.codeID == READYNOTREADY_CODE || information.codeID == REFRESH_CODE || information.codeID == USERPOINTS_CODE;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo information)
{
    RequestResult requestResult;
    Room* room;
    requestResult.newHandler = this;
    // Check if room is active.
    try
    {
        //room = m_roomManager.getRoom(m_room->getMetaData().id);
        int code = information.codeID;
        switch (code)
        {
        case ERROR_CODE:
            handleProgramClosed();
            requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
            break;

        case READYNOTREADY_CODE:
            requestResult = readyUnready();
            break;

        case GETROOMSTATE_CODE:
            requestResult = getRoomState();
            break;

        case LEAVEROOM_CODE:
            requestResult = leaveRoom();
            break;

        case REFRESH_CODE:
            requestResult = refresh();
            break;

        case USERPOINTS_CODE:
            requestResult = getUserPoints();
            break;

        case PLAYERSINROOM_CODE:
            requestResult = GetPlayers();
            break;
        }

    }
    catch (std::exception e)
    {
        // If not active then send result of not existing room
        ErrorResponse error;
        error.message = "Room has been closed by the owner";
        requestResult.buffer = JsonResponsePacketSerializer::serializeErrorResponse(error);
        requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
        if (m_user.getIsReady())
            m_user.readyUnready();
    }

    return requestResult;
}

void RoomMemberRequestHandler::handleProgramClosed()
{
    leaveRoom();
}

RequestResult RoomMemberRequestHandler::leaveRoom()
{
    RequestResult requestResult;
    LeaveRoomResponse leave;

    m_room->removeUser(&m_user);
    leave.status = success;
    requestResult.buffer = JsonResponsePacketSerializer::serializeLeaveRoomResponse(leave);

    //Menu Handler
    requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);

    if (m_user.getIsReady())
        m_user.readyUnready();

    return requestResult;
}

RequestResult RoomMemberRequestHandler::getRoomState()
{
    RequestResult requestResult;
    GetRoomStateResponse roomState;

    roomState.answerTimeout = m_room->getMetaData().timePerQuestion;
    roomState.hasGameBegun = m_room->getMetaData().isActive;
    roomState.players = m_room->getAllUsers();
    roomState.questionCount = m_room->getMetaData().numOfQuestionsInGame;

    roomState.status = success;
    requestResult.buffer = JsonResponsePacketSerializer::serializeGetRoomStateRespone(roomState);

    requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);

    return requestResult;
}

RequestResult RoomMemberRequestHandler::readyUnready()
{
    RequestResult requestResult;
    m_user.readyUnready();
    m_room->readyUnreadyUser(&m_user);
    ReadyUnreadyResponse response;
    response.status = success;
    requestResult.newHandler = this;
    requestResult.buffer = JsonResponsePacketSerializer::serializeReadyUnreadyResponse(response);
    return requestResult;
}

RequestResult RoomMemberRequestHandler::refresh()
{
    RequestResult requestResult;

    try 
    {
        if (m_roomManager.getRoom(m_room->getMetaData().id) == nullptr)
        {
            ErrorResponse error;
            error.message = "Room has been closed by the owner";
            requestResult.buffer = JsonResponsePacketSerializer::serializeErrorResponse(error);
            requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
        }
        else if (m_roomManager.getRoom(m_room->getMetaData().id)->getMetaData().isActive == success) {
            m_user.readyUnready();
            RefreshResponse response;
            response.status = 2;
            requestResult.buffer = JsonResponsePacketSerializer::serializeRefreshResponse(response);
            //New handler game start
            requestResult.newHandler = new GameRequestHandler(m_handlerFactory, &m_user, m_room);
        }
        else
        {
            // Check if the user is kicked out by the room admin
            if (!m_roomManager.getRoom(m_room).hasUserBeenApproved(&m_user)) 
            {
                leaveRoom();
                RefreshResponse response;
                response.status = 9; // kicked out status
                requestResult.buffer = JsonResponsePacketSerializer::serializeRefreshResponse(response);
                requestResult.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
            }
            else
            {
                // No update 
                RefreshResponse response;
                response.status = success;
                requestResult.buffer = JsonResponsePacketSerializer::serializeRefreshResponse(response);
                requestResult.newHandler = this;
            }
        }   
        return requestResult;
    }
    catch (...)
    {
        return requestResult;
    }
}

RequestResult RoomMemberRequestHandler::getUserPoints()
{
    RequestResult requestResult;

    UserPointsResponse response;
    response.points = m_handlerFactory.getStatisticsManager().gerUserPoints(m_user.getUserName());
    requestResult.buffer = JsonResponsePacketSerializer::serializeUserPointsResponse(response);

    requestResult.newHandler = this;

    return requestResult;
}


RequestResult RoomMemberRequestHandler::GetPlayers()
{
    RequestResult result;
    try {
        GetPlayersInRoomResponse response;      
        response.users = m_roomManager.getRoom(m_room->getMetaData().id)->getAllUsers();
        result.buffer = JsonResponsePacketSerializer::serializeGetPlayersInRoomResponse(response);
        result.newHandler = this;
    }
    catch (...)
    {
        ErrorResponse error;
        error.message = "Room has been closed by the owner";
        result.buffer = JsonResponsePacketSerializer::serializeErrorResponse(error);
        result.newHandler = new MenuRequestHandler(m_handlerFactory, &m_user);
    }
    return result;
}