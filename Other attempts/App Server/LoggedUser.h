#pragma once

#include <string>
#include <vector>

class Room; // Forward declaration
class RoomAdminRequestHandler;

class LoggedUser
{
public:
    LoggedUser(const std::string& userName, int points);
    std::string getUserName() const;
    void readyUnready();
    bool getIsReady() const;
    void addRoom(Room* room);
    void deleteOwnedRoom();
    void clearRoomList();
    void addRoom(RoomAdminRequestHandler* room);
    int getTotalPoints() const;
    bool isApproved();
    Room* getWaitingRoom();
    bool operator==(const LoggedUser* other) const;

private:
    std::string m_username;
    int m_totalPoints;
    bool m_isReady;
    Room* m_ownedRoom;
    RoomAdminRequestHandler* m_roomWaitingToApprove; // Declare the member variable


};

bool operator==(const LoggedUser& lhs, const LoggedUser& rhs);
bool operator<(const LoggedUser& lhs, const LoggedUser& rhs);
