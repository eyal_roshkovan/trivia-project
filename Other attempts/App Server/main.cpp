#include <iostream>
#include "WSAInitializer.h"
#include "Server.h"
#include "JsonRequestPacketDeserializer.h"
#include "SqliteDatabase.h"
#include "JsonResponsePacketSerializer.h"

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server server;
		server.run();

	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	

	return 0;
}