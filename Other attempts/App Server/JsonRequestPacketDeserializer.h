#pragma once
#include "StructsHeader.h"
#include "Helper.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequest(std::vector<unsigned char> buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);
	static ReadyUnreadyRequest deserializeReadyUnreadyRequest(std::vector<unsigned char> buffer);
	static ApprovedUserRequest deserializeApprovedUserRequest(std::vector<unsigned char> buffer);
	static KickPlayerRequest deserializeKickPlayerRequest(std::vector<unsigned char> buffer);
	static SubmitAnswerRequest deserializerSubmitAnswerRequest(std::vector<unsigned char> buffer);
private:
	static string get_string(std::vector<unsigned char> buffer);
};

