#pragma once
#include "IRequestHandler.h"
#include "StructsHeader.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"

#define LISTEN_PORT 8826


class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory& factory);
    virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;
	virtual void handleProgramClosed() override;
private:
	RequestHandlerFactory& m_handlerFactory;
};

