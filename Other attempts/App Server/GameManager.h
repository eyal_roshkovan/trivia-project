#pragma once
#include "Game.h"

class GameManager
{
public:
	GameManager(IDatabase* db);
	Game* createGame(Room* room);
	void deleteGame(int gameId);
	Question* getQuestionForUserAndGame(LoggedUser* user, Game* game);
	std::vector<PlayerResults> getGameResultsOfGame(Game* game, int& status);
	bool removePlayer(LoggedUser* usertoRemove, Game* game);
	int submitAnswer(LoggedUser* usertoRemove, Game* game, int answerId);
	void incGamesInTable(LoggedUser* user);
	bool hasEveryoneFinished(Game* game);
private:
	IDatabase* m_database;
	std::vector<Game*> m_games;

};

