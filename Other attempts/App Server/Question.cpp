#include "Question.h"

Question::Question(string question, std::vector<string> possibleAns, int correctId) :
    m_question(question),
    m_possibleAnswers(possibleAns),
    m_correctAnswerId(correctId)
{
}

string Question::getQuestion() const
{
    return m_question;
}

std::vector<string> Question::getPossibleAnswers()
{
    return m_possibleAnswers;
}

int Question::getCorrectAnswerId()
{
    return m_correctAnswerId;
}

bool Question::operator==(const Question& other)
{
    return m_question == other.m_question;
}
