﻿using GUI.Views;
using MY_WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GUI.ProgramManager
{
    internal class LoginOrSignupManager
    {
        public static MainWindow loginWindow;
        public static SignupPage signupWindow;

        public static void ShowLogin()
        {
            if (loginWindow == null)
            {
                loginWindow = new MainWindow();
            }
            loginWindow.Show();
            signupWindow.Visibility = Visibility.Collapsed;
        }
        public static void ShowSignup(Service1Client client)
        {
            if (signupWindow == null)
            {
                signupWindow = new SignupPage(client);
            }
            signupWindow.Show();
            loginWindow.Visibility = Visibility.Collapsed;
        }
        public static void HideAll()
        {
            if(loginWindow != null) loginWindow.Visibility = Visibility.Collapsed;
            if(signupWindow != null) signupWindow.Visibility = Visibility.Collapsed;
        }
    }
}
