﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Runtime.CompilerServices;
using classes;

namespace GUI.ProgramManager
{

    public class MenuManager
    {
        public static bool toAskApprove = true;
        public static bool menuActive = true;
        private static Menu menuWindow;
        //private static JoinRoom joinRoomWindow;
        //private static CreateRoom createRoomWindow;
        //private static Statistics statisticsWindow;
        //private static PersonalStatistics personalStatisticsWindow;
        //private static GeneralStatistics generalStatisticsWindow;
        private static LoggedUser user;
        private static Service1Client client;

        public static void SetMenu(LoggedUser user, Service1Client client)
        {
            MenuManager.user = user;
            MenuManager.client = client;
            DefineAllWindows();
        }
        public static void CloseAllWindows()
        {
            try
            {
                menuWindow.Visibility = System.Windows.Visibility.Collapsed;
                //createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                //joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                //personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                //generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                //statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch { }
        }
        public static void DefineAllWindows()
        {
            menuWindow = new Menu(user, client);
            //joinRoomWindow = new JoinRoom(user, client);
            //createRoomWindow = new CreateRoom(user, client);
            //statisticsWindow = new Statistics(user, client);
            //personalStatisticsWindow = new PersonalStatistics();
            //generalStatisticsWindow = new GeneralStatistics();
        }
        //public static void ShowStatistics()
        //{
        //    statisticsWindow.Show();
        //    try
        //    {
        //        menuWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //    }
        //    catch
        //    {

        //    }
        //}
        //public static void ShowPersonalStatistics()
        //{
        //    personalStatisticsWindow.Show();
        //    personalStatisticsWindow.Update();
        //    try
        //    {
        //        createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        menuWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //    }
        //    catch
        //    {

        //    }
        //}
        //public static void ShowGeneralStatistics()
        //{
        //    generalStatisticsWindow.Show();
        //    generalStatisticsWindow.Update();
        //    try
        //    {
        //        createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        menuWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //    }
        //    catch
        //    {

        //    }
        //}
        public static void ShowMenu()
        {
            LoginOrSignupManager.HideAll();
            menuWindow.Show();
            try
            {
                //createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                //joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
                //statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                //personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
                //generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
            }
            catch 
            {

            }
        }
        //public static void ShowJoinRoom()
        //{
        //    joinRoomWindow.Show();
        //    try
        //    {
        //        createRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        menuWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //    }
        //    catch {

        //    }
        //}
        //public static void ShowCreateRoom()
        //{
        //    createRoomWindow.Show();
        //    try
        //    {
        //        joinRoomWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        menuWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        statisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        personalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //        generalStatisticsWindow.Visibility = System.Windows.Visibility.Collapsed;
        //    }
        //    catch   
        //    {

        //    }
        //}
    }
}
