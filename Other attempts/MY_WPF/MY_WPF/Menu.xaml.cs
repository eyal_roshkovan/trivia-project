﻿using classes;
using GUI.ProgramManager;
using MY_WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    [DataContract]
    public partial class Menu : Window
    {
        private LoggedUser user;
        private Service1Client client;
        public Menu(LoggedUser user, Service1Client client)
        {
            InitializeComponent();
            //Uri logoUri = new Uri(Manager.path);
            //BitmapImage logoBitmap = new BitmapImage(logoUri);
            //Icon = logoBitmap;
            //Closing += Manager.HandleClose;
            //this.service = currentCLient;
            this.client = client;
            this.user = user;
            WelcomeLabel.Content = "Welcome, " + user.username + "!!!";
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            LogoutRequest request = new LogoutRequest
            {
                UserWhoRequests = user
            };
            GetRoomsRequest request1 = new GetRoomsRequest{ UserWhoRequests = user };
            client.GetRoomsResponse(request1);
            Close();
        }

        private void Statistics_Click(object sender, RoutedEventArgs e)
        {
            // MenuManager.ShowStatistics();
        }
        private void CreatingRoom_Click(object sender, RoutedEventArgs e)
        {
            // MenuManager.ShowCreateRoom();
        }
        private void JoiningRoom_Click(Object sender, RoutedEventArgs e)
        {
            // MenuManager.ShowJoinRoom();
        }
    }
}
