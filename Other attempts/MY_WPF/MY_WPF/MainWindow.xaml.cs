﻿using classes;
using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MY_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Service1Client client;
        public MainWindow()
        {
            InitializeComponent();
            client = new Service1Client();
            LoginOrSignupManager.loginWindow = this;
        }
        private void Login_Click(object sender, RoutedEventArgs e)
        {
            LoginRequest request = new LoginRequest();
            request.Username = txtUsername.Text;
            request.Password = txtPassword.Password;
            LoggedUser user = client.GetLoginResponse(request).User;
            if (user != null)
            {
                MessageBox.Show("Login successful. Welcome!", "Login Success", MessageBoxButton.OK, MessageBoxImage.Information);
                MenuManager.SetMenu(user, client);
                MenuManager.ShowMenu();
                return;
            }
            MessageBox.Show("Login failed. Please check your username and password and try again.", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            LoginOrSignupManager.ShowSignup(client);
        }
    }
}
