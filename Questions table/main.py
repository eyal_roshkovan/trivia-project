import requests
from bs4 import BeautifulSoup
import sqlite3
import json

# Function to scrape trivia questions from a website


def insert_question_answers(incorrect_answers, question, correct_answer, category, difficulty):
    # Connect to the SQLite database
    conn = sqlite3.connect('C:\\Users\\User\\Desktop\\projects\\trivia-ran-eyal\\Trivia\\Trivia.sqlite')
    cursor = conn.cursor()

    # Prepare the SQL query to insert the data into the table
    sql = f"INSERT INTO QuestionsAnswers (question, firstWrongAnswer, secondWrongAnswer, thirdWrongAnswer, rightAnswer," \
          f" difficulty, category) VALUES ('{question}', '{incorrect_answers[0]}', '{incorrect_answers[1]}'," \
          f" '{incorrect_answers[2]}', '{correct_answer}', '{difficulty}', '{category}');"

    # Execute the query with the provided values
    cursor.execute(sql)

    # Commit the transaction and close the connection
    conn.commit()
    conn.close()


def scrape_trivia_questions(category: str, difficulty: str):
    url = f"https://opentdb.com/api.php?amount=100&category={category}&difficulty={difficulty}&type=multiple"
    response = requests.get(url)
    json_format = json.loads(response.text.replace('&#039;', ''))
    data = json_format['results']

    new_list = []

    for item in data:
        question = item['question']
        wrong_answers = item['incorrect_answers'][:3]  # Extract the first three wrong answers
        right_answer = item['correct_answer']
        if '&' not in question and '&' not in wrong_answers and '&' not in right_answer:
            new_list.append((question, wrong_answers, right_answer))

    return new_list, data[0]["category"]
    # Logic to extract the trivia questions from the website
    # Modify this code according to the structure of the website you are scraping


difficulties = ["easy", "medium", "hard"]

for i in range(0, 50):
    for j in range(3):
        try:
            all_data = (scrape_trivia_questions(str(i), difficulties[j]))
            questions_answers = all_data[0]
            wrong_questions = []
            questions = []
            correct_answers = []
            for set in questions_answers:
                questions.append(set[0])
                wrong_questions.append(set[1])
                correct_answers.append(set[2])

            for k in range(len(correct_answers)):
                insert_question_answers(incorrect_answers=wrong_questions[k], question=questions[k],
                                        correct_answer=correct_answers[k], category=all_data[1], difficulty=difficulties[j])
        except Exception as e:
            print(e)

