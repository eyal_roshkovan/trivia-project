﻿using System;
using System.Collections.Generic;
using System.Linq;

public class LoginRequestHandler : IRequestHandler
{

    public LoginRequestHandler()
    {}

    public RequestResult HandleRequest(RequestInfo requestInfo)
    {
        RequestResult requestResult = new RequestResult();
        bool loginValid;

        try
        {
            if (requestInfo.CodeID == TypesOfRequest.LOGIN_CODE)
            {
                LoginRequest loginRequest = JsonRequestPacketDeserializer.DeserializeLoginRequest(requestInfo.Buffer);
                LoginResponse response = new LoginResponse();

                // Access to DB
                loginValid = LoginManager.GetInstance().Login(loginRequest.Username, loginRequest.Password);

                if (!loginValid)
                {
                    response.Status = states.fail;
                    requestResult.Buffer = JsonResponsePacketSerializer.SerializeLoginResponse(response);
                    requestResult.NewHandler = new LoginRequestHandler();
                }
                else
                {
                    response.Status = states.success;
                    requestResult.Buffer = JsonResponsePacketSerializer.SerializeLoginResponse(response);

                    // Create new handler MenuRequestHandler
                    LoggedUser user = new LoggedUser(loginRequest.Username, Convert.ToInt32(DBConnector.GetNumOfPlayerGames(loginRequest.Username)));
                    requestResult.NewHandler = new MenuRequestHandler(user);
                }
            }
            else if (requestInfo.CodeID == TypesOfRequest.SIGNUP_CODE)
            {
                SignupRequest signupRequest = JsonRequestPacketDeserializer.DeserializeSignupRequest(requestInfo.Buffer);
                SignupResponse response = new SignupResponse();

                // Access to DB
                loginValid = LoginManager.GetInstance().Signup(signupRequest.Username, signupRequest.Password, signupRequest.Email);

                if (!loginValid)
                {
                    response.Status = states.fail;
                    requestResult.Buffer = JsonResponsePacketSerializer.SerializeSignUpResponse(response);
                    requestResult.NewHandler = new LoginRequestHandler();
                }
                else
                {
                    response.Status = states.success;
                    requestResult.Buffer = JsonResponsePacketSerializer.SerializeSignUpResponse(response);

                    // Create new handler MenuRequestHandler
                    LoggedUser user = new LoggedUser(signupRequest.Username, 0);
                    requestResult.NewHandler = new MenuRequestHandler(user);
                }
            }
            else
            {
                ErrorResponse response = new ErrorResponse
                {
                    Message = "Login Error"
                };

                requestResult.Buffer = JsonResponsePacketSerializer.SerializeErrorResponse(response);
                requestResult.NewHandler = null;
            }
        }
        catch (Exception)
        {
            ErrorResponse response = new ErrorResponse
            {
                Message = "Syntax Error"
            };

            requestResult.Buffer = JsonResponsePacketSerializer.SerializeErrorResponse(response);
            requestResult.NewHandler = null;
        }

        return requestResult;
    }

    public void HandleProgramClosed()
    {
        // can be empty
    }
}
