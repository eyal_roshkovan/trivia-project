﻿using System;
using System.Collections.Generic;

public class RoomMemberRequestHandler : IRequestHandler
{
    private RoomManager roomManager;
    private Room room;
    private LoggedUser user;

    public RoomMemberRequestHandler(RoomManager roomManager, LoggedUser user, Room room)
    {
        this.roomManager = roomManager;
        this.room = room;
        this.user = user;
    }

    public RequestResult HandleRequest(RequestInfo information)
    {
        RequestResult requestResult = new RequestResult();
        requestResult.NewHandler = this;

        try
        {
            switch (information.CodeID)
            {
                case TypesOfRequest.ERROR_CODE:
                    HandleProgramClosed();
                    requestResult.NewHandler = new MenuRequestHandler(user);
                    break;

                case TypesOfRequest.READY_NOTREADY_CODE:
                    requestResult = ReadyUnready();
                    break;

                case TypesOfRequest.GET_ROOM_STATE_CODE:
                    requestResult = GetRoomState();
                    break;

                case TypesOfRequest.LEAVE_ROOM_CODE:
                    requestResult = LeaveRoom();
                    break;

                case TypesOfRequest.REFRESH_CODE:
                    requestResult = Refresh();
                    break;

                case TypesOfRequest.USER_POINTS_CODE:
                    requestResult = GetUserPoints();
                    break;

                case TypesOfRequest.PLAYERS_IN_ROOM_CODE:
                    requestResult = GetPlayers();
                    break;

                case TypesOfRequest.REFRESH_WEB_CODE:
                    requestResult = WebRefresh();
                    break;

                case TypesOfRequest.GET_IS_OWNER:
                    requestResult = GetIsOwner();
                    break;
            }
        }
        catch (Exception)
        {
            // Handle exceptions as needed
        }

        return requestResult;
    }

    public void HandleProgramClosed()
    {
        LeaveRoom();
    }

    private RequestResult WebRefresh()
    {
        RequestResult requestResult = new RequestResult();
        WebMemberRefreshResponse response = new WebMemberRefreshResponse();
        RefreshResponse refreshResponse = new RefreshResponse();
        if (roomManager.GetRoom(room.GetMetaData().Id) == null)
        {
            ErrorResponse error = new ErrorResponse();
            error.Message = "Room has been closed by the owner\n";
            requestResult.Buffer = JsonResponsePacketSerializer.SerializeErrorResponse(error);
            requestResult.NewHandler = new MenuRequestHandler(user);
            return requestResult;
        }

        if (roomManager.GetRoom(room.GetMetaData().Id).GetMetaData().IsActive == (int)states.success)
        {
            user.ReadyUnready();
            refreshResponse.Status = "2\n";

            // New handler game start
            requestResult.NewHandler = new GameRequestHandler(user, room);
        }
        else
        {
            // Check if the user is kicked out by the room admin
            if (roomManager.GetRoom(room).HasUserBeenApproved(user) == 0)
            {
                LeaveRoom();
                refreshResponse.Status = "9\n"; // kicked out status
                requestResult.NewHandler = new MenuRequestHandler(user);
            }
            else
            {
                // No update 
                refreshResponse.Status = "1\n";
                requestResult.NewHandler = this;
            }
        }
        response.refreshResponse = refreshResponse;

        response.PlayersInRoom = new GetPlayersInRoomResponse
        {
            Users = room.GetAllUsers()
        };

        requestResult.Buffer = JsonResponsePacketSerializer.SerializeRefreshWebResponse(response);

        return requestResult;
    }
    private RequestResult GetIsOwner()
    {
        GetIsOwnerResponse getIsOwnerResponse = new GetIsOwnerResponse { IsOwner = false };
        RequestResult result = new RequestResult();
        result.NewHandler = this;
        result.Buffer = JsonResponsePacketSerializer.SerializeIsOwnerResponse(getIsOwnerResponse);
        return result;
    }

    private RequestResult LeaveRoom()
    {
        RequestResult requestResult = new RequestResult();
        LeaveRoomResponse leave = new LeaveRoomResponse();

        room.RemoveUser(user);
        leave.Status = (int)states.success;
        requestResult.Buffer = JsonResponsePacketSerializer.SerializeLeaveRoomResponse(leave);

        // Menu Handler
        requestResult.NewHandler = new MenuRequestHandler(user);

        if (user.GetIsReady())
            user.ReadyUnready();

        return requestResult;
    }

    private RequestResult GetRoomState()
    {
        RequestResult requestResult = new RequestResult();
        GetRoomStateResponse roomState = new GetRoomStateResponse();

        roomState.AnswerTimeout = room.GetMetaData().TimePerQuestion;
        roomState.HasGameBegun = room.GetMetaData().IsActive != 0;
        roomState.Players = room.GetAllUsers();
        roomState.QuestionCount = room.GetMetaData().NumOfQuestionsInGame;

        roomState.Status = (int)states.success;
        requestResult.Buffer = JsonResponsePacketSerializer.SerializeGetRoomStateResponse(roomState);

        requestResult.NewHandler = new MenuRequestHandler(user);

        return requestResult;
    }

    private RequestResult ReadyUnready()
    {
        RequestResult requestResult = new RequestResult();
        user.ReadyUnready();
        ReadyUnreadyResponse response = new ReadyUnreadyResponse();
        response.Status = (int)states.success;
        requestResult.NewHandler = this;
        requestResult.Buffer = JsonResponsePacketSerializer.SerializeReadyUnreadyResponse(response);
        return requestResult;
    }

    private RequestResult Refresh()
    {
        RequestResult requestResult = new RequestResult();

        try
        {
            if (roomManager.GetRoom(room.GetMetaData().Id) == null)
            {
                ErrorResponse error = new ErrorResponse();
                error.Message = "Room has been closed by the owner";
                requestResult.Buffer = JsonResponsePacketSerializer.SerializeErrorResponse(error);
                requestResult.NewHandler = new MenuRequestHandler(user);
            }
            else if (roomManager.GetRoom(room.GetMetaData().Id).GetMetaData().IsActive == (int)states.success)
            {
                user.ReadyUnready();
                RefreshResponse response = new RefreshResponse();
                response.Status = "2";
                requestResult.Buffer = JsonResponsePacketSerializer.SerializeRefreshResponse(response);

                // New handler game start
                requestResult.NewHandler = new GameRequestHandler(user, room);
            }
            else
            {
                // Check if the user is kicked out by the room admin
                if (roomManager.GetRoom(room).HasUserBeenApproved(user) == 0)
                {
                    LeaveRoom();
                    RefreshResponse response = new RefreshResponse();
                    response.Status = "9"; // kicked out status
                    requestResult.Buffer = JsonResponsePacketSerializer.SerializeRefreshResponse(response);
                    requestResult.NewHandler = new MenuRequestHandler(user);
                }
                else
                {
                    // No update 
                    RefreshResponse response = new RefreshResponse();
                    response.Status = "1";
                    requestResult.Buffer = JsonResponsePacketSerializer.SerializeRefreshResponse(response);
                    requestResult.NewHandler = this;
                }
            }
            return requestResult;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return requestResult;
        }
    }

    private RequestResult GetUserPoints()
    {
        RequestResult requestResult = new RequestResult();

        UserGamesWonResponse response = new UserGamesWonResponse();
        response.GamesWon = DBConnector.GetAmountOfGamesWon(user.GetUsername());
        requestResult.Buffer = JsonResponsePacketSerializer.SerializeUserPointsResponse(response);

        requestResult.NewHandler = this;

        return requestResult;
    }

    private RequestResult GetPlayers()
    {
        RequestResult result = new RequestResult();
        try
        {
            GetPlayersInRoomResponse response = new GetPlayersInRoomResponse();
            response.Users = roomManager.GetRoom(room.GetMetaData().Id).GetAllUsers();
            result.Buffer = JsonResponsePacketSerializer.SerializeGetPlayersInRoomResponse(response);
            result.NewHandler = this;
        }
        catch (Exception)
        {
            ErrorResponse error = new ErrorResponse();
            error.Message = "Room has been closed by the owner";
            result.Buffer = JsonResponsePacketSerializer.SerializeErrorResponse(error);
            result.NewHandler = new MenuRequestHandler(user);
        }
        return result;
    }
}
