﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Question
{
    public Question(string question, List<string> possibleAns, int correctId)
    {
        this.question = question;
        possibleAnswers = possibleAns;
        correctAnswerId = correctId;
    }

    public string GetQuestion()
    {
        return question;
    }

    public List<string> GetPossibleAnswers()
    {
        return possibleAnswers;
    }

    public int GetCorrectAnswerId()
    {
        return correctAnswerId;
    }

    public bool Equals(Question other)
    {
        // Implement your own logic for equality comparison
        // This is a basic example, adjust as needed
        return question == other.question &&
               possibleAnswers.SequenceEqual(other.possibleAnswers) &&
               correctAnswerId == other.correctAnswerId;
    }

    private string question;
    private List<string> possibleAnswers;
    private int correctAnswerId;
}
