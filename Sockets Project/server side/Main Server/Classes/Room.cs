﻿using System;
using System.Collections.Generic;

public class Room
{
    private RoomData metadata;
    private Dictionary<LoggedUser, string> users;
    private LoggedUser roomOwner;

    public Room(RoomData roomData, LoggedUser roomOwner)
    {
        metadata = roomData;
        users = new Dictionary<LoggedUser, string>();
        users[roomOwner] = "Owner";
        this.roomOwner = roomOwner;
    }

    public void AddUser(LoggedUser newUser)
    {
        users[newUser] = "Member";
        metadata.CurrentAmountOfPlayers++;
    }

    public void RemoveUser(LoggedUser userToRemove)
    {
        foreach (var user in new Dictionary<LoggedUser, string>(users))
        {
            if (user.Key == userToRemove)
            {
                users.Remove(user.Key);
                metadata.CurrentAmountOfPlayers--;
            }
        }
    }

    public Dictionary<LoggedUser, string> GetAllUsers()
    {
        return new Dictionary<LoggedUser, string>(users);
    }
    public LoggedUser GetOwner() 
    { 
        return roomOwner; 
    }
    public RoomData GetMetaData()
    {
        if (this == null)
        {
            throw new InvalidOperationException("Room has been closed");
        }

        return metadata;
    }

    public void SetActive(int isActive)
    {
        metadata.IsActive = isActive;
    }

    public bool Equals(Room other)
    {
        return GetMetaData().Id == other.GetMetaData().Id;
    }

    public void ReadyUnreadyUser(LoggedUser user)
    {
        foreach (var entry in users)
        {
            if (entry.Key.Equals(user))
            {
                entry.Key.ReadyUnready();
            }
        }
    }

    public bool IsFull()
    {
        return metadata.MaxPlayers == metadata.CurrentAmountOfPlayers;
    }
}

public static class RoomExtensions
{
    public static bool Equals(this Room lhs, Room rhs)
    {
        return lhs.GetMetaData().Name == rhs.GetMetaData().Name;
    }
}
