﻿using System;
using System.Collections.Generic;

public enum states
{
    success = 1,
    fail = 0
};


public enum TypesOfRequest
{
    ERROR_CODE = 0,
    LOGIN_CODE = 1,
    SIGNUP_CODE = 2,
    LOGOUT_CODE = 3,
    JOIN_ROOM_CODE = 4,
    CREATE_ROOM_CODE = 5,
    GET_ROOMS_CODE = 6,
    PLAYERS_IN_ROOM_CODE = 7,
    GET_TOP_WINNERS_CODE = 80,
    GET_TOP_CATEGORIES_CODE = 81,
    PERSONAL_STATISTICS_CODE = 9,
    CLOSE_ROOM_CODE = 10,
    READY_NOTREADY_CODE = 11,
    GET_ROOM_STATE_CODE = 12,
    LEAVE_ROOM_CODE = 13,
    START_GAME_CODE = 14,
    REFRESH_CODE = 15,
    REFRESH_WEB_CODE = 151,
    USER_POINTS_CODE = 16,
    GET_OWNER_ROOM_CODE = 17,
    HAS_APPROVED_CODE = 18,
    STOP_WAITING_CODE = 19,
    GET_WAITING_TO_APPROVE_LIST_CODE = 20,
    APPROVE_USER_CODE = 21,
    GET_IS_OWNER = 40,
    LEAVE_GAME_CODE = 22,
    GET_QUESTION_CODE = 23,
    SUBMIT_ANSWER_CODE = 24,
    GET_GAME_RESULTS_CODE = 25,
    KICK_PLAYER_CODE = 26,
    HAS_EVERYONE_FINISHED_CODE = 27
}

public class RequestInfo
{
    public TypesOfRequest CodeID;
    public List<byte> Buffer;
}

public class RequestResult
{
    public List<byte> Buffer;
    public IRequestHandler NewHandler;
}


public class ScoreData
{
    public string userName;
    public int score;
}

public class RoomData
{
    public int Id;
    public string Name;
    public int MaxPlayers;
    public int NumOfQuestionsInGame;
    public int CurrentAmountOfPlayers;
    public int TimePerQuestion;
    public int IsActive;
    public bool IsPublic;
    public string RoomOwner;
    public string Category;
}

public class PlayerResults
{
    public string Username;
    public int CorrectAnswerCount;
    public int WrongAnswerCount;
    public int AverageAnswerTime;
}

public class GameData
{
    public Question CurrentQuestion;
    public int CorrectAnswerCount;
    public int WrongAnswerCount;
    public float AverageAnswerTime;
    public int QuestionIndex;
    public bool HasAnsweredLastQuestion;
}

