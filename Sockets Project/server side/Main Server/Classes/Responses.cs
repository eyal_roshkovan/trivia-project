﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class Response
{ }

public class UserGamesWonResponse : Response
{
    public int GamesWon;
}

public class WebAdminRefreshResponse : Response
{
    public GetPlayersInRoomResponse PlayersInRoom;
    public GetWaitingToApproveListResponse usersWaiting;
}

public class WebMemberRefreshResponse : Response
{
    public GetPlayersInRoomResponse PlayersInRoom;
    public RefreshResponse refreshResponse;
}

public class LoginResponse : Response
{
    public states Status;
}

public class SignupResponse : Response
{
    public states Status;
}

public class ErrorResponse : Response
{
    public string Message;
}

public class GetTopWinnersResponse : Response
{
    public int Status;
    public List<Tuple<string, int, int>> Winners;
};

public class GetTopCategoriesResponse : Response
{
    public List<Tuple<string, int>> topCategories;
}

public class CloseRoomResponse : Response
{
    public int Status;
}

public class StartGameResponse : Response
{
    public int Status;
}

public class LeaveRoomResponse : Response
{
    public int Status;
}

public class ReadyUnreadyResponse : Response
{
    public int Status;
}

public class ReadyUnreadyRequest : Request
{
    public bool ReadyOrNot;
}

public class GetRoomStateResponse : Response
{
    public int Status;
    public bool HasGameBegun;
    public Dictionary<LoggedUser, string> Players;
    public int QuestionCount;
    public int AnswerTimeout;
}

public class GetIsOwnerResponse : Response
{
    public bool IsOwner;
}

public class GetOwnerRoomResponse : Response
{
    public Room Room;
}

public class RefreshResponse : Response
{
    public string Status;
}
public class GetPlayersInRoomResponse : Response
{
    public Dictionary<LoggedUser, string> Users;
}

public class HasApprovedResponse : Response
{
    public int ApprovedCode;
}

public class StopWaitingResponse : Response
{
    public bool Succeded;
}

public class GetWaitingToApproveListResponse : Response
{
    public List<LoggedUser> Users;
}

public class ApprovedUserResponse : Response
{
    public int Status;
}

public class LogoutResponse : Response
{
    public int Status;
}

public class GetRoomsResponse : Response
{
    public int Status;
    public List<RoomData> Rooms;
}

public class GetPersonalStatsResponse : Response
{
    public List<string> Statistics;
}

public class JoinRoomResponse : Response
{
    public int Status;
}

public class CreateRoomResponse : Response
{
    public int Status;
}

public class KickPlayerResponse : Response
{
    public bool Success;
}

public class GetQuestionResponse : Response
{
    public int Status;
    public string Question;
    public Dictionary<int, string> Answers;
}

public class SubmitAnswerResponse : Response
{
    public int Status;
    public int CorrectAnswerId;
}

public class GetGameResultsResponse : Response
{
    public int Status;
    public List<PlayerResults> Results;
}

public class GetTimePerQuestionResponse : Response
{
    public int TimePerQuestion;
}

public class LeaveGameResponse : Response
{
    public int Status;
}

public class HasEveryoneFinishedResponse : Response
{
    public bool HasEveryoneFinished;
}
