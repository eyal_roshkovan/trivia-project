﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Request
{ }

public class SubmitAnswerRequest : Request
{
    public int AnswerId;
}

public class LoginRequest : Request
{
    public string Username;
    public string Password;
}

public class SignupRequest : Request
{
    public string Username;
    public string Password;
    public string Email;
}

public class CreateRoomRequest : Request
{
    public string Category;
    public string RoomName;
    public int MaxUsers;
    public int QuestionCount;
    public int AnswerTimeout;
    public string Privacy;
}

public class JoinRoomRequest : Request
{
    public int RoomID;
}

public class GetPlayersInRoomRequest : Request
{
    public int RoomID;
}


public class ApprovedUserRequest : Request
{
    public string Username;
    public bool Approved;
}

public class KickPlayerRequest : Request
{
    public string Username;
}