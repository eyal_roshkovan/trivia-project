﻿using System;
using System.Collections.Generic;
using System.Linq;

public class GameManager
{
    private readonly List<Game> games;
    private static GameManager instance;
    private GameManager()
    {
        games = new List<Game>();
    }

    public static GameManager GetInstance()
    {
        if(instance == null)
            instance = new GameManager();
        return instance;
    }

    public Game CreateGame(Room room)
    {
        // If the game already exists, return the game; if not, create a new game
        try
        {
            Game existingGame = games.FirstOrDefault(game => game.GetId() == room.GetMetaData().Id);
            if (existingGame != null)
            {
                return existingGame;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        Game newGame;
        try
        {
            newGame = new Game(room);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        try
        {
            games.Add(newGame);
            return newGame;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void DeleteGame(int gameId)
    {
        games.RemoveAll(game => game.GetId() == gameId);
    }

    public Question GetQuestionForUserAndGame(LoggedUser user, Game game)
    {
        Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
        return gameToSearch?.GetQuestionForUser(user);
    }

    public List<PlayerResults> GetGameResultsOfGame(Game game, int status)
    {
        Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
        if (gameToSearch != null)
        {
            return gameToSearch.GetGameResults(status);
        }

        status = 1;
        return new List<PlayerResults>();
    }

    public bool RemovePlayer(LoggedUser userToRemove, Game game)
    {
        try
        {
            Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
            gameToSearch?.RemovePlayer(userToRemove);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public int SubmitAnswer(LoggedUser user, Game game, int answerId)
    {
        Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
        return gameToSearch?.SubmitAnswer(user, answerId) ?? 0;
    }

    public void IncGamesInTable(LoggedUser user)
    {
        DBConnector.IncrementCountOfGames(user.GetUsername());
    }

    public bool HasEveryoneFinished(Game game)
    {
        Game gameToSearch = games.FirstOrDefault(g => g.GetId() == game.GetId());
        return gameToSearch?.HasEveryoneFinished() ?? false;
    }
}
