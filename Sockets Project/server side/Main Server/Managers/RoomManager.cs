﻿using System;
using System.Collections.Generic;
using System.Linq;

public class RoomManager
{
    private Dictionary<int, Room> rooms;
    private List<RoomAdminRequestHandler> roomAdmins;
    private static RoomManager instance;
    private RoomManager()
    {
        rooms = new Dictionary<int, Room>();
        roomAdmins = new List<RoomAdminRequestHandler>();
    }
    public static RoomManager GetInstance()
    {
        if(instance == null)
            instance = new RoomManager();
        return instance;
    }

    public void CreateRoom(LoggedUser user, RoomData data)
    {
        Room newRoom = new Room(data, user);
        rooms[data.Id] = newRoom;
    }

    public void DeleteRoom(int id)
    {
        if (rooms.ContainsKey((int)id))
        {
            rooms.Remove((int)id);
        }
    }

    public int GetRoomState(int id)
    {
        if (rooms.ContainsKey((int)id))
        {
            return rooms[(int)id].GetMetaData().IsActive;
        }

        return 0;
    }

    public List<RoomData> GetRooms()
    {
        return rooms.Values.Where(room => room.GetMetaData().IsActive == 0).Select(room => room.GetMetaData()).ToList();
    }

    public Room GetRoom(int id)
    {
        return rooms.ContainsKey((int)id) ? rooms[(int)id] : null;
    }

    public int GetFreeId()
    {
        int closestFreeId = 1;

        foreach (var pair in rooms)
        {
            if (pair.Key > closestFreeId)
            {
                break;
            }
            closestFreeId++;
        }

        return closestFreeId;
    }

    public void AddRoomAdmin(RoomAdminRequestHandler admin)
    {
        roomAdmins.Add(admin);
    }

    public RoomAdminRequestHandler GetRoom(Room room)
    {
        foreach (var admin in roomAdmins)
        {
            if (admin.GetRoom() == room)
            {
                return admin;
            }
        }

        throw new Exception("couldn't find such a room");
    }

    public void DeleteRoom(RoomAdminRequestHandler admin)
    {
        roomAdmins.Remove(admin);

        if (admin.GetRoom() != null)
        {
            rooms.Remove(admin.GetRoom().GetMetaData().Id);
        }
    }
}
