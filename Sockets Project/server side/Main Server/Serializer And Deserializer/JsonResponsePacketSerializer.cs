﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Runtime.InteropServices.ComTypes;

public static class JsonResponsePacketSerializer
{
    public static List<byte> ConvertIntoShape(Dictionary<string, object> data, int code)
    {
        List<byte> buffer = new List<byte>();
        string strBuffer;
        string strJson;

        strJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);

        // Insert code 
        strBuffer = Convert.ToString(code, 2).PadLeft(Helper.CODE_SIZE, '0');

        // Insert len of data
        int jLen = strJson.Length;
        strBuffer += Convert.ToString(jLen, 2).PadLeft(Helper.DATA_LEN_SIZE, '0');

        // Insert data
        foreach (char ch in strJson)
            strBuffer += Convert.ToString(ch, 2).PadLeft(8, '0');

        foreach (char ch in strBuffer)
        {
            if (ch == '0')
                buffer.Add(0);
            else
                buffer.Add(1);
        }

        return buffer;
    }

    public static List<byte> SerializeLoginResponse(LoginResponse loginResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", loginResponse.Status }
        };

        return ConvertIntoShape(data, (int)TypesOfRequest.LOGIN_CODE);
    }

    public static List<byte> SerializeRefreshWebResponse(WebMemberRefreshResponse refreshWebResponse)
    {
        string strData = refreshWebResponse.refreshResponse.Status;
        foreach (var playerEntry in refreshWebResponse.PlayersInRoom.Users)
        {
            var user = playerEntry.Key;
            var additionalInfo = playerEntry.Value;
            strData += user.GetUsername() + ", " + additionalInfo + ", " + user.GetAmountOfGamesPlayed() + ", " + user.GetIsReady() + "\n";
        }
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "data", strData},
        };
        return ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_WEB_CODE);
    }

    public static List<byte> SerializeIsOwnerResponse(GetIsOwnerResponse isOwnerResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "isOwner", isOwnerResponse.IsOwner},
        };
        return ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_WEB_CODE);
    }

    public static List<byte> SerializeRefreshWebResponse(WebAdminRefreshResponse refreshWebResponse)
    {
        string strData = "users waiting:\n";
        foreach (var userEntry in refreshWebResponse.usersWaiting.Users)
        {
            strData += userEntry.GetUsername() + ", Member, " + userEntry.GetAmountOfGamesPlayed() + "\n";
        }
        strData += "users in room:\n";
        foreach (var playerEntry in refreshWebResponse.PlayersInRoom.Users)
        {
            var user = playerEntry.Key;
            var additionalInfo = playerEntry.Value;
            strData += user.GetUsername() + ", " + additionalInfo + ", " + user.GetAmountOfGamesPlayed() + ", " + user.GetIsReady() + ", ";
        }
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "data", strData},
        };
        return ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_WEB_CODE);
    }

    public static List<byte> SerializeSignUpResponse(SignupResponse signUpResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", signUpResponse.Status }
        };

        return ConvertIntoShape(data, (int)TypesOfRequest.SIGNUP_CODE);
    }

    public static List<byte> SerializeErrorResponse(ErrorResponse errorResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "message", errorResponse.Message }
        };

        return ConvertIntoShape(data, (int)TypesOfRequest.ERROR_CODE);
    }

    public static List<byte> SerializeLogoutResponse(LogoutResponse logoutResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", logoutResponse.Status }
        };

        return ConvertIntoShape(data, (int)TypesOfRequest.LOGOUT_CODE);
    }

    public static List<byte> SerializeJoinRoomResponse(JoinRoomResponse joinRoomResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", joinRoomResponse.Status }
        };

        return ConvertIntoShape(data, (int)TypesOfRequest.JOIN_ROOM_CODE);
    }

    public static List<byte> SerializeCreateRoomResponse(CreateRoomResponse createRoomResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>
        {
            { "status", createRoomResponse.Status }
        };

        return ConvertIntoShape(data, (int)TypesOfRequest.CREATE_ROOM_CODE);
    }

    public static List<byte> SerializeGetRoomResponse(GetRoomsResponse getRoomsResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        string roomsList = "";

        foreach (RoomData roomData in getRoomsResponse.Rooms)
        {

            roomsList += roomData.Name;
            roomsList += ", ";
            roomsList += roomData.RoomOwner;
            roomsList += ", ";
            roomsList += roomData.Category;
            roomsList += ", ";
            roomsList += roomData.IsPublic;
            roomsList += ", ";
            roomsList += roomData.MaxPlayers;
            roomsList += ", ";
            roomsList += roomData.CurrentAmountOfPlayers;
            roomsList += ", ";
            roomsList += roomData.Id;
            roomsList += ", ";
            roomsList += roomData.NumOfQuestionsInGame;
            roomsList += ", ";
            roomsList += roomData.TimePerQuestion;
            roomsList += "\n";
        }

        data["status"] = getRoomsResponse.Status;
        data["Rooms"] = roomsList;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_ROOMS_CODE);
    }

    public static List<byte> SerializeGetPlayersInRoomResponse(GetPlayersInRoomResponse playerInRoomResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        string playersList = "";

        foreach (var playerEntry in playerInRoomResponse.Users)
        {
            var user = playerEntry.Key;
            var additionalInfo = playerEntry.Value;
            playersList += user.GetUsername() + ", " + additionalInfo + ", " + user.GetAmountOfGamesPlayed() + ", " + user.GetIsReady() + "\n";
        }

        data["PlayersInRoom"] = playersList;

        return ConvertIntoShape(data, (int)TypesOfRequest.PLAYERS_IN_ROOM_CODE);
    }

    public static List<byte> SerializeTopWinnersResponse(GetTopWinnersResponse topWinnersResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = topWinnersResponse.Status;
        data["Winners"] = topWinnersResponse.Winners;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_TOP_WINNERS_CODE);
    }

    public static List<Byte> SerializeTopCategoriesResponse(GetTopCategoriesResponse response)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["Categories"] = response.topCategories;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_TOP_WINNERS_CODE);
    }

    public static List<byte> SerializePersonalStatsResponse(GetPersonalStatsResponse personalStatsResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["Stats"] = personalStatsResponse.Statistics;

        return ConvertIntoShape(data, (int)TypesOfRequest.PERSONAL_STATISTICS_CODE);
    }

    public static List<byte> SerializeGetRoomStateResponse(GetRoomStateResponse getRoomStateResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        List<string> playersDataList = new List<string>();

        foreach (var kvp in getRoomStateResponse.Players)
        {
            playersDataList.Add($"{kvp.Key.GetUsername()}, {kvp.Value}, {kvp.Key.GetAmountOfGamesPlayed()}");
        }

        data["status"] = getRoomStateResponse.Status;
        data["hasGameBegun"] = getRoomStateResponse.HasGameBegun;
        data["players"] = playersDataList;
        data["answerCount"] = getRoomStateResponse.QuestionCount;
        data["answerTimeOut"] = getRoomStateResponse.AnswerTimeout;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_ROOM_STATE_CODE);
    }

    public static List<byte> SerializeReadyUnreadyResponse(ReadyUnreadyResponse readyUnreadyResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = readyUnreadyResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.READY_NOTREADY_CODE);
    }

    public static List<byte> SerializeRefreshResponse(RefreshResponse refreshResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = refreshResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.REFRESH_CODE);
    }

    public static List<byte> SerializeUserPointsResponse(UserGamesWonResponse userPointsResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["gamesWon"] = userPointsResponse.GamesWon;

        return ConvertIntoShape(data, (int)TypesOfRequest.USER_POINTS_CODE);
    }


    public static List<byte> SerializeCloseRoomResponse(CloseRoomResponse closeRoomResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = closeRoomResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.CLOSE_ROOM_CODE);
    }

    public static List<byte> SerializeStartGameResponse(StartGameResponse startGameResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = startGameResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.START_GAME_CODE);
    }

    public static List<byte> SerializeLeaveRoomResponse(LeaveRoomResponse leaveRoomResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = leaveRoomResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.LEAVE_ROOM_CODE);
    }

    public static List<byte> SerializeGetOwnerRoomResponse(GetOwnerRoomResponse getOwnerRoomResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        RoomData roomData = getOwnerRoomResponse.Room.GetMetaData();
        string roomInfo = $"{roomData.Name}, {roomData.RoomOwner}, {roomData.Category}, {roomData.IsPublic}, {roomData.MaxPlayers}, {roomData.CurrentAmountOfPlayers}, {roomData.Id}, {roomData.NumOfQuestionsInGame}, {roomData.TimePerQuestion}";

        data["Room"] = roomInfo;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_OWNER_ROOM_CODE);
    }

    public static List<byte> SerializeHasApprovedResponse(HasApprovedResponse hasApprovedResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["approved"] = hasApprovedResponse.ApprovedCode;

        return ConvertIntoShape(data, (int)TypesOfRequest.HAS_APPROVED_CODE);
    }

    public static List<byte> SerializeStopWaitingResponse(StopWaitingResponse stopWaitingResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["succeeded"] = stopWaitingResponse.Succeded;

        return ConvertIntoShape(data, (int)TypesOfRequest.STOP_WAITING_CODE);
    }

    public static List<byte> SerializeGetWaitingUsersResponse(GetWaitingToApproveListResponse getWaitingToApproveListResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        string players = "";
        foreach (var userEntry in getWaitingToApproveListResponse.Users)
        {
            players += userEntry.GetUsername() + ", Member, " + userEntry.GetAmountOfGamesPlayed() + "\n";
        }

        if (!string.IsNullOrEmpty(players))
        {
            players = players.TrimEnd('\n');
        }

        data["WaitingPlayers"] = players;

        return ConvertIntoShape(data, (int)TypesOfRequest.PLAYERS_IN_ROOM_CODE);
    }

    public static List<byte> SerializeApproveUserResponse(ApprovedUserResponse approvedUserResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = approvedUserResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.APPROVE_USER_CODE);
    }

    public static List<byte> SerializeKickPlayerResponse(KickPlayerResponse kickPlayerResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["succeeded"] = kickPlayerResponse.Success;

        return ConvertIntoShape(data, (int)TypesOfRequest.KICK_PLAYER_CODE);
    }

    public static List<byte> SerializeGetGameResultsResponse(GetGameResultsResponse getGameResultsResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = getGameResultsResponse.Status;

        // Convert results vector to JSON list
        string playersResults = "";
        foreach (PlayerResults playerResults in getGameResultsResponse.Results)
        {
            playersResults += playerResults.Username + ", " +
                playerResults.CorrectAnswerCount + ", " +
                playerResults.WrongAnswerCount + ", " +
                playerResults.AverageAnswerTime + "  ";
        }

        if (!string.IsNullOrEmpty(playersResults))
        {
            playersResults = playersResults.TrimEnd('\n');
        }

        data["results"] = playersResults;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_GAME_RESULTS_CODE);
    }

    public static List<byte> SerializeSubmitAnswerResponse(SubmitAnswerResponse submitAnswerResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = submitAnswerResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.SUBMIT_ANSWER_CODE);
    }

    public static List<byte> SerializeGetQuestionResponse(GetQuestionResponse getQuestionResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = getQuestionResponse.Status;
        data["question"] = getQuestionResponse.Question;
        data["answers"] = getQuestionResponse.Answers;

        return ConvertIntoShape(data, (int)TypesOfRequest.GET_QUESTION_CODE);
    }

    public static List<byte> SerializeLeaveGameResponse(LeaveGameResponse leaveGameResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["status"] = leaveGameResponse.Status;

        return ConvertIntoShape(data, (int)TypesOfRequest.LEAVE_GAME_CODE);
    }

    public static List<byte> SerializeHasEveryoneFinishedResponse(HasEveryoneFinishedResponse hasEveryoneFinishedResponse)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        data["hasEveryoneFinished"] = hasEveryoneFinishedResponse.HasEveryoneFinished;

        return ConvertIntoShape(data, (int)TypesOfRequest.HAS_EVERYONE_FINISHED_CODE);
    }
}
