﻿using System;

class Server
{
    private readonly Communicator communicator;

    public Server()
    {
        communicator = new Communicator();

        DBConnector.Open();
    }

    public void Run()
    {
        communicator.BindAndListen();
    }
}
