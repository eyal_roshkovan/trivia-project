﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

class Communicator
{
    private const int LISTEN_PORT = 8826; 

    private readonly Dictionary<Socket, IRequestHandler> _mapSockets = new Dictionary<Socket, IRequestHandler>();
    private readonly Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    private readonly object _mapSocketLock = new object();

    public Communicator()
    {
        if (_serverSocket == null)
        {
            throw new Exception($"{nameof(Communicator)} - socket");
        }
    }

    public void BindAndListen()
    {
        var sa = new IPEndPoint(IPAddress.Any, LISTEN_PORT);

        try
        {
            _serverSocket.Bind(sa);
            _serverSocket.Listen(int.MaxValue);
            Console.WriteLine($"Listening on port {LISTEN_PORT}");

            while (true)
            {
                Console.WriteLine("Waiting for client connection request");
                try
                {
                    ClientAccept();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error accepting client: {e.Message}");
                    // Log or handle the exception appropriately
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine($"Error binding socket: {e.Message}");
            // Log or handle the exception appropriately
        }
    }

    private void HandleNewClient(Socket clientSocket)
    {
        IRequestHandler handler = new LoginRequestHandler();

        try
        {
            while (handler != null)
            {
                RequestInfo requestInfo = HandleNewMessage(clientSocket);
                RequestResult result = null;
                try
                {
                    result = handler.HandleRequest(requestInfo);
                    if(result.Buffer != null)
                        Helper.SendData(clientSocket, result.Buffer.ToArray());
                    handler = result.NewHandler;
                }
                catch (Exception e)
                {
                    handler?.HandleProgramClosed();
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine($"Error handling client message: {e.Message}");
            handler?.HandleProgramClosed();
            return;
        }
    }

    private RequestInfo HandleNewMessage(Socket clientSocket)
    {
        List<byte> buffer;
        int requestId = 0;
        string message;
        RequestInfo requestInfo = new RequestInfo();

        // Using Task.Run to handle the receiving with a timeout
        var result = Task.Run(() =>
        {
            try
            {
                return Helper.GetDataFromSocket(clientSocket, 2048);
            }
            catch(Exception e)
            {
                return "";
            }
        });

        // Wait for the result with a timeout of 120 seconds
        if (!result.Wait(TimeSpan.FromSeconds(120)))
        {
            // Throw an exception if no data received within 2 minutes
            throw new TimeoutException("No data received within 2 minutes.");
        }

        // Retrieve the message from the result
        message = result.Result;

        buffer = new List<byte>(message.Length);

        foreach (char bitChar in message)
        {
            if (bitChar == '0')
            {
                buffer.Add(0);
            }
            else if (bitChar == '1')
            {
                buffer.Add(1);
            }
            else
            {
                Console.WriteLine("sss");
            }
            // Ignore other characters (if any)
        }
        try
        {
            requestId = Helper.GetMessageCode(buffer);
        }
        catch (Exception)
        {
            // Handle exception if any
        }

        requestInfo.CodeID = (TypesOfRequest)requestId;
        requestInfo.Buffer = buffer;

        return requestInfo;
    }

    private void ClientAccept()
    {
        // this accepts the client and creates a specific socket from the server to this client
        // the process will not continue until a client connects to the server
        Socket clientSocket = _serverSocket.Accept();

        if (clientSocket == null)
        {
            throw new Exception($"{nameof(Communicator)} - accept");
        }

        Console.WriteLine("Client accepted. Server and client can speak");

        lock (_mapSocketLock)
        {
            LoginRequestHandler loginHandler = new LoginRequestHandler();
            _mapSockets.Add(clientSocket, loginHandler);

            Thread handlerThread = new Thread(() => HandleNewClient(clientSocket));
            handlerThread.Start();
        }
    }
}
