﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Main_Server
{
    internal class Program
    {
        public static bool ExecuteNonQuery(string sqlStatement, OleDbConnection db)
        {
            try
            {
                using (OleDbCommand command = new OleDbCommand(sqlStatement, db))
                {
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        static void Main()
        {
            string dbFileName = "C:\\Users\\User\\Desktop\\projects\\trivia-project\\Sockets Project\\server side\\Main Server\\Database.mdb";
            OleDbConnection db = new OleDbConnection($"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={dbFileName};"); ;

            db.Open();

            if (db.State != ConnectionState.Open)
            {
                Console.WriteLine("Error with opening the database");
            }
            string query = "CREATE TABLE USERS (\r\n    UserName TEXT PRIMARY KEY,\r\n    Pass TEXT,\r\n    Email TEXT,\r\n    GamesPlayed INTEGER,\r\n    QuestionsAnswered INTEGER\r\n);\r\n";
            ExecuteNonQuery(query, db);
            query = "CREATE TABLE GameStatistics (\r\n    GameID AUTOINCREMENT PRIMARY KEY,\r\n    AdminUserName TEXT,\r\n    GameCategory TEXT,\r\n    QuestionsAnswered INTEGER,\r\n    WinnerUserName TEXT,\r\n    FOREIGN KEY (AdminUserName) REFERENCES USERS(UserName),\r\n    FOREIGN KEY (WinnerUserName) REFERENCES USERS(UserName)\r\n);\r\n";
            ExecuteNonQuery(query, db);

            Server server = new Server();
            server.Run();
        }
    }
}
