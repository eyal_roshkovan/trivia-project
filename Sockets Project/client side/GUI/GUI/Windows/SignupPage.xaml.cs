﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media.Imaging;
using GUI.ProgramManager;

namespace GUI.Views
{
    public partial class SignupPage : Window
    {
        public SignupPage()
        {
            InitializeComponent();
            Uri logoUri = new Uri(Manager.path);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
        }
        
        public static bool IsEmailValid(string email)
        {
            // Check for a valid email pattern
            string pattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
            Regex regex = new Regex(pattern);

            return regex.IsMatch(email);
        }

        public static bool IsPasswordValid(string password)
        {
            // Check length
            if (password.Length < 8)
            {
                return false;
            }

            // Check for at least one capital letter
            if (!Regex.IsMatch(password, "[A-Z]"))
            {
                return false;
            }

            // Check for at least one digit
            if (!Regex.IsMatch(password, "[0-9]"))
            {
                return false;
            }

            return true;
        }

        private void Signup_Click(object sender, RoutedEventArgs e)
        {
            if(Password.Password != ConfirmPasswordBox.Password) 
            {
                MessageBox.Show("Your Passwords don't match", "Passwords not matching error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(TermsCheckBox.IsChecked == false)
            {
                MessageBox.Show("You must accept the terms in order to sign up", "Accepting the terms error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(!IsPasswordValid(Password.Password))
            {    
                MessageBox.Show("The password should contain at least 8 characters, with at least 1 capital letter and at least 1 digit", "Invalid Password", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(!IsEmailValid(EmailTextBox.Text)) 
            {
                MessageBox.Show("Invalid email address.", "Email error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!LoginOrSignupManager.TrySignUp(UsernameTextBox.Text, Password.Password, EmailTextBox.Text))
            {
                MessageBox.Show("username is already taken", "Error while signing up", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MessageBox.Show("Successfully signed up", "Sign up successfully", MessageBoxButton.OK, MessageBoxImage.Information);
            MenuManager.DefineAllWindows(UsernameTextBox.Text);
            MenuManager.ShowMenu();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginOrSignupManager.ShowLogin();
        }
    }
}
