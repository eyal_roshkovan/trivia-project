﻿using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI.Windows
{
    /// <summary>
    /// Interaction logic for EndGame.xaml
    /// </summary>
    public partial class EndGame : Window
    {
        public ObservableCollection<Result> Results { get; set; }

        public EndGame()
        {
            InitializeComponent();
            Results = new ObservableCollection<Result>();
            lvResults.ItemsSource = Results;
            Closing += Manager.HandleClose;
            Thread thread = new Thread(WaitTillEveryoneFinishes);
            thread.Start();
        }

        private void WaitTillEveryoneFinishes()
        {
            while (!GameManager.HasEveryoneFinished())
            {
                Thread.Sleep(1000);
            }

            Dispatcher.Invoke(() =>
            {
                lblPleaseWait.Visibility = Visibility.Collapsed;
                lvResults.Visibility = Visibility.Visible;
                BackButton.Visibility = Visibility.Visible;
            });

            // Show results
            Result[] results = GameManager.GetResults();

            foreach (Result result in results)
            {
                Dispatcher.Invoke(() =>
                {
                    Results.Add(result);
                });
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Closing -= Manager.HandleClose;
            GameManager.ShowMenu();
        }
    }
}
