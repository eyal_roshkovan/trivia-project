﻿using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI.Windows
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        private string username, category, isPublic;
        public CreateRoom(string username)
        {
            InitializeComponent();
            Uri logoUri = new Uri(Manager.path);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
            this.username = username;
            Closing += Manager.HandleClose;
        }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            int timePerQuestion;
            int maxUsers;
            int questionCount;
            if(txtRoomName.Text == "")
            {
                MessageBox.Show("Fill the room name field.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }  
            if (txtTimePerQuestion.Text == "")
            {
                MessageBox.Show("Fill the Time Per Question field.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(!int.TryParse(txtTimePerQuestion.Text, out timePerQuestion))
            {
                MessageBox.Show("Fill the Time Per Question field with an integer.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(int.Parse(txtTimePerQuestion.Text) > 60 || int.Parse(txtTimePerQuestion.Text) < 1)
            {
                MessageBox.Show("The maximum time per question is 60 secand the minimum is 1 sec.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtQuestionsCount.Text == "")
            {
                MessageBox.Show("Fill the Question Count field.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txtQuestionsCount.Text, out questionCount))
            {
                MessageBox.Show("Fill the Question Count field with an integer.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (int.Parse(txtQuestionsCount.Text) < 1)
            {
                MessageBox.Show("There should be at least 1 question in a game.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtParticipantsLimit.Text == "")
            {
                MessageBox.Show("Fill the Participants Limit field.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txtParticipantsLimit.Text, out maxUsers))
            {
                MessageBox.Show("Fill the Participants Limit field with an integer.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(int.Parse(txtParticipantsLimit.Text) <= 1)
            {
                MessageBox.Show("There should be at least 2 players in a single room.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(category == null)
            {
                MessageBox.Show("Please select your category.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (isPublic == null)
            {
                MessageBox.Show("Please select your privacy.", "Create Room Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(MenuManager.TryCreateRoom(txtRoomName.Text, category, maxUsers, questionCount, timePerQuestion, isPublic))
            {
                MenuManager.menuActive = false;
                Room room = WaitingRoomManager.GetRoom();
                // Room Window must be shown now
                if(room == null)
                {
                    MessageBox.Show("Room Couldn't get Created successfully, you will return to the menu", "Error Creating Room", MessageBoxButton.OK, MessageBoxImage.Error);
                    MenuManager.ShowMenu();
                    return;
                }
                MessageBox.Show("Room Created successfully, you will move there right now", "Success Creating Room", MessageBoxButton.OK, MessageBoxImage.Information);
                WaitingRoomManager.ShowWaitingRoom(room, username);
            }
            else
            {
                MessageBox.Show("Unfortunately, the room could not be created, please try again or try joining a room instead.", "Error while Creating Room", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowMenu();   
        }

        private void cmbPrivatePublic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

            if (selectedItem != null)
            {
                isPublic = selectedItem.Content.ToString();
            }
        }

        private void cmbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

            if (selectedItem != null)
            {
                category = selectedItem.Content.ToString();
            }
        }
    }
}
