﻿using GUI.ProgramManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GUI.Windows
{
    /// <summary>
    /// Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Window
    {
        private Room UserRoom;
        private int timePerQuestion;
        private int timerCount;
        private int maxTime;
        private int questionCount;
        public GamePage(Room room, string username)
        {
            InitializeComponent();
            questionCount = 1;
            Uri logoUri = new Uri(Manager.path);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
            UserRoom = room;
            maxTime = room.TimePerQuestion;
            timerCount = maxTime;
            Closing += Manager.HandleClose;
            getQuestion();
            Name.Content = username;
            QuestionCountLabel.Content = "question 1 out of " + room.AmountOfQuestions;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timerTick;
            timer.Start();
        }

        private void timerTick(object? sender, EventArgs e)
        {
            if (timerCount == 0) 
            { 
                //Submit answer
                getQuestion();
                questionCount++;
                QuestionCountLabel.Content = "question " + questionCount + " out of " + UserRoom.AmountOfQuestions;
            }
            else
            {
                timerCount--;
                TimeLeftLabel.Content = "Time Left:" + timerCount.ToString() + " seconds";
            }
        }

        public void getQuestion()
        {
            if (!GameManager.gameIsOn)
                return;

            JObject allQuestionData = GameManager.sendAndRecive_GetQuestion();
            if (allQuestionData["status"].ToString() == "0")
            {
                //No more question left, move to game results page
                MessageBox.Show("Game is Done !! you will see the results right now");
                Closing -= Manager.HandleClose;
                GameManager.FinishGame();
            }

            QuestionLabel.Text = allQuestionData["question"].ToString();
            //Get answers
            string answers = allQuestionData["answers"].ToString();
            answers = answers.Replace("\r\n", "").Replace("  ", "");
            Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(answers);

            // Convert Dictionary values to a List
            List<string> valuesList = new List<string>(data.Values);

            // Convert the list of strings into a dictionary with integer keys
            Dictionary<int, string> map = new Dictionary<int, string>();
            foreach (string entry in valuesList)
            {
                // Assuming each entry is in the format "key: value"
                string[] parts = entry.Split(':');
                if (parts.Length == 2)
                {
                    int key = Convert.ToInt32(parts[0].Trim());
                    string value = parts[1].Trim();
                    map[key] = value;
                }
            }

            // Now you can use the map to set content for different controls
            try
            {
                PossibleAnswer1.Content = valuesList[0];
                PossibleAnswer2.Content = valuesList[1];
                PossibleAnswer3.Content = valuesList[2];
                PossibleAnswer4.Content = valuesList[3];
            }
            catch (Exception ex)
            {

            }

            timerCount = maxTime;
        }

        private void PossibleAnswer1_Click(object sender, RoutedEventArgs e)
        {
            questionCount++;
            GameManager.sendAndRecive_SubmitAnswer(1);
            getQuestion();
            QuestionCountLabel.Content = "question " + questionCount + " out of " + UserRoom.AmountOfQuestions;
        }

        private void PossibleAnswer2_Click(object sender, RoutedEventArgs e)
        {
            questionCount++;
            GameManager.sendAndRecive_SubmitAnswer(2);
            getQuestion();
            QuestionCountLabel.Content = "question " + questionCount + " out of " + UserRoom.AmountOfQuestions;
        }

        private void PossibleAnswer3_Click(object sender, RoutedEventArgs e)
        {
            questionCount++;
            GameManager.sendAndRecive_SubmitAnswer(3);
            getQuestion();
            QuestionCountLabel.Content = "question " + questionCount + " out of " + UserRoom.AmountOfQuestions;
        }

        private void PossibleAnswer4_Click(object sender, RoutedEventArgs e)
        {
            questionCount++;
            GameManager.sendAndRecive_SubmitAnswer(4);
            getQuestion();
            QuestionCountLabel.Content = "question " + questionCount + " out of " + UserRoom.AmountOfQuestions;
        }
    }
}
