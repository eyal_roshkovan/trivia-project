﻿using System.Windows;
using GUI.Views;
using System;
using GUI.ProgramManager;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.IO;


namespace GUI.Views
{
    public partial class LoginPage : Window
    {
        public static string RemoveBetween(string path)
        {
            string startPattern = "GUI\\GUI";
            string endPattern = "\\logo.png";

            int startIndex = path.IndexOf(startPattern);
            int endIndex = path.IndexOf(endPattern, startIndex + startPattern.Length);

            if (startIndex != -1 && endIndex != -1)
            {
                return path.Remove(startIndex + startPattern.Length, endIndex - (startIndex + startPattern.Length));
            }

            return path;
        }

        public LoginPage()
        {
            InitializeComponent();
            string constantEnding = "GUI\\GUI\\logo.png";
            string baseDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string fullPath = Path.Combine(baseDirectory, constantEnding);
            fullPath = RemoveBetween(fullPath);
            Manager.path = fullPath;
            Uri logoUri = new Uri(fullPath);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if(LoginOrSignupManager.TryLogin(txtUsername.Text, txtPassword.Password))
            {
                MessageBox.Show("Login successful. Welcome!", "Login Success", MessageBoxButton.OK, MessageBoxImage.Information);
                MenuManager.DefineAllWindows(txtUsername.Text);
                MenuManager.ShowMenu();
                return;
            }
            MessageBox.Show("Login failed. Please check your username and password and try again.", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            LoginOrSignupManager.ShowSignup();
        }
    }
}