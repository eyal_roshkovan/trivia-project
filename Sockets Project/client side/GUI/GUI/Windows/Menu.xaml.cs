﻿using GUI.ProgramManager;
using GUI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        private string username;
        private const int logOutCode = 3;
        public Menu(string username)
        {
            InitializeComponent();
            Uri logoUri = new Uri(Manager.path);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
            Closing += Manager.HandleClose;
            this.username = username;
            WelcomeLabel.Content = "Welcome, " + username + "!!!";
        }
        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.Logout();
        }
        private void Statistics_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowStatistics();
        }
        private void CreatingRoom_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowCreateRoom();
        }
        private void JoiningRoom_Click(Object sender, RoutedEventArgs e)
        {
            MenuManager.ShowJoinRoom();
        }
    }
}
