﻿using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI.Windows
{
    public class Room
    {
        public string RoomName { get; set; }
        public string RoomOwner { get; set; }
        public string Category { get; set; }
        public bool IsPublic { get; set; }
        public int MaxPlayers { get; set; }
        public int CurrentPlayers { get; set; }
        public int id { get; set; }
        public int AmountOfQuestions { get; set; }
        public int TimePerQuestion { get; set; }
        public Room(string roomName, string roomOwner, bool isPublic, string category, int currentPlayers, int maxPlayers, int id, int amountOfQuestions, int timePerQuestion)
        {
            RoomName = roomName;
            RoomOwner = roomOwner;
            IsPublic = isPublic;
            Category = category;
            CurrentPlayers = currentPlayers;
            MaxPlayers = maxPlayers;
            this.id = id;
            AmountOfQuestions = amountOfQuestions;
            TimePerQuestion = timePerQuestion;
        }
    }

    public partial class JoinRoom : Window
    {
        private string username;
        private bool isSearchOn = false;
        private string roomToSearch = "";

        public JoinRoom(string username)
        {
            InitializeComponent();
            Uri logoUri = new Uri(Manager.path);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
            Closing += Manager.HandleClose;
            this.username = username;
            Thread thread = new Thread(new ThreadStart(UpdateTable));
            thread.Start();
        }

        public void UpdateTable()
        {
            while (MenuManager.menuActive)
            {
                Room[] rooms = MenuManager.GetRooms().ToArray();
                GridView gridView = null;
                if (isSearchOn)
                {
                    Room[] filteredRooms = rooms.Where(room => room.RoomName.ToLower().StartsWith(roomToSearch) || room.RoomName.ToLower().Contains(roomToSearch)).ToArray();
                    rooms = filteredRooms;
                }

                // Execute the update operation on the UI thread
                Dispatcher.Invoke(() =>
                {
                    gridView = (GridView)listView.View;
                    gridView.Columns.Clear();

                    // Add columns to the GridView
                    gridView.Columns.Add(new GridViewColumn
                    {
                        Header = "Room Name",
                        DisplayMemberBinding = new Binding("RoomName"),
                        Width = 150
                    });

                    gridView.Columns.Add(new GridViewColumn
                    {
                        Header = "Room Owner",
                        DisplayMemberBinding = new Binding("RoomOwner"),
                        Width = 150
                    });

                    gridView.Columns.Add(new GridViewColumn
                    {
                        Header = "Public/Private",
                        DisplayMemberBinding = new Binding("IsPublic"),
                        Width = 150
                    });

                    gridView.Columns.Add(new GridViewColumn
                    {
                        Header = "Category",
                        DisplayMemberBinding = new Binding("Category"),
                        Width = 150
                    });

                    gridView.Columns.Add(new GridViewColumn
                    {
                        Header = "Capacity",
                        DisplayMemberBinding = new Binding("Capacity"),
                        Width = 150
                    });

                    // Additional column with the "Join" button
                    DataTemplate joinButtonTemplate = new DataTemplate();
                    FrameworkElementFactory joinButtonFactory = new FrameworkElementFactory(typeof(Button));
                    joinButtonFactory.SetValue(ContentProperty, "Join");
                    joinButtonFactory.SetValue(WidthProperty, 80.0);
                    joinButtonFactory.SetValue(HeightProperty, 30.0);
                    joinButtonFactory.AddHandler(Button.ClickEvent, new RoutedEventHandler(JoinButton_Click));
                    joinButtonFactory.SetBinding(Button.TagProperty, new Binding("RoomObject"));
                    joinButtonFactory.SetBinding(Button.IsEnabledProperty, new Binding("IsJoinEnabled"));
                    joinButtonFactory.SetValue(Button.BackgroundProperty, new Binding("JoinButtonBackground"));
                    joinButtonFactory.SetValue(Button.ForegroundProperty, new Binding("JoinButtonForeground"));
                    joinButtonTemplate.VisualTree = joinButtonFactory;

                    listView.ItemsSource = rooms.Select(room => new
                    {
                        RoomName = room.RoomName,
                        RoomOwner = room.RoomOwner,
                        IsPublic = room.IsPublic ? "Public" : "Private",
                        Category = room.Category,
                        RoomObject = room,
                        Capacity = room.CurrentPlayers < room.MaxPlayers ? "Available" : "Full",
                        IsJoinEnabled = room.CurrentPlayers < room.MaxPlayers,
                        JoinButtonBackground = room.CurrentPlayers < room.MaxPlayers ? Brushes.Green : Brushes.Gray,
                        JoinButtonForeground = Brushes.White
                    });

                    gridView.Columns.Add(new GridViewColumn
                    {
                        Header = "Actions",
                        CellTemplate = joinButtonTemplate,
                        Width = 150
                    });
                });

                Thread.Sleep(3000); // Sleep for 3 seconds
            }
        }

        private void JoinButton_Click(object sender, RoutedEventArgs e)
        {

            Button joinButton = (Button)sender;
            Room room = (Room)joinButton.Tag;

            // Access the properties of the room object


            if (!room.IsPublic)
            {
                MenuManager.toAskApprove = true;
                isSearchOn = false;
                MessageBoxResult result = MessageBox.Show("Please wait till the owner approves you. Press No to cancel", "Message Box", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    MenuManager.TryJoinRoom(room);
                    Thread thread = new Thread(MenuManager.GetRoomResponseForJoining); // Pass room and username as parameters
                    thread.Start();
                    thread.Join();
                    if (MenuManager.approved == 1)
                    {
                        MenuManager.menuActive = false;
                        WaitingRoomManager.ShowWaitingRoom(room, username);
                    }
                    else if (MenuManager.approved == 0)
                    {
                        MessageBox.Show("You are not allowed to get into this room !", "NOT ALLOWED !!!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else if(MenuManager.approved == 9)
                    {
                        MessageBox.Show("The room has been closed", "NOT ALLOWED !!!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else if (result == MessageBoxResult.No)
                {
                    isSearchOn = true;
                    // MenuManager.StopAskForJoiningRoom(); // maybe I could add an option for stop asking to join a room at line 175-176 (probably)
                }
            }
            else
            {
                MenuManager.menuActive = false;
                if(MenuManager.TryJoinRoom(room))
                {
                    WaitingRoomManager.ShowWaitingRoom(room, username);
                }
                else
                {
                    MessageBox.Show("Your are not allowed to get into this room !", "NOT ALLOWED !!!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            string search = txtRoomName.Text;
            if (search == "")
            {
                isSearchOn = false;
                roomToSearch = "";
                return; 
            }
            isSearchOn = true;
            roomToSearch = search;
            txtRoomName.Text = "";
        }

        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowMenu();
        }
    }
}
