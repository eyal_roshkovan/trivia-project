﻿using GUI.ProgramManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI.Windows
{
    
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Window
    {
        string username;
        public Statistics(string userName)
        {
            InitializeComponent();
            username = userName;
            Uri logoUri = new Uri(Manager.path);
            BitmapImage logoBitmap = new BitmapImage(logoUri);
            Icon = logoBitmap;
            Closing += Manager.HandleClose;
        }

        private void PersonalStatistics_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowPersonalStatistics();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MenuManager.ShowGeneralStatistics();

        }
    }
}
