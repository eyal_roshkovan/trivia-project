﻿using GUI.Windows;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;
using System.Printing;

namespace GUI.ProgramManager
{
    public class Result
    {
        public string Username { get ; set; }
        public int RightAnswers { get; set; }
        public int WrongAnswers { get; set; }
        public int AverageTimeToAnswer { get; set; }
        public Result(string username, int rightAnswers, int wrongAnswers, int averageTimeToAnswer)
        {
            Username = username;
            RightAnswers = rightAnswers;
            WrongAnswers = wrongAnswers;
            AverageTimeToAnswer = averageTimeToAnswer;
        }
    }

    public class RootObject
    {
        public List<Result> Results { get; set; }
        public int Status { get; set; }
    }

    internal class GameManager : Manager
    {
        public static bool gameIsOn = true;
        private const int leaveGameCode = 22;
        private const int getQuestionCode = 23;
        private const int submitAnswerCode = 24;
        private const int getResultsCode = 25;
        private const int hasEveryoneFinishedCode = 27;
        private static GamePage gameWindow;
        private static EndGame endGameWindow;
        public static void FinishGame()
        {
            gameIsOn = false;
            gameWindow.Close();
            endGameWindow = new EndGame();
            endGameWindow.Show();
        }
        public static void ShowMenu()
        {
            SendMessage(CodeToBinary(leaveGameCode));
            gameWindow.Close();
            endGameWindow.Close();
            MenuManager.DefineAllWindows(MenuManager.username);
            MenuManager.ShowMenu();
        }
        public static Result[] GetResults()
        {
            List<Result> results = new List<Result>();
            string response = SendMessage(CodeToBinary(getResultsCode));
            // dumb algorithm because I couldn't find a way
            string json = GetJsonFromBinary(response);

            var jsonDocument = JsonDocument.Parse(json);
            var root = jsonDocument.RootElement;

            // Access the "results" field
            string resultsJson = root.GetProperty("results").GetString();

            string[] lines = resultsJson.Split("  ");
            foreach (string line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue; // Skip empty lines
                }

                string[] elements = line.Split(", ");

                if (elements.Length == 4)
                {
                    results.Add(new Result(elements[0], int.Parse(elements[1]), int.Parse(elements[2]), int.Parse(elements[3])));
                }
            }

            return results.ToArray();
        }
        public static void Leave()
        {
            SendMessage(CodeToBinary(leaveGameCode));
            MenuManager.ShowJoinRoom();
            endGameWindow.Close();
        }
        public static bool HasEveryoneFinished()
        {
            string response = SendMessage(CodeToBinary(hasEveryoneFinishedCode));
            return OneBoolResponseDeserializer(response);
        }
        public static void ShowGameWindow(Room room, string username)
        {
            gameIsOn = true;
            gameWindow = new GamePage(room, username);
            gameWindow.Show();
            WaitingRoomManager.Close();
        }
        public static JObject sendAndRecive_GetQuestion()
        {
            string response = SendMessage(CodeToBinary(getQuestionCode));
            string deCodedString = GetJsonFromBinary(response);
           
            JObject j = JObject.Parse(deCodedString);

            return j;
        }
        public static void sendAndRecive_SubmitAnswer(int answerId)
        {
            var jsonFormat = new { answerId = answerId};
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(submitAnswerCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            SendMessage(message);
        }
    }
}