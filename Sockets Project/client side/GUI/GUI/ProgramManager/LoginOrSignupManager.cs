﻿using GUI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;

namespace GUI.ProgramManager
{
    internal class LoginOrSignupManager : Manager
    {
        private const int loginCode = 1;
        private const int signupCode = 2;
        private static LoginPage loginWindow;
        private static SignupPage signupWindow;

        public static void Run(int port)
        {
            Manager.Run(port);
            loginWindow = new LoginPage();
            signupWindow = new SignupPage();
            loginWindow.Show();
        }

        public static void ShowLogin()
        {
            loginWindow.Show();
            signupWindow.Visibility = Visibility.Collapsed;
        }
        public static void ShowSignup()
        {
            signupWindow.Show();
            loginWindow.Visibility = Visibility.Collapsed;
        }
        public static void HideAll()
        {
            loginWindow.Visibility = Visibility.Collapsed;
            signupWindow.Visibility = Visibility.Collapsed;
        }
        
        public static bool TryLogin(string username, string password)
        {
            var jsonFormat = new
            {
                username,
                password
            };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(loginCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            string response = SendMessage(message);
            bool isOK = OneIntResponseDeserializer(response) == 1;

            return isOK;
        }
        public static bool TrySignUp(string username, string password, string email)
        {
            var jsonFormat = new
            {
                username,
                password,
                email
            };
            string jsonString = JsonSerializer.Serialize(jsonFormat);
            string newJsonString = "";
            foreach (char c in jsonString)
            {
                newJsonString += StringToBinary(c.ToString());
            }
            int len = byteSize * jsonString.Length;
            string Code = CodeToBinary(signupCode);
            string lengthMessage = LengthToBinary(len);

            string message = Code + lengthMessage + newJsonString;
            string response = SendMessage(message);

            bool isOK = OneIntResponseDeserializer(response) == 1;

            return isOK;
        }
    }
}
