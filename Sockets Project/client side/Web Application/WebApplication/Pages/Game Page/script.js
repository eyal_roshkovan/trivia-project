﻿import { byteSize, CloseSocket, GetJsonFromBinary, SendMessage, CodeToBinary, StringToBinary, LengthToBinary } from '../../Helper.js'
const leaveGameCode = 22;
const getQuestionCode = 23;
const submitAnswerCode = 24;
let question_counter = 0;
let firstAnswerButton = document.getElementById('PossibleAnswer1');
let secondAnswerButton = document.getElementById('PossibleAnswer2');
let thirdAnswerButton = document.getElementById('PossibleAnswer3');
let fourthAnswerButton = document.getElementById('PossibleAnswer4');
let questionCount = document.getElementById('QuestionCountLabel');
const timePerQuestion = Number(localStorage.getItem('timePerQuestion'));
const amountOfQuestions = Number(localStorage.getItem('timePerQuestion'));
let timerInterval = undefined;
async function initialize() {
    if (question_counter == 0) {
        await getQuestion();
    }
}

async function submitAnswer(answerId) {

    const jsonFormat = {
        answerId
    };

    const jsonString = JSON.stringify(jsonFormat);

    let newJsonString = "";
    for (let i = 0; i < jsonString.length; i++) {
        newJsonString += StringToBinary(jsonString[i]);
    }

    const len = byteSize * jsonString.length;
    const Code = CodeToBinary(submitAnswerCode);
    const lengthMessage = LengthToBinary(len);

    const message = Code + lengthMessage + newJsonString;

    try {
        await SendMessage(message);
        clearInterval(timerInterval);
        getQuestion();
    }
    catch (error) {
        console.error('Error:', error);
    }
}

setInterval(() => {
    initialize();
}, 1000);

async function getQuestion() {
    question_counter += 1;
    let response = await SendMessage(CodeToBinary(getQuestionCode));
    let decoded = GetJsonFromBinary(response);
    let jsonObject = JSON.parse(decoded);
    let status = jsonObject["status"];
    if (status == "0") {
        alert("Game is Done !! you will see the results right now");
        CloseSocket();
        window.location.href = '../End Game/Endgame.html';
    }
    let questionLabel = document.getElementById('questionLabel');
    
    questionLabel.textContent = jsonObject["question"];
    let answers = jsonObject["answers"];
    firstAnswerButton.textContent = answers[1];
    secondAnswerButton.textContent = answers[2];
    thirdAnswerButton.textContent = answers[3];
    fourthAnswerButton.textContent = answers[4];
    questionCount.textContent = "Question " + question_counter + " out of " + amountOfQuestions;
    startTimer(timePerQuestion);
    // TODO Start timer
}

function startTimer(duration) {
    const timeLeftLabel = document.getElementById('TimeLeftLabel');
    let timer = duration;

    function updateTimer() {
        const minutes = Math.floor(timer / 60);
        const seconds = timer % 60;

        timeLeftLabel.textContent = `Time Left: ${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;

        if (timer <= 0) {
            clearInterval(timerInterval);
            getQuestion();
        }

        timer--;
    }

    updateTimer(); // Initial update before setInterval

    // Remove the 'let' keyword here to use the global timerInterval
    timerInterval = setInterval(updateTimer, 1000);
}


document.addEventListener("DOMContentLoaded", async function () {
    firstAnswerButton.addEventListener("click", async function () {
        await submitAnswer(1);
    });
    
    secondAnswerButton.addEventListener("click", async function () {
        await submitAnswer(2);
    });

    thirdAnswerButton.addEventListener("click", async function () {
        await submitAnswer(3);
    });

    fourthAnswerButton.addEventListener("click", async function () {
        await submitAnswer(4);
    });
});
