﻿document.addEventListener("DOMContentLoaded", function () {
    const goBackButton = document.getElementById("goBackButton");
    goBackButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Menu/Menu.html";
    });

    const highScoresButton = document.getElementById("highScoresButton");
    highScoresButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../High Scores/HighScores.html";
    });

    const personalStatisticsButton = document.getElementById("personalStatisticsButton");
    personalStatisticsButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Personal Statistics/PersonalStatistics.html";
    });
})