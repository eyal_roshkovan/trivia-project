﻿import { CodeToBinary, SendMessage, GetJsonFromBinary, CloseSocket } from '../../Helper.js';
const getHighScoresCode = 80;
const getHighStatisticsCode = 81;

// Function to add a row to the high scores table
function addTableRow(points, playerName) {
    // Get a reference to the table
    let table = document.getElementById("highScoresTable");

    // Create a new row element
    let row = table.insertRow(-1); // -1 adds the row to the end of the table

    // Create cells for rank and player name
    let rankCell = row.insertCell(0);
    let playerNameCell = row.insertCell(1);

    // Set the content of the cells
    rankCell.innerHTML = points;
    playerNameCell.innerHTML = playerName;
}

function ExtractData(data) {
    // Parse the data as a JSON object
    try {
        var parsedData = JSON.parse(data);

        if (parsedData.HighScores) {
            // Split the HighScores string by comma
            var userPoints = parsedData.HighScores.split(',');

            for (var i = 0; i < userPoints.length; i++) {
                var userPointPair = userPoints[i].split('-');

                if (userPointPair.length == 2) {
                    var user = userPointPair[0];
                    var point = userPointPair[1];
                    addTableRow(point, user);
                }
            }
        }
    } catch (e) {
        // Handle any parsing errors here
        console.error("Error parsing the data: " + e.message);
    }
}


async function getHighScores() {
    let response = await SendMessage(CodeToBinary(getHighScoresCode));
    ExtractData(GetJsonFromBinary(response));
}

document.addEventListener("DOMContentLoaded", function () {
    const goBackButton = document.getElementById("goBackButton");
    goBackButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Statistics/Statistics.html";
    });

    // Set a timer for, let's say, 0.1 seconds (you can adjust the time in milliseconds)
    const timerDelay = 100; // 0.1 seconds
    setTimeout(async function () {
        // Call the getHighScores function after the timer is done
       await getHighScores();
    }, timerDelay);

})