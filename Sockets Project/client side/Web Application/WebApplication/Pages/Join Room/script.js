﻿// Import statements
import { Room } from '../../classes.js';
import { CloseSocket, byteSize, CodeToBinary, SendMessage, StringToBinary, LengthToBinary, OneIntResponseDeserializer, GetJsonFromBinary } from '../../Helper.js';

const getRoomsCode = 6;
const joinRoomCode = 4;
const isApprovedCode = 18;
let roomsData = "";
let joiningRoom = false;

// Function to join a room
async function JoinRoom(room) {
    const jsonFormat = {
        roomID: room.id
    };

    const jsonString = JSON.stringify(jsonFormat);
    let newJsonString = "";

    for (const c of jsonString) {
        newJsonString += StringToBinary(c);
    }

    const len = byteSize * jsonString.length;
    const code = CodeToBinary(joinRoomCode);
    const lengthMessage = LengthToBinary(len);

    const message = code + lengthMessage + newJsonString;
    joiningRoom = true;

    try {
        let response = await SendMessage(message);

        return OneIntResponseDeserializer(response) === 1;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}
async function getIsApproved() {
    let sData = CodeToBinary(isApprovedCode);
    let response = await SendMessage(sData);
    let result = OneIntResponseDeserializer(response);
    return result;

}
// Function to get all rooms
async function getAllRooms() {
    let sData = CodeToBinary(getRoomsCode);
    let roomList = [];

    let response = await SendMessage(sData);
    let decodedString = GetJsonFromBinary(response);

    let jsonObject = JSON.parse(decodedString);
    let rooms = jsonObject["Rooms"];
    if (rooms === null) {
        return rooms;
    }
    
    rooms = rooms.split("\n");

    if (rooms === "") {
        return rooms;
    }

    for (let i = 0; i < rooms.length; i++) {
        let elements = rooms[i].split(", ");
        roomList.push(new Room(elements[0], elements[1], elements[3] == 'True', elements[2], parseInt(elements[5]), parseInt(elements[4]), parseInt(elements[6]), parseInt(elements[7]), parseInt(elements[8])));
    }
    roomsData = roomList;
    return roomList;
}

// Function to update the rooms table
async function updateRoomsTable() {
    if (joiningRoom)
        return;

    let roomsData = await getAllRooms();
    const roomList = document.querySelector("#roomList tbody");

    // Clear the existing table rows
    roomList.innerHTML = '';

    // Filter out rooms with undefined properties
    const filteredRooms = roomsData.filter(room =>
        room.roomName !== undefined &&
        room.roomOwner !== undefined &&
        room.isPublic !== undefined &&
        room.category !== undefined &&
        room.currentPlayers !== undefined &&
        room.maxPlayers !== undefined
    );

    // Loop through the filtered array and create new rows for each room
    filteredRooms.forEach((room) => {
        const row = document.createElement("tr");
        const isFull = room.currentPlayers >= room.maxPlayers;

        row.innerHTML = `
            <td>${room.roomName}</td>
            <td>${room.roomOwner}</td>
            <td>${room.isPublic ? 'Public' : 'Private'}</td>
            <td>${room.category}</td>
            <td>${room.currentPlayers}/${room.maxPlayers}</td>
            <td><button class="joinButton" data-room-id="${room.id}" style="${isFull ? 'background-color: grey; color: white;' : ''}" ${isFull ? 'disabled' : ''}>Join</button></td>
        `;
        roomList.appendChild(row);
    });
}
async function sleep() {
    return new Promise(resolve => setTimeout(resolve, 5000));
}

// Set intervals for updating rooms table and checking socket state
setInterval(await updateRoomsTable, 2000); // every 2 seconds


// Event listener when the DOM is loaded
document.addEventListener("DOMContentLoaded", function () {
    const goBackButton = document.getElementById("goBackButton");
    goBackButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Menu/Menu.html";
    });

    // Attach a click event listener to the table
    const roomList = document.querySelector("#roomList tbody");
    roomList.addEventListener("click", async function (event) {
        const target = event.target;

        // Check if the clicked element is a "Join" button
        if (target.tagName === "BUTTON" && target.classList.contains("joinButton")) {
            // Extract the room ID from the button's data attribute
            const roomId = parseInt(target.dataset.roomId, 10);
            const room = target.dataset;
            // Find the corresponding room object
            const selectedRoom = roomsData.find(room => room.id === roomId);

            if (selectedRoom) {
                if (selectedRoom.isPublic == false) { // if the room is Private
                    const approval = window.confirm("This is a private room. Are you sure you want to wait")
                    if (approval)
                    {
                        await JoinRoom(selectedRoom);
                        await sleep();
                        let approved = -1;
                        while (approved == -1) {
                            approved = await getIsApproved();
                        }

                        if (approved == 1) {
                            CloseSocket();
                            alert("You have been allowed to enter the room, you will be transferred there immediately");
                            localStorage.setItem('timePerQuestion', selectedRoom.timePerQuestion);
                            localStorage.setItem('questionCount', selectedRoom.amountOfQuestions);

                            window.location.href = '../Waiting Room/WaitingRoom.html';
                        }
                        else if (approved == 0) {
                            alert("You are not allowed to get into this room !");
                        }
                        else if (approved == 9) {
                            alert("The room has been closed");
                        }
                    }

                }
                else {
                    JoinRoom(selectedRoom);
                    CloseSocket();
                    localStorage.setItem('timePerQuestion', selectedRoom.timePerQuestion);
                    localStorage.setItem('questionCount', selectedRoom.amountOfQuestions);
                    window.location.href = "../Waiting Room/WaitingRoom.html";
                }
            }
        }
    });
});
