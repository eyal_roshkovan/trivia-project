﻿import { CodeToBinary, SendMessage, GetJsonFromBinary, CloseSocket } from '../../Helper.js';
const personalStatisticsCode = 9;

function displayStatistics(data) {
    const jsonData = JSON.parse(data).Stats;

    // Update the HTML elements with the statistics
    document.getElementById('gamesWon').textContent = `Games Won: ${jsonData[0]}`;
    document.getElementById('roomsCreated').textContent = `Rooms Created: ${jsonData[1]}`;
    document.getElementById('totalAnswers').textContent = `Total Answers: ${jsonData[2]}`;
    document.getElementById('gamesPlayed').textContent = `Games Played: ${jsonData[3]}`;
}

async function getStatistics() {
    let response = await SendMessage(CodeToBinary(personalStatisticsCode));
    displayStatistics(GetJsonFromBinary(response));
}

document.addEventListener("DOMContentLoaded", function () {

    const goBackButton = document.getElementById("goBackButton");
    goBackButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Statistics/Statistics.html";
    });

    // Set a timer for, let's say, 0.1 seconds (you can adjust the time in milliseconds)
    const timerDelay = 100; // 0.1 seconds
    setTimeout(async function () {
        // Call the getHighScores function after the timer is done
        await getStatistics();
    }, timerDelay);
})