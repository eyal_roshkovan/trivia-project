﻿import { Result } from '../../classes.js'
import { CloseSocket, GetJsonFromBinary, OneBoolResponseDeserializer, SendMessage, CodeToBinary} from '../../Helper.js'
const getResultsCode = 25;
const hasEveryoneFinishedCode = 27;
let interval = undefined;
let waitLabel = document.getElementById('waitLabel');
let backButton = document.getElementById('goBackButton');

document.addEventListener("DOMContentLoaded", async function () {
    interval = setInterval(updateResults, 1000);
    backButton.addEventListener("click", function () {
        // Navigate to the Menu.html page
        CloseSocket();
        window.location.href = "../Menu/Menu.html";
    });
});

async function hasEveryoneFinished() {
    let response = await SendMessage(CodeToBinary(hasEveryoneFinishedCode));
    let hasEveryonefinished = OneBoolResponseDeserializer(response);
    return hasEveryonefinished;
}

async function getResults() {
    let results = [];
    let response = await SendMessage(CodeToBinary(getResultsCode));

    // Dumb algorithm because I couldn't find a way
    let json = GetJsonFromBinary(response);

    let jsonDocument = JSON.parse(json);

    // Access the "results" field
    let resultsJson = jsonDocument.results;

    let lines = resultsJson.split("  ");
    for (let line of lines) {
        if (!line.trim()) {
            continue; // Skip empty lines
        }

        let elements = line.split(", ");

        if (elements.length === 4) {
            results.push(new Result(elements[0], parseInt(elements[1]), parseInt(elements[2]), parseInt(elements[3])));
        }
    }

    return results;
}


async function updateResults() {
    if (!await hasEveryoneFinished) {
        return;
    }
    clearInterval(interval);
    let results = await getResults();
    console.log(results);
    let tbody = document.querySelector("#resultsTable tbody");

    // Clear existing rows in the table body
    tbody.innerHTML = "";

    // Iterate over the results and add rows to the table
    results.forEach(result => {
        // Create a new table row
        let row = document.createElement("tr");

        // Create and append table cells for each result property
        let usernameCell = document.createElement("td");
        usernameCell.textContent = result.username;
        row.appendChild(usernameCell);

        let correctAnswersCell = document.createElement("td");
        correctAnswersCell.textContent = result.rightAnswers;
        row.appendChild(correctAnswersCell);

        let wrongAnswersCell = document.createElement("td");
        wrongAnswersCell.textContent = result.wrongAnswers;
        row.appendChild(wrongAnswersCell);

        let avgTimeCell = document.createElement("td");
        avgTimeCell.textContent = result.averageTimeToAnswer;
        row.appendChild(avgTimeCell);

        // Append the row to the table body
        tbody.appendChild(row);
    });
    waitLabel.remove();
}