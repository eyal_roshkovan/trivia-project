// Import statements
import { UserData } from '../../classes.js'
import { byteSize, CloseSocket, GetJsonFromBinary, OneIntResponseDeserializer, OneBoolResponseDeserializer, SendMessage, CodeToBinary, StringToBinary, LengthToBinary } from '../../Helper.js'

// Constants

const approveUserCode = 21;
const CloseRoom = 10;
const leaveRoom = 13;
const kickPlayerCode = 26;
const readyUnreadyCode = 11;
const startGame = 14;
const refreshCode = 151;
const getIsOwnerCode = 40;
let canRefresh = true;
// Retrieve username from local storage
const username = localStorage.getItem('username');

// Initialize variables
var isOwner;
let goBackButton = document.getElementById("go-back-button");
let readyButton = document.getElementById("ready-button");


// Function to approve a waiting player
async function approvePlayer(username, approved) {

    const jsonFormat = {
        username,
        approved
    };

    const jsonString = JSON.stringify(jsonFormat);

    let newJsonString = "";
    for (let i = 0; i < jsonString.length; i++) {
        newJsonString += StringToBinary(jsonString[i]);
    }

    const len = byteSize * jsonString.length;
    const Code = CodeToBinary(approveUserCode);
    const lengthMessage = LengthToBinary(len);

    const message = Code + lengthMessage + newJsonString;

    try
    {
        await SendMessage(message);
    }
    catch (error) {
        console.error('Error:', error);
    }
}

async function sendCloseRoom() {
    let sData = CodeToBinary(CloseRoom);
    await SendMessage(sData);
}

// Function to kick a user
async function kickUser(username) {
    const jsonFormat = {
        username
    };

    const jsonString = JSON.stringify(jsonFormat);
    let newJsonString = "";

    for (let i = 0; i < jsonString.length; i++) {
        newJsonString += StringToBinary(jsonString.charAt(i));
    }

    const len = byteSize * jsonString.length;
    const code = CodeToBinary(kickPlayerCode);
    const lengthMessage = LengthToBinary(len);

    const message = code + lengthMessage + newJsonString;

    try {
        let response = await SendMessage(message);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}

// Function to send a leave room command
async function sendLeaveRoom() {
    let sData = CodeToBinary(leaveRoom);
    await SendMessage(sData);
}

// Function to send a ready/unready command
async function sendReadyUnready() {
    if (isOwner) {
        canRefresh = false;
        if (OneIntResponseDeserializer(await SendMessage(CodeToBinary(startGame))) == 1) {
            CloseSocket();
            window.location.href = '../Game Page/Gamepage.html';
        }
        else
        {
            alert("You can't start a game while you are alone in this room");
        }
    }
    else
    {
        SendMessage(CodeToBinary(readyUnreadyCode))
    }
}

async function getIsOwner() {
    let sData = CodeToBinary(getIsOwnerCode);
    return GetJsonFromBinary((await SendMessage(sData))).includes("true");
}

async function refresh() {
    if (isOwner === undefined) {
        isOwner = await getIsOwner();
        if (isOwner) {
            goBackButton.textContent = "Close Room";
            readyButton.textContent = "Start game";
        }
    }
    while (!canRefresh) {

    }
    let userList = [];
    let userWaitingList = [];

    let message = CodeToBinary(refreshCode);
    let response = await SendMessage(message);
    let decoded = GetJsonFromBinary(response);
    let jsonObject = JSON.parse(decoded);
    let data = jsonObject["data"];
    if (isOwner) {
        let elements = data.split("users in room:\n");

        let datas = elements[1].split(", ");

        for (let i = 0; i < datas.length; i += 4)
        {
            if (i < datas.length - 2)
            {
                userList.push(new UserData(datas[i], datas[i + 2], datas[i + 1], datas[i + 3] === "True"));
            }
        }

        let usersWaiting = elements[0].split("users waiting:\n");

        for (let i = 1; i < usersWaiting.length; i++) {
            let usersData = usersWaiting[i].split(", ");
            if (usersData.length > 2) {
                userWaitingList.push(new UserData(usersData[0], usersData[2], usersData[1], usersData[3]));
            }
        }
        updateWaitingList(userWaitingList);
    }
    else
    {
        try {
            let elements = data.split("\n");
            if (elements[0] == '1') // Everything is ok
            {
                for (let i = 1; i < elements.length - 1; i++) {
                    let user_elements = elements[i].split(", ");
                    userList.push(new UserData(user_elements[0], user_elements[2], user_elements[1], user_elements[3] == "True"));
                }
            }
            else if (elements[0] == '2') {
                CloseSocket();
                alert("Game begins in few moments, Get ready!");
                window.location.href = '../Game Page/Gamepage.html';
            }
            else if (elements[0] == '9') {
                CloseSocket();
                alert("You have been kicked out of the room by the owner");
                window.location.href = '../Join Room/JoinRoom.html';
            }
        }
        catch {
            CloseSocket();
            alert("The room has been closed by the owner");
            window.location.href = '../Join Room/JoinRoom.html';
        }
        console.log(data);
    }
    updateWaitingRoomTable(userList);


}

function updateWaitingList(userWaitingList) {
    userWaitingList.forEach((waitingUser) => {
        // Use the confirm function to prompt the user
        const userApproval = window.confirm(`Do you want to approve ${waitingUser.username}?`);

        if (userApproval) {
            // User approved
            // You can perform additional logic or handle the approval here
            console.log(`${waitingUser.username} is approved.`);
            approvePlayer(waitingUser.username, true);
        } else {
            // User disapproved (clicked "Cancel")
            // You can perform additional logic or handle the disapproval here
            console.log(`${waitingUser.username} is not approved.`);
            declinePlayer(waitingUser.username, false);
        }
    });
}



// Function to update the user list in the Waiting Room
async function updateWaitingRoomTable(userDataList) {
    
    const userList = document.getElementById("user-list");

    // Clear the existing list
    userList.innerHTML = '';

    // Create a list item for the table header with column headlines
    const tableHeader = document.createElement("li");
    tableHeader.innerHTML = `
        <div class="list-item">
            <div class="table-cell header">
                <span>Username</span>
            </div>
            <div class="table-cell header">
                <span>Games Played  </span>
            </div>
            <div class="table-cell header">
                <span>Ready ?</span>
            </div>
            <div class="table-cell header">
                <span>Roll</span>
            </div>
            <div class="table-cell header">
                <span>Actions</span>
            </div>
        </div>
    `;
    userList.appendChild(tableHeader);

    // Loop through the array and create new list items for each user
    userDataList.forEach((userData) => {
        const listItem = document.createElement("li");
        listItem.innerHTML = `
            <div class="list-item" style="color: ${userData.username === username ? 'blue' : 'black'}">
                <div class="table-cell">
                    <span>${userData.username}</span>
                </div>
                <div class="table-cell">
                    <span>${userData.points}</span>
                </div>
                <div class="table-cell">
                    <span>${userData.ready ? 'Yes' : 'No'}</span>
                </div>
                <div class="table-cell">
                    <span>${userData.roll}</span>
                </div>
                <div class="table-cell">
                    ${isOwner && userData.roll !== 'Owner' ? `<button class="kick-button" data-username="${userData.username}">Kick</button>` : '<span>None</span>'}
                </div>
            </div>
        `;
        userList.appendChild(listItem);
    });

    // Check conditions and set the "Start game" button's properties
    const allReadyExceptOwner = userDataList.every(user => user.ready || user.roll === 'Owner');
    const aloneInRoom = userDataList.length === 1 && userDataList[0].roll === 'Owner';
    if (aloneInRoom) {
        readyButton.disabled = true;
        readyButton.style.backgroundColor = 'grey';
        readyButton.style.color = 'white';
    } else {
        if (isOwner) {
            if (!allReadyExceptOwner) {
                readyButton.disabled = true;
                readyButton.style.backgroundColor = 'grey';
                readyButton.style.color = 'white';
            } else {
                readyButton.disabled = false;
                readyButton.style.backgroundColor = '';
                readyButton.style.color = '';
            }
        } else {
            readyButton.disabled = false;
        }
    }
}


// Sample function to show the confirmation modal
async function showConfirmationModal() {
    const modal = document.getElementById("confirmation-modal");
    modal.style.display = "block";

    // Add event listeners to the "Yes" and "No" buttons
    const modalYesButton = document.getElementById("modal-yes");
    modalYesButton.addEventListener("click", async function () {
        // Send Leave Room or Close Room based on the user's role
        if (isOwner) {
            await sendCloseRoom();
        } else {
            await sendLeaveRoom();
        }

        CloseSocket();
        window.location.href = '../Join Room/JoinRoom.html';
    });

    const modalNoButton = document.getElementById("modal-no");
    modalNoButton.addEventListener("click", function () {
        // Handle the "No" button action here
        modal.style.display = "none"; // Close the modal
    });
}

// Set interval for updating the waiting room table
setInterval(() => { refresh();}, 1000);

// Attach event listener to the document and listen for clicks on elements with the "kick-button" class
document.addEventListener("click", function (event) {
    if (event.target && event.target.classList.contains("kick-button")) {
        // Extract the username from the button's data-attribute or any other suitable method
        const username = event.target.dataset.username;

        // Call the kickUser function with the extracted username
        kickUser(username);
    }
});

// Attach event listener to the "Leave" button
document.addEventListener("DOMContentLoaded", async function () {
    goBackButton.addEventListener("click", await    showConfirmationModal);

    // Add click event listener to the "Ready" button
    readyButton.addEventListener("click", async function (event) {
            await sendReadyUnready();
    });
});