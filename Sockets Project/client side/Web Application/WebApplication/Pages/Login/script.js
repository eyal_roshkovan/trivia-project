﻿// script.js

// Import statements
import { CodeToBinary, LengthToBinary, StringToBinary, OneIntResponseDeserializer, SendMessage, byteSize, CloseSocket } from '../../Helper.js';

const loginCode = 1;

// Get the popup element
let popup = document.getElementById("popup");

// Function to open the popup
function openPopup() {
    // Hide specific elements inside the login-form except for the popup and label
    const loginForm = document.getElementById('login-form');
    const elementsToHide = loginForm.querySelectorAll('.inputbox, #LoginButton, #register-section, h2');

    for (const element of elementsToHide) {
        element.style.display = 'none';
    }

    // Make the popup visible
    popup.classList.add('open-popup');
}

// Function to close the popup
function closePopup() {
    // Show the previously hidden elements inside the login-form
    const loginForm = document.getElementById('login-form');
    const elementsToShow = loginForm.querySelectorAll('.inputbox, #LoginButton, #register-section, h2');

    for (const element of elementsToShow) {
        element.style.display = 'block';
    }

    // Hide the popup
    popup.classList.remove('open-popup');
}

// Function to try login
async function TryLogin(username, password) {
    const jsonFormat = {
        username,
        password
    };

    const jsonString = JSON.stringify(jsonFormat);

    let newJsonString = "";
    for (let i = 0; i < jsonString.length; i++) {
        newJsonString += StringToBinary(jsonString[i]);
    }

    const len = byteSize * jsonString.length;
    const Code = CodeToBinary(loginCode);
    const lengthMessage = LengthToBinary(len);

    const message = Code + lengthMessage + newJsonString;

    try {
        let response = await SendMessage(message);
        const isOK = OneIntResponseDeserializer(response) === 1;
        return isOK;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}

// Event listener when the DOM is loaded
document.addEventListener('DOMContentLoaded', () => {
    const loginForm = document.getElementById('login-form');
    const closePopupButton = document.getElementById('ClosePopup');

    const serialNumber = localStorage.getItem('serialNumber');
    localStorage.setItem('serialNumber', serialNumber);

    closePopupButton.addEventListener('click', (event) => {
        event.preventDefault();
        closePopup();
    });

    loginForm.addEventListener('submit', async (event) => {
        event.preventDefault();
        const username = loginForm.querySelector('input[type="text"]').value;
        const password = loginForm.querySelector('input[type="password"]').value;

        try {
            let isLoginOK = await TryLogin(username, password);

            if (isLoginOK) {
                localStorage.setItem('username', username);
                CloseSocket();
                window.location.href = "../Menu/Menu.html";
            } else {
                openPopup();
            }
        } catch (error) {
            console.error('Error:', error);
            // Handle the error if needed
        }
    });
});
