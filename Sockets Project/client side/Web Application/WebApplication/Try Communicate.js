﻿// Replace with your server's IP address and port
const serverIP = '127.0.0.1';
const serverPort = 8826;

// Create a WebSocket instance and connect to the server
const socket = new WebSocket(`ws://${serverIP}:${serverPort}`);

// Event handler for when the WebSocket connection is opened
socket.addEventListener('open', (event) => {
    console.log('Connected to the server.');

    // Send a message to the server
    const message = 'Hello, server!';
    socket.send(message);
});

// Event handler for when a message is received from the server
socket.addEventListener('message', (event) => {
    console.log('Received message from the server:', event.data);
});

// Event handler for any errors that occur
socket.addEventListener('error', (event) => {
    console.error('WebSocket error:', event);
});

// Event handler for when the connection is closed
socket.addEventListener('close', (event) => {
    if (event.wasClean) {
        console.log('Connection closed cleanly, code=' + event.code + ', reason=' + event.reason);
    } else {
        console.error('Connection abruptly closed');
    }
});

// Function to send a message to the server
function sendMessageToServer(message) {
    if (socket.readyState === WebSocket.OPEN) {
        socket.send(message);
    } else {
        console.error('Socket is not connected.');
    }
}

// Example: Sending a message to the server
sendMessageToServer('How are you, server?');
